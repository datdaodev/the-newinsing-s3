server {
    listen 80;
    server_name  new-insing.s3.test.insing.com;

    root   /insing/sites/new_insing/liveWeb/web;
    
    access_log  /insing/sites/new_insing/logs/access.log;
    error_log  /insing/sites/new_insing/logs/error.log;
    
    # Redirect /admin
    rewrite ^/admin(.*)$ https://admin.$host$1 permanent;
    
     ### START: inSing rewrite rules ###

         # Redirect iPad to /apps/tablet/xxxxx
         # https://twiki.insing.com/twiki/bin/view/Technology/WWWRedirectRules#Tablet_Redirect_Rules
         if ($http_user_agent ~ "(iPad|SHW-M110S|TF101)") {
             rewrite ^/food-drink(.*)$   http://apps.test.insing.com/tablet/food-drink$1;
             rewrite ^/shopping(.*)$     http://apps.test.insing.com/tablet/shopping$1;
             #rewrite ^/events(.*)$       http://apps.test.insing.com/tablet/events$1;
             #rewrite ^/movies(.*)$       http://apps.test.insing.com/tablet/movies$1;
             rewrite ^/news(.*)$         http://apps.test.insing.com/tablet/news$1;
         }

         # MObile and PC switch
         # https://twiki.insing.com/twiki/bin/view/Technology/WWWRedirectRules#Mobile_Redirect_Controller_Redir
         # https://twiki.insing.com/twiki/bin/view/Technology/WWWRedirectRules#PC_Redirect_Controller_Redirect
         rewrite ^/switchToPc.do     http://switch.test.insing.com/switchToPc.do     permanent;
         rewrite ^/switchToMobile.do http://switch.test.insing.com/switchToMobile.do permanent;

         # Redirect anything ending with search to search.insing.com
         # https://twiki.insing.com/twiki/bin/view/Technology/WWWRedirectRules#Legacy_Search_Redirect_Controlle
         rewrite ^/home/movies/search.*      /notfound   permanent;
         rewrite ^/singnet/movies/search.*   /notfound   permanent;
         rewrite ^/business-search(.*)       http://search.test.insing.com$1                 permanent;
         rewrite ^/movie/search(.*)          http://movies.test.insing.com/movie/search$1    permanent;
         if ($uri !~ ^/search/autocomplete) {
             rewrite ^.*search(.*)           http://search.test.insing.com$1     permanent;
         }

         # Superdeals Newsletter
         # https://twiki.insing.com/twiki/bin/view/Technology/WWWRedirectRules#Deals_Newsletter_Subscription_Co
         rewrite ^/superdeals/newsletter/subscribe.do(.*) http://superdeals.insing.com/newsletter/subscribe.do$1 permanent;

         # Newsletter
         rewrite ^/um/subscribeNewsletter.do(.*)$    http://newsletter.test.insing.com/subscribeNewsletter.do$1  permanent;
         rewrite ^/newsletters/newsletter/(.*)$      http://newsletter.test.insing.com/$1    permanent;

         # Redirect to mobile site m.test.insing.com
         # https://twiki.insing.com/twiki/bin/view/Technology/WWWRedirectRules#Mobile_Site_Redirect_Rules
         # Note: This is a "hack" way to support multiple if statments in NGINX
         #if ($cookie_d_cookie != p) {
         #    set $viewmode "M";
         #}
         #if ($http_user_agent ~* "(android|HTC_Sensation|iphone|symbianOS/9|opera.*mobi|Mobile|BlackBerry|Blazer|Bolt|Nokia|Samsung|Doris|Fennec|Iris|MOT.*MIB|Minimo|Netfront|Opera Mini|SonyEricsson|TeaShark|uZardWeb|windows phone|windows ce|htc.*touch|wap2.0|midp-2|hd.*mini|Playstation|maui.*wap)") {
             set $viewmode "${viewmode}O";
         #}
         #if ($uri !~ ^/[hungrygowhere|iframe\/superdeals|js|css]) {
         #    set $viewmode "${viewmode}B";
         #}
         #if ($viewmode = MOB) {
         #    rewrite ^(.*)$  http://m.test.insing.com$1;
         #}

         # Entertainment
         rewrite ^/entertainment/movies(.*)$ http://movies.test.insing.com$1     permanent;

         # Recipe
         rewrite ^/recipe(.*)$   http://food.test.insing.com/feature$1           permanent;

         # Business Finder
         rewrite ^/business-finder/biz-info/(.*)/(.*)/(.*)/(.*)/(.*)$    http://search.test.insing.com/business/$5/id-$1$2$3$4   redirect;
         rewrite ^/food-drink/biz-info/(.*)/(.*)/(.*)/(.*)/(.*)$         http://food.test.insing.com/business/$5/id-$1$2$3$4     redirect;

         # General redirects to the channels
         # https://twiki.insing.com/twiki/bin/view/Technology/WWWRedirectRules#Legacy_Hot_Deals_URL_Redirect_Ru
         # https://twiki.insing.com/twiki/bin/view/Technology/WWWRedirectRules#Legacy_URL_Redirect_Rules
         rewrite ^/food-drink(.*)$       http://food.test.insing.com$1                   permanent;
         rewrite ^/shopping(.*)$         http://shopping.test.insing.com$1               permanent;
         #rewrite ^/events(.*)$           http://events.test.insing.com$1                 permanent;
         #rewrite ^/event(.*)$            http://events.test.insing.com$1                 permanent;
         #rewrite ^/things-to-do(.*)$     http://events.test.insing.com$1                 permanent;
         #rewrite ^/movies/(.*)$           http://movies.test.insing.com/$1                 permanent;
         rewrite ^/news(.*)$             http://news.test.insing.com$1                   permanent;
         rewrite ^/business(.*)$         http://search.test.insing.com/business$1        permanent;
         rewrite ^/singnet(.*)$          http://singnet.test.insing.com$1                permanent;
         rewrite ^/star-reviewers(.*)$   http://reviews.test.insing.com/star-reviewers$1 permanent;
         rewrite ^/profiles(.*)$         http://profiles.test.insing.com/$1              permanent;
         rewrite ^/articles(.*)$         http://editorials.test.insing.com/articles$1    permanent;
         rewrite ^/sme(.*)$              http://search.test.insing.com/sme$1             permanent;

         # Redirect to /um
         # https://twiki.insing.com/twiki/bin/view/Technology/WWWRedirectRules#Force_Login_Controller_Redirect
         # https://twiki.insing.com/twiki/bin/view/Technology/WWWRedirectRules#Sign_Out_Controller_Redirect_Rul
         # https://twiki.insing.com/twiki/bin/view/Technology/WWWRedirectRules#Account_Link_Controller_Redirect
         # https://twiki.insing.com/twiki/bin/view/Technology/WWWRedirectRules#Filter_Redirects
         rewrite ^/um(.*)$   https://accounts.test.insing.com$1    permanent;

         # RSS
         #rewrite ^/rss/asia-entertainment    http://features.test.insing.com/rss/entertainment/asia  permanent;
         #rewrite ^/rss/business-news         http://features.test.insing.com/rss/business            permanent;
         rewrite ^/rss/celebrity-gossip      http://features.test.insing.com/rss/entertainment       permanent;
         rewrite ^/rss/entertainment         http://features.test.insing.com/rss/entertainment       permanent;
         #rewrite ^/rss/events                http://events.test.insing.com/rss                   permanent;
         rewrite ^/rss/food-drink            http://food.test.insing.com/rss                     permanent;
         #rewrite ^/rss/health-news           /info/rss                                           permanent;
         #rewrite ^/rss/home                  /info/rss                                           permanent;
         rewrite ^/rss/international-news    http://features.test.insing.com/rss                     permanent;
         rewrite ^/rss/lifestyle-news        http://features.test.insing.com/rss/entertainment       permanent;
         rewrite ^/rss/music-features        /info/rss                                           permanent;
         rewrite ^/rss/offbeat-news          /info/rss                                           permanent;
         rewrite ^/rss/shopping              http://shopping.test.insing.com/rss                 permanent;
         rewrite ^/rss/singapore-news        http://features.test.insing.com/rss/singapore           permanent;
         rewrite ^/rss/sports-news           http://features.test.insing.com/rss                     permanent;
         rewrite ^/rss/technology-news       http://features.test.insing.com/rss                     permanent;
         #rewrite ^/rss/things-to-do          http://events.test.insing.com/rss                   permanent;
         rewrite ^/rss/entertainment/asia    http://features.test.insing.com/rss/entertainment/asia  permanent;
         rewrite ^/rss/entertainment         http://features.test.insing.com/rss/entertainment       permanent;
         rewrite ^/rss/news/business         http://features.test.insing.com/rss/business            permanent;
         rewrite ^/rss/news/singapore        http://features.test.insing.com/rss/singapore           permanent;
         rewrite ^/rss/news(.*)              http://features.test.insing.com/rss                     permanent;
         rewrite ^/movies-list.xml           http://movies.test.insing.com/movies-list.xml       permanent;

         ### END: inSing rewrite rules ###

    location / {
        server_tokens off;
        index  app.php;
        try_files $uri $uri/ /app.php?$args;
    }

    ## Images and static content is treated different
    location ~* ^.+.(jpg|jpeg|gif|css|png|js|ico|eot|woff|ttf)$ {
            server_tokens off;
            log_not_found   off;
            access_log  off;
            expires     max;
    }

    ## Parse all .php file in the /var/www directory
    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass   fpm_backend;
        fastcgi_index  index.php;
        include fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        fastcgi_param  SCRIPT_NAME      $fastcgi_script_name;
        #fastcgi_intercept_errors        on;
        fastcgi_ignore_client_abort     off;
    }

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    location ~ /\.ht {
        server_tokens off;
        log_not_found   off;
        access_log  off;
        deny  all;
    }
}
server {
    listen 443;
    server_name  new-insing.s3.test.insing.com;

    root   /insing/sites/new_insing/liveWeb/web;
    
    access_log  /insing/sites/new_insing/logs/access.log;
    error_log  /insing/sites/new_insing/logs/error.log;
    
    location / {
        server_tokens off;
        index  app.php;
        try_files $uri $uri/ /app.php?$args;
    }

    ## Images and static content is treated different
    location ~* ^.+\.(jpg|jpeg|gif|css|png|js|ico|xml)$ {
        server_tokens off;
        log_not_found   off;
        access_log  off;
        expires     max;
    }

    ## Parse all .php file in the /var/www directory
    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass   fpm_backend;
        fastcgi_index  index.php;
        include fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        fastcgi_param  SCRIPT_NAME      $fastcgi_script_name;
    }

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    location ~ /\.ht {
        server_tokens off;
        log_not_found   off;
        access_log  off;
        deny  all;
    }
}

