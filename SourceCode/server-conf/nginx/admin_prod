server {
    listen 443;
    server_name  admin.homepage.insing.com;

    root   /insing/sites/new_insing/live/web_admin;
    
    # rewrite_log on;
    access_log /insing/sites/new_insing/logs/access.log;
    error_log  /insing/sites/new_insing/logs/error.log;

    ssl                         on;
    ssl_certificate             /etc/nginx/certs/new.insing.com.crt;
    ssl_certificate_key         /etc/nginx/certs/new.insing.com.key;
    ssl_session_timeout         5m;
    ssl_protocols               SSLv2 SSLv3 TLSv1;
    ssl_ciphers                 ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP;
    ssl_prefer_server_ciphers   on;

    location / {
        server_tokens off;
        index app.php;
        try_files $uri $uri/ /app.php?$args;
    }
    
    ## Images and static content is treated different
    location ~* ^.+.(jpg|jpeg|gif|css|png|js|ico|eot|woff|ttf)$ {
        server_tokens off;
        log_not_found   off;
        access_log  off;
        expires     max;
    }

    ## Parse all .php file in the /var/www directory
    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass   fpm_backend;
        fastcgi_index  index.php;
        include fastcgi_params;
        fastcgi_param  HTTPS            on;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        fastcgi_param  SCRIPT_NAME      $fastcgi_script_name;
        #fastcgi_intercept_errors        on;
        fastcgi_ignore_client_abort     off;
    }

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    location ~ /\.ht {
        server_tokens off;
        log_not_found   off;
        access_log  off;
        deny  all;
    }
}
