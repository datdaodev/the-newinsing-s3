<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/16/15
 * Time: 5:59 PM
 */

namespace inSing\ApiAdapterBundle\Channels;

use inSing\ApiAdapterBundle\Channels\Helper\AccountApiBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * Description of AccountApiRestful
 *
 * @author Dat.Dao <dat.dao@s3corp.com.vn>
 */
class AccountApiRestful extends AccountApiBase{

    //Test ??? http://api.accountsss.test.insing.com/auth

    /**
     * @author Dat Dao <john.doe@example.com>
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, $cache, $logger) {
        parent::__construct($container, $cache, $logger, 'profile');
    }


    /**
     * @author Dat Dao <john.doe@example.com>
     * @return type
     */
    private function getSessionToken() {
        $sesstion_token_obj = $this->getSesstionToken();

        if (isset($sesstion_token_obj['session_token']) && !empty($sesstion_token_obj['session_token'])) {
            if (isset($sesstion_token_obj['channel'])) {
                $this->channelName = $sesstion_token_obj["channel"];
            }
            return $sesstion_token_obj['session_token'];
        }
        throw new \Exception("Empty session_token", 402);
    }

    /**
     * @author Dat Dao <john.doe@example.com>
     * @param type $id
     * @param type $email
     * @param type $displayName
     * @return array
     * @todo  8.LIST USERS API
    The Get User Details API is used to retrieve the details for 1 or list of user Ids. Clients must use a valid session token
    obtained with a previous call to the Authenticate API.
    The URL used to access Get User Detail API is (only active user is retrieved-able):
    ??? Production ??? http://api.account.insing.com/users
    ??? Test ??? http://api.account.test.insing.com/users
    HTTP method supported is: GET.
     */
    public function getListUsers($inputs, &$message) {
        $message = "";
        $params = array();

        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/users";
            $method = "get";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->runApi($baseUriUrl, $method, $params, true);
            $profile = json_decode($result, true);
            $data = array();
            if (isset($profile['status']) && $profile['status'] == "200" ) {
                if (isset($profile['data']['0'])) {
                    $user = ($profile['data']['0']);
                    $data = array(
                        'insing_user_id' => isset($user['id']) ? $user['id'] : null,
                        'first_name' => isset($user['first_name']) ? $user['first_name'] : '',
                        'last_name' => isset($user['last_name']) ? $user['last_name'] : '',
                        'facebook_name' => isset($user['facebook_name']) ? $user['facebook_name'] : '',
                        'insing_user_name' => isset($user['last_name']) ? $user['last_name'] : '',
                        'nick_name' => isset($user['nickname']) ? $user['nickname'] : '',
                        'avatar' => (isset($user['avatar']) && isset($user['avatar']['avatar_url'])) ? $user['avatar']['avatar_url'] : '',
                        'email' => isset($user['email']) ? $user['email'] : '',
                        'mobile' => isset($user['mobile_number']) ? $user['mobile_number'] : '',
                        'facebook_id' => isset($user['facebook_id']) ? $user['facebook_id'] : '',
                        'number_of_followers' => isset($user['number_of_followers']) ? (int)$user['number_of_followers'] : 0
                    );
                }
            }
            return $data;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->error($e);
        }
        return array();
    }

}
