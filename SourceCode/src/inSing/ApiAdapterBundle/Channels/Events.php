<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\ApiAdapterBundle\Channels;

use inSing\ApiAdapterBundle\Channels\Helper\ApiAdapter;

/**
 * Description of ThirdPartyMovies
 *
 * @author linuxmint
 */
class Events extends ApiAdapter {

    private $allCategoriesCache = array();

    public function __construct($container, $cache, $channelLogger) {
        parent::__construct($container, $cache, $channelLogger, "events");

        $this->allCategoriesCache['KEY'] = 'ALL_CATEGORIES_CACHE_KEY';
        $this->allCategoriesCache['TIME'] = 600;
    }    

    public function search($params = array()) {
        $url = $this->basicUrl . EventsUriMapping::SEARCH;
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function getDetails($ids) {
        $url = $this->basicUrl . str_replace("{id}", $ids, EventsUriMapping::DETAILS);
        $params = array();
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function autoComplete($params) {
        $url = $this->basicUrl . EventsUriMapping::AUTO_COMPLETE;
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function getAllCategories() {
        $categories = $this->_cache->getCache($this->allCategoriesCache['KEY']);
        if(! empty($categories)) {
            //get from cache
            $this->_logger->info('getAllCategories get from cache with key:' . $this->allCategoriesCache['KEY']);
            return $categories;
        }
        //st call api
        $url = $this->basicUrl . EventsUriMapping::GET_ALL_CATEGORIES;
        $params = array();
        $result = $this->runApiByMethod($url, "GET", $params);
        //ed call api
        if(! empty($result) && ! empty($result['data'])) {
            $categories = $result['data'];
            //set into cache
            //$this->_logger->info('getAllCategories set into cache with key:' . $this->allCategoriesCache['KEY'] . ', data'. json_encode($categories) );
            $this->_logger->info('getAllCategories set into cache with key:' . $this->allCategoriesCache['KEY'] );
            $this->_cache->setCache($this->allCategoriesCache['KEY'], $categories, $this->allCategoriesCache['TIME']);
            return $categories;
        }
        return array();
    }

    public function createEvent(array $params = array()) {
        /**
        $today = new \DateTime();
        $params = array();
        $params['name'] = 'title-test-dmd';
        $params['subcategories'] = implode('|', array('15,16'));
        $params['description'] = 'description-test-dmd';
        $params['website'] = 'http://google.com.vn';
        $params['adult_rating'] = 'N';
        $params['start_date'] = $today->format('Y-m-d');
        $params['end_date'] = $today->format('Y-m-d');
        $params['venue_name'] = 'venue-name-test-dmd';
        $params['venue_unit_number'] = 12345;
        $params['venue_building_name'] = 'abc';
        $params['venue_block_number'] = 789;
        $params['venue_street_name'] = 'address';
        $params['venue_postal_code'] = 123456;
        $params['venue_phone_number'] = 77666;
        $params['price_min'] = 123;
        $params['price_max'] = 1234;
        $params['organizer_name'] = 'company name';
        $params['organizer_contact_person'] = 'full-name-test-dmd';
        $params['organizer_contact_number'] = 8888;
        $params['organizer_contact_email'] = 'datagit@yahoo.com';
        $params['gallery_id'] = 20022686;
        $params['facebook_event_ids'] = '123';//str_replace(',', '|', $this->getFacebookEventIds());
        $params['vendor_url'] = 'http://yahoo.com';//$this->ticketUrl;
        $params['vendor_image_url'] = 'http://abc';
        $params['vendor_name'] = 'vender name';
        $params['event_timing'] = '';

        $params['published'] = 'N';
         */
        $url = $this->basicUrl . EventsUriMapping::CREATE_EVENTS;
        $result = $this->runApiByMethod($url, "POST", $params);
        if(! empty($result) && ! empty($result['data'])) {
            return $result['data'];
        }
        return 0;
    }
    public function getTopThingsToDo($params = array())
    {
        $url = $this->basicUrl . EventsUriMapping::TOP_THINGS_TO_DO;
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function getSupermodule($params = array())
    {
        $url = $this->basicUrl . EventsUriMapping::SUPER_MODULE;
        return $this->runApiByMethod($url, "GET", $params);
    }
}
