<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace inSing\ApiAdapterBundle\Channels\Helper;

/**
 * Description of ThirdPartyApi
 *
 * @author linuxmint
 */
class ApiAdapter
{
    protected $_logger = null;
    protected $_cache = null;
    protected $_channel = null;
    protected $_container = null;
    protected $_config = array();

    protected $consumerKey = null;
    protected $consumerSerect = null;
    protected $accessToken = null;
    protected $accessTokenSecrect = null;
    protected $basicUrl = null;
    protected $authenticateUrl = null;
    protected $sessionTokenCacheKey = null;

    private function initCache($container, $cache)
    {
        $cacheObject = $container->get('new.insing.cache');
        return $cacheObject;
    }

    public function __construct($container, $cache, $channelLog, $channelConfig)
    {
        $this->_container = $container;
        $this->_logger = $container->get($channelLog);
        $this->_cache = $this->initCache($container, $cache);

        $configs = $this->loadConfig($channelConfig);

        $this->_channel = $channelLog;        

        $this->consumerKey = $configs["consumer_key"];
        $this->consumerSerect = $configs["consumer_secret"];
        $this->accessToken = $configs["access_token"];
        $this->accessTokenSecrect = $configs["access_token_secret"];

        $this->authenticateUrl = $configs["authenticate_url"];
        $this->basicUrl = $configs["basic_url"];

        $this->sessionTokenCacheKey = $configs["session_token_cache_key"];
    }

    protected function loadConfig($channel)
    {
        $channelsApi = $this->_container->getParameter("channels_api");
        $this->_config = $channelsApi[$channel];

        return $this->_config;
    }

    private function genSignature($params, $pathinfo)
    {
        $result = $pathinfo;

        ksort($params);
        foreach ($params as $key => $value) {
            $result .= urlencode($key . $value);
        }

        return md5($result);
    }

    private function getCallingString($url, $secret, $fields, $is_return_array = true)
    {
        //Generate API Signature
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);
        $pathinfo = $secret . $pathinfo;
        # fix case url have app_dev.php
        $pathinfo = str_replace('app_dev.php', '', $pathinfo);

        $fields['sig'] = $this->genSignature($fields, $pathinfo);

        if (!$is_return_array) {
            // form up variables in the correct format for HTTP POST
            $fields_string = '';
            foreach ($fields as $key => $value) {
                $fields_string .= $key . '=' . urlencode($value) . '&';
            }
            $fields_string = rtrim($fields_string, '&');

            return $fields_string;
        }
        return $fields;
    }

    protected function authenticate()
    {
        try
        {
            $result = $this->doPostWrapper($this->authenticateUrl, $this->consumerSerect, array(
                'consumer_key' => $this->consumerKey,
                'consumer_secret' => $this->consumerSerect,
                'access_token' => $this->accessToken,
                'access_token_secret' => $this->accessTokenSecrect
            ));

            $this->_logger->info($this->authenticateUrl);
            $this->_logger->info(json_encode($result));
            

            if (RestfulAPIHelper::HTTP_OK != $result['status']) {
                $this->_logger->error("Authenticate fail, results: " . json_encode($result));
                return null;
            }
            return $result['data'];
        } catch (\Exception $exc) {
            $this->_logger->error("Authenticate fail, " . $exc->getMessage());
        }
        return null;
    }

    /**
     * Get session token
     * @return string sessionToken
     */
    protected function getSessionToken()
    {
        try {
            $cache_key = $this->sessionTokenCacheKey;
            $sessionToken = $this->_cache->getCache($cache_key);

            if (empty($sessionToken)) {
                $result = $this->authenticate();
                if (!empty($result) && count($result)) {
                    $sessionToken = $result['session_token'];
                    $expire_at = new \DateTime();
                    $expire_at->setTimestamp($result['expire_at']);
                    $current_date = new \DateTime();
                    $diff_date = $current_date->diff($expire_at);

                    $minutes = $diff_date->days * 24 * 60;
                    $minutes += $diff_date->h * 60;
                    $minutes += $diff_date->i;
                    $second = ($diff_date->s + ($minutes * 60));
                    // Time backup
                    if ($second > 600)
                    {
                        $second = $second - 600;
                    }

                    $this->_cache->setCache($cache_key, $sessionToken, $second);
                }
            }

            return $sessionToken;
        }
        catch (\Exception $exc)
        {
            // Log            
            $this->_logger->error("getSessionToken fail., " . $exc->getMessage());
        }

        return null;
    }

    protected function doGetWrapper($url, $secret, $fields) {

        $curl = new Curl($this->_container);
        $curl->setMethod(Curl::HTTP_GET);
        $curl->setChannel($this->_channel);

        $fields = $this->getCallingString($url, $secret, $fields);
        $res = $curl->call($url, $fields);

        if ($res['status'] == Curl::SESSION_TOKEN_TIMEOUT 
                || $res['status'] == Curl::HTTP_UNAUTHORIZED) {
            $this->_cache->deleteCache($this->sessionTokenCacheKey);
            $sessionToken = $this->getSessionToken();
            $fields['session_token'] = $sessionToken;

            $fields = $this->getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
        } else if ($res['status'] != RestfulAPIHelper::HTTP_OK) {
            if (is_array($res)) {
                $this->_logger->error("Success URL= " . $url);
                $this->_logger->error(json_encode($res));
            } else {
                $this->_logger->error('RETURN NULL, URL= ' . $url, $fields);
            }
        }
        return $res;
    }

    protected function doGetJsonWrapper($url, $secret, $fields) {
        try {
            $curl = new Curl($this->_container);
            $curl->setMethod(Curl::HTTP_GET);
            $curl->setChannel($this->_channel);
            $fields = $this->getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
            if (!$res) {
                return array();
            }
            return $res;
        } catch (\Exception $exc) {
            $this->_logger->error("CALL FAIL URL= " . $url);
            $this->_logger->error($exc->getMessage());
        }
        return array();
    }

    protected function doPostWrapper($url, $secret, $fields) {

        $curl = new Curl($this->_container);
        $curl->setMethod(Curl::HTTP_POST);

        $fields = $this->getCallingString($url, $secret, $fields);
        $res = $curl->call($url, $fields);


        $this->_logger->info("URL= " . $url, $fields);

        if ($res['status'] == RestfulAPIHelper::SESSION_TOKEN_TIMEOUT || $res['status'] == RestfulAPIHelper::HTTP_UNAUTHORIZED)
        {
            $this->_cache->deleteCache($this->sessionTokenCacheKey);
            $sessionToken = $this->getSessionToken();
            $fields['session_token'] = $sessionToken;

            $fields = $this->getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
        } else if ($res['status'] != RestfulAPIHelper::HTTP_OK) {
            if (is_array($res)) {
                $this->_logger->error("Success URL= " . $url, $res);
                $this->_logger->error(json_encode($res));
            } else {
                $this->_logger->error('RETURN NULL, URL=' . $url, $fields);
            }
        }
        return $res;
    }

    protected function runApiByMethod($url, $method, $params) {
        $session_token = $this->getSessionToken();
        if(! $session_token) {
            $this->_logger->error("Authenticate is failure, session_token: " . $session_token);
            return array();
        }
        $params["session_token"] = $session_token;
        switch ($method) {
            case "GET":
                if (isset($params['content_type']) && $params['content_type'] == 'json') {
                    $results = $this->doGetJsonWrapper($url, $this->consumerSerect, $params);
                } else {
                    $results = $this->doGetWrapper($url, $this->consumerSerect, $params);
                }
                break;
            case "POST":
                $results = $this->doPostWrapper($url, $this->consumerSerect, $params);
                break;
            default:   
                //default GET
                $results = $this->doGetWrapper($url, $this->consumerSerect, $params);                
                break;
        }
        
        if (RestfulAPIHelper::HTTP_OK != $results['status']) {
            //write log
            $this->_logger->error($url);
            $this->_logger->error("Call Failed with status #200, Results: " . json_encode($results));
        }
        //write log        
        //$this->_logger->info("Call API Success", $results);
        return $results;
    }
}
