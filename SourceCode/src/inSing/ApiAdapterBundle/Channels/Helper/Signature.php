<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/5/15
 * Time: 5:56 PM
 */

namespace inSing\ApiAdapterBundle\Channels\Helper;


class Signature {

    private $secret;

    public function __construct($secret){
        $this->secret    = $secret;
    }

    /**
     * Check signature
     *
     * @author  Co Vu Thanh Tung
     * @param   string $params
     * @param   string $contentType
     * @param   array  $pathInfo
     * @return  string
     */
    public function checkSignature($params, $contentType, $pathInfo, $content)
    {
        if(!isset($params['sig']) || $params['sig'] == ''){
            throw new \Exception(UserMessage::ERROR_SIGNATURE_INCORRECT_MSG, UserMessage::ERROR_PARAMS_INVALID_CODE);
        }

        $getSig = false;
        if(isset($params['create-sig']) && $params['create-sig']) {
            $getSig = true;
        }

        $pathInfo = $this->secret . $pathInfo;
        $pathInfo = str_replace('/', '', $pathInfo);
        if($contentType == 'json'){
            $sig = $this->genSignatureJson($content, $pathInfo);
        } else {
            $sig = $this->genSignature($params, $pathInfo);
        }

        /*         if($getSig) {
                    echo 'Sig: <b>'.$sig.'</b>';die;
                } */

        if( $params['sig'] != $sig ){
            throw new \Exception(UserMessage::ERROR_SIGNATURE_INCORRECT_MSG, UserMessage::ERROR_PARAMS_INVALID_CODE);
        }

        return true;
    }


    /**
     * Generate API Signature for Content-Type : application/json
     *
     * @author  Co Vu Thanh Tung
     * @param   string $params
     * @param   array  $pathInfo
     * @return  string
     */
    public function genSignatureJson($params, $pathInfo)
    {
        $params = str_replace("\n", '', $params);
        $params = str_replace("\r", '', $params);

        //echo md5($pathInfo . $params)."\n";

        return md5($pathInfo . $params);
    }

    /**
     * Generate API Signature
     *
     * @author  Co Vu Thanh Tung
     * @param   string $params
     * @param   array  $pathInfo
     * @return  string
     */
    public function genSignature($params, $pathInfo)
    {
        $result = $pathInfo;

        if(isset($params['sig'])){
            unset($params['sig']);
        }

        if(isset($params['create-sig'])){
            unset($params['create-sig']);
        }

        ksort($params);
        foreach ($params as $key => $value){
            if($key != 'sig'){
                if (is_array($value)) {
                    foreach ($value as $subValue) {
                        $result .= rawurlencode( $key . '[]' ) . urlencode($subValue);
                    }
                } else {
                    $result .= $key . urlencode($value);
                }
            }
        }

        //echo md5($result)."\n";

        return md5($result);
    }

    public function genPathInfo($url) {
        //Generate API Signature
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);
        $pathinfo = $this->secret . $pathinfo;
        return $pathinfo;
    }

}