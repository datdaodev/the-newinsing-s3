<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\ApiAdapterBundle\Channels;

use inSing\ApiAdapterBundle\Channels\Helper\ApiAdapter;

/**
 * Description of ThirdPartyMovies
 *
 * @author linuxmint
 */
class Movies extends ApiAdapter
{
    public function __construct($container, $cache, $channelLogger)
    {        
        parent::__construct($container, $cache, $channelLogger, "movies");
    }

    public function autoComplete($params = array())
    {
        $url = $this->basicUrl . MoviesUriMapping::AUTOCOMPLETE;
        return $this->runApiByMethod($url, "GET", $params);
    }
    
    public function search($params = array())
    {
        $url = $this->basicUrl . MoviesUriMapping::SEARCH;
        return $this->runApiByMethod($url, "GET", $params);        
    }

    /**
     * @param array $params
     * @return array|Helper\Ambigous
     * @author Cuong Au
     */
    public function  getShowtmes($params = array())
    {
        $url = $this->basicUrl . MoviesUriMapping::SHOWTIME;
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function getDetails($ids)
    {
        $url = $this->basicUrl . str_replace("{id}", $ids, MoviesUriMapping::DETAILS);
        $params = array();
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function getNews($params = array())
    {
        $url = $this->basicUrl . MoviesUriMapping::NEWS;
        return $this->runApiByMethod($url, "GET", $params);
    }

    /**
     * @author Dat.Dao
     * @param array $params
     * @return array|Helper\Ambigous
     */
    public function getListCinemas($params = array())
    {
        /**
         * per_page = 20 is default
         * $result = $this->fetchCinemas($params, $useCache);
        $cinemas = (isset($result['data']) && $result['data']) ? $result['data'] : array();
        if (isset($result['per_page']) && isset($result['total']) && ($result['per_page'] < $result['total'])) {
        $ceil = ceil($result['total'] / $result['per_page']);
        for ($i = 2; $i <= $ceil; $i++) {
        $params['page'] = $i;
        $result = $this->fetchCinemas($params, $useCache);
        if (isset($result['data']) && $result['data']) {
        $cinemas = array_merge($cinemas, $result['data']);
        }
        }
        }

        return $cinemas;
         */
        $url = $this->basicUrl . MoviesUriMapping::LIST_CINEMAS;
        $result = $this->runApiByMethod($url, "GET", $params);
        $cinemas = (isset($result['data']) && $result['data']) ?
            $result['data'] :
            array();
        if (isset($result['per_page'])
            && isset($result['total'])
            && (($result['per_page']* $result['page'] )< $result['total'])) {
            $ceil = ceil($result['total'] / $result['per_page']);
            for ($i = ($result['page'] + 1); $i <= $ceil; $i++) {
                $params['page'] = $i;
                $result = $this->runApiByMethod($url, "GET", $params);
                if (isset($result['data'])
                    && $result['data']) {
                    $cinemas = array_merge($cinemas, $result['data']);
                }
            }
        }


        return $cinemas;
    }

    /**
     * @author Dat.Dao
     * @param array $params
     * @return array|Helper\Ambigous
     */
    public function getListCinemaChains($params = array())
    {
        $url = $this->basicUrl . MoviesUriMapping::LIST_CINEMA_CHAINS;
        return $this->runApiByMethod($url, "GET", $params);
    }

    /**
     * @author Dat.Dao
     * @param $id
     * @param array $params
     * @return array|Helper\Ambigous
     */
    public function getCinemaDetail($id, array $params = array())
    {
        $url = $this->basicUrl . str_replace("{id}", $id, MoviesUriMapping::CINEMA_DETAIL);
        return $this->runApiByMethod($url, "GET", $params);
    }
    /**
     * @author Dat.Dao
     * @param array $id
     * @param array $params
     * @return array|Helper\Ambigous
     */
    public function getDetailById(array $id, array $params = array())
    {
        $url = $this->basicUrl . str_replace("{id}", implode('|', $id), MoviesUriMapping::DETAILS);
        return $this->runApiByMethod($url, "GET", $params);
    }
}
