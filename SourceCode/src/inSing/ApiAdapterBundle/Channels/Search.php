<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\ApiAdapterBundle\Channels;

use inSing\ApiAdapterBundle\Channels\Helper\ApiAdapter;

/**
 * Description of ThirdPartyMovies
 *
 * @author linuxmint
 */
class Search extends ApiAdapter
{
    public function __construct($container, $cache, $channelLogger)
    {        
        parent::__construct($container, $cache, $channelLogger, "search");
    }
    
    public function getEvents()
    {
        $url = $this->basicUrl . SearchUriMapping::EVENT_SEARCH;
        $params = array();
        return $this->runApiByMethod($url, "GET", $params);        
    }

    public function getEventDetails($ids) {
        $url = $this->basicUrl . str_replace("{id}", $ids, SearchUriMapping::EVENT_DETAILS);
        $params = array();
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function getEventPrevNext($id) {
        $url = $this->basicUrl . str_replace("{id}", $id, SearchUriMapping::EVENT_PREV_NEXT);
        $params = array();
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function getBusinessDetails($ids, $params = array()) {
        $url = $this->basicUrl . str_replace("{ids}", $ids, SearchUriMapping::BUSINESS_DETAIL_IDS);
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function GetBusinessSearchV5($params = array()) {
        $url = $this->basicUrl . SearchUriMapping::BUSINESS_SEARCH_V5;
        return $this->runApiByMethod($url, "GET", $params);
    }
}
