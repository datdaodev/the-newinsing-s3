<?php

namespace inSing\ApiAdapterBundle\Channels;

/**
 * Class UriMapping
 *
 * @package inSing\ApiBundle\Channels
 */
class MoviesUriMapping
{
    const SEARCH  = "/2.0/movies";
    const DETAILS = "/2.0/movie/{id}";
    const AUTOCOMPLETE = "/2.0/movies/autocomplete";
    const NEWS  = "/2.0/news";
    const SHOWTIME  = "/2.0/showtimes";
    const CINEMA_DETAIL = "/2.0/cinema/{id}";
    const LIST_CINEMA_CHAINS = "/2.0/cinemachains";
    const LIST_CINEMAS = "/2.0/cinemas";
    const ACTIVE_STATUS = '*';
}