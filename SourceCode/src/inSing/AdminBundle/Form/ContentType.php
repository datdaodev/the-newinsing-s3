<?php
namespace inSing\AdminBundle\Form;

use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use inSing\DataSourceBundle\Lib\Constant;
use inSing\AdminBundle\Form\EventListener\ContentSubscriber;

class ContentType extends AbstractType
{
    private $doctrine;
    private $container;
    private $mode;

    public function __construct($doctrine, $container, $mode = array('type' => 'add', 'module' => 'carousel', 'file_uploaded' => false))
    {
        $this->doctrine = $doctrine;
        $this->container = $container;
        $this->mode = $mode;
    }

    public function getMode()
    {
        return $this->mode;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $subscriber = new ContentSubscriber($builder->getFormFactory(), $this->doctrine, $this->mode);
        $builder->addEventSubscriber($subscriber);

        if ($this->mode['module'] == 'hgw') {
            $builder->add('content_type', 'choice', array(
                'choices' => Constant::getHGWContentTypes()
            ));
        } else {
            $builder->add('content_type', 'choice', array(
                'choices' => Constant::getContentTypes()
            ));
        }


        $builder->add('item_id', 'text', array(
            'required' => false,
            'constraints' => array(
                new NotBlank(array('message' => 'ID must be required.'))
            )
        ));


        if ($this->mode['module'] == 'hgw') {
            $builder->add('content_url', 'text', array(
                'read_only' => true,
                'required' => false
            ));
        } else {
            $builder->add('content_url', 'text', array(
                'required' => false,
                'constraints' => array(
                    new NotBlank(array('message' => 'Content URL must be required.')),
                    new Url(array('message' => 'Content URL is invalid.'))
                ),
            ));
        }

        $builder->add('image_browse', 'file', array(
            'required' => false,
            'mapped' => false,
            'attr' => array('accept' => "image/*")
        ));

        if ($this->mode['module'] == 'hgw') {
            $builder->add('slot', 'choice', array(
                'choices' => Constant::getAllHGWSlot(),
                'constraints' => array(
                    new NotBlank(array('message' => 'Slot must be required.'))
                ),
            ));
        } else {
            $builder->add('slot', 'choice', array(
                'choices' => Constant::getAllCarouselSlot(),
                'constraints' => array(
                    new NotBlank(array('message' => 'Slot must be required.'))
                ),
            ));
        }

        if ($this->mode['module'] == 'hgw') {
            $builder->add('title', 'text', array(
                'required' => false,
                'read_only' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Title must be required.')),
                    new Length(array('maxMessage' => 'Title max length is 80 characters.', 'max' => 80)),
                ),
                'attr' => array('maxlength' => 80)
            ));
        } else {
            $builder->add('title', 'text', array(
                'required' => false,
                'constraints' => array(
                    new NotBlank(array('message' => 'Title must be required.')),
                    new Length(array('maxMessage' => 'Title max length is 80 characters.', 'max' => 80)),
                ),
                'attr' => array('maxlength' => 80)
            ));
        }

        if ($this->mode['module'] == 'hgw') {
            $builder->add('description', 'textarea', array(
                'required' => false,
                'read_only' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Description must be required.'))
                ),
                'attr' => array('class' => 'full')
            ));

            $builder->add('editor', 'text', array(
                'required' => false,
                'read_only' => true
            ));
        } else {

            $builder->add('tagline_1', 'text', array(
                'required' => false,
                'mapped' => false,
                'constraints' => array(
                    new NotBlank(array('message' => 'Tagline 1 must be required.')),
                    new Length(array('maxMessage' => 'Title max length is 14 characters.', 'max' => 14)),
                ),
                'attr' => array('maxlength' => 14)
            ));

            $builder->add('tagline_2', 'text', array(
                'required' => false,
                'mapped' => false,
                'constraints' => array(
                    new NotBlank(array('message' => 'Tagline 2 must be required.')),
                    new Length(array('maxMessage' => 'Title max length is 14 characters.', 'max' => 14)),
                ),
                'attr' => array('maxlength' => 14)
            ));

            $builder->add('tagline_3', 'text', array(
                'required' => false,
                'mapped' => false,
                'constraints' => array(
                    new NotBlank(array('message' => 'Tagline 3 must be required.')),
                    new Length(array('maxMessage' => 'Title max length is 14 characters.', 'max' => 14)),
                ),
                'attr' => array('maxlength' => 14)
            ));
        }

        if ($this->mode['file_uploaded']) {
            $builder->add('image_url', 'text', array(
                'required' => false
            ));

        } else {
            if ($this->mode['module'] == 'hgw') {
                $builder->add('image_url', 'text', array(
                    'required' => false,
                    'read_only' => true
                ));

            } else {
                $builder->add('image_url', 'text', array(
                    'required' => false,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Image must be required.')),
                        new Url(array('message' => 'Image is invalid.'))
                    ),
                ));
            }
        }

        $builder->add('channel_name', 'text', array(
            'required' => false,
            'read_only' => true,
            'constraints' => array(
                new NotBlank(array('message' => 'Channel must be required.'))
            )
        ));



        $builder->add('publish_on', 'date', array(
            'required' => false,
            'widget' => 'single_text',
            'format' => 'dd MMM yyyy',
            'constraints' => array(
                new NotBlank(array('message' => 'Publish On must be required.'))
            ),
            'attr' => array('size' => 30, 'class' => 'span150')
        ));


        $builder->add('expire_on', 'date', array(
            'required' => false,
            'widget' => 'single_text',
            'format' => 'dd MMM yyyy',
            'attr' => array('size' => 30, 'class' => 'span150')
        ));

        $builder->add('module', 'hidden', array(
            'required' => false,
            'data' => $this->mode['module']
        ));

        $builder->add('updated_at', 'hidden', array(
            'required' => false,
            'data' => new \DateTime("now")
        ));

        //hidden field

        $builder->add('deal_id', 'hidden', array(
            'mapped' => false,
            'required' => false
        ));

        $builder->add('tabledb_id', 'hidden', array(
            'mapped' => false,
            'required' => false
        ));

        $builder->add('slug', 'hidden', array(
            'mapped' => false,
            'required' => false
        ));

        $builder->add('event_data', 'hidden', array(
            'mapped' => false,
            'required' => false
        ));

        $builder->add('series_seo_url', 'hidden', array(
            'mapped' => false,
            'required' => false
        ));

    }

    public function getName()
    {
        return 'admin_content';
    }
}