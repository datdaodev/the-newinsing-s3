<?php
/**
 * Customize assetic filter
 *
 * @author Trung Nguyen
 */
namespace inSing\AdminBundle\Assetic\Filter;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;

class InSingCssRewrite implements FilterInterface
{
	private $enviroment;

	public function __construct($enviroment)
	{
		$this->enviroment = $enviroment;
	}

    public function filterLoad(AssetInterface $asset)
    {
        $content = $asset->getContent();

        if ($this->enviroment == 'admin_prod' || $this->enviroment == 'admin_dev') {
        	$content = str_replace('../images/', '/bundles/insingadmin/images/', $content);
        }

        $asset->setContent($content);
    }


    public function filterDump(AssetInterface $asset)
    {
        $content = $asset->getContent();
        $asset->setContent($content);
    }
}
?>