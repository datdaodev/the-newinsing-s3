<?php
namespace inSing\AdminBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;

class ExceptionListener
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        $template = $this->container->get('templating')->render('inSingAdminBundle:Exception:error.html.twig', array(
            'exception' => $exception
        ));

        $response = new Response();
        $response->setContent($template);
        if ($exception->getCode()) {
            $response->setStatusCode($exception->getCode());
            $event->setResponse($response);
        }
    }
}