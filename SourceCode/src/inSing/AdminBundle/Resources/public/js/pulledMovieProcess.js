/**
 * Created by dat on 7/23/15.
 */
$(document).ready(function() {
    $('#btnGetData').on("click", function() {
        var frmName = 'insing_datasourcebundle_pulledmovie';
        var $btn = $(this);
        var link = $btn.data('url');
        var $txtMovieId = $('#' + frmName + '_movieId');
        var $txtMovieTitle = $('#' + frmName + '_movieTitle');
        var $txtareaDescription = $('#' + frmName + '_movieDescription');

        if( (! $txtMovieId.val().match(/^\d+$/)) ) {
            $txtMovieId.val('');
            $txtMovieTitle.val('');
            $txtareaDescription.val('');
            return;
        }

        $btn.button('loading');
        $.ajax({
            method: "POST",
            url: link,
            data: { movie_id: $txtMovieId.val() },
            dataType: 'json'
        })
            .done(function( response ) {
                if(response.status == 200 && response.result.data) {
                    //update input
                    $txtMovieId.val(response.result.data.id);
                    $txtMovieTitle.val(response.result.data.title);
                    $txtareaDescription.val(response.result.data.plot);
                }
                $btn.button('reset');
            });



    });
});