<?php
namespace inSing\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\JsonResponse,
    API\CoreBundle\Util\InsingAPI,
    API\CoreBundle\Util\Helper as APIHelper,
    inSing\ApiBundle\Util\Common,
    FOS\RestBundle\Util\Codes as APICode;


class FeaturesController extends APIBaseController
{

    function getAllFeaturesAction(Request $request)
    {
        $IsAPI = $this->get('IsAPI');
        $data = $IsAPI->getAllFeaturesContent();

        $cache_time = $this->container->getParameter('new_insing_api_cache_lifetime');
        $logger = $this->get("monolog.logger.features_insing_api");
        $routeOptions = $this->currentRoute->getOptions();
        $params = array(
            'function' => 'insing_features_all_content',
            'page_number'   => Common::convertInt($request->query->get('page', 1), 'page'),
            'page_size'     => Common::convertInt($request->query->get('per_page', 10), 'page_size')
        );

        $arrSortBy = array('publish_date', 'last_updated_date');
        if ( isset($params['sort_by']) && (empty($params['sort_by']) || !in_array($params['sort_by'], $arrSortBy)) ) {
            throw new \Exception('Sort by invalid.', strval(APICode::HTTP_BAD_REQUEST));
        }
        $cache = null;
        if ($routeOptions['cache'] == true) {
            $cache = $this->cache;
            $cache_name = $cache->createCacheName($params);
        }

        if ( is_null($cache) || ( $cache && !$cache->hasCache($cache_name))) {
                $IsAPI = $this->get('IsAPI');
                $data = $IsAPI->getAllFeaturesContent();

            if ( !is_null($cache) && is_array($data) && count($data) ) {
                $cache->setCache($cache_name, $data, $cache_time);
            }
        } else {
            $logger->addInfo('===== HAS CACHE Features Content from CACHE ===== with Cache name: ' . $cache_name);
            $data = $cache->getCache($cache_name);
        }
        $result = array();

        $result = array(
            'status' => (string)APICode::HTTP_OK,
            'data' => $data
        );

        return $this->output(APICode::HTTP_OK, $result);

    }

}
