<?php
/**
 * Description of Common
 *
 * @author Vu Tran
 */

namespace inSing\ApiBundle\Util;

use Symfony\Component\DependencyInjection\ContainerAware;

use FOS\RestBundle\Util\Codes;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Collections\ArrayCollection;

class Common extends ContainerAware
{
    /**
     * Convert from int to hash
     * @param int $id
     * @return hash id
     */
    public static function idToUrlId($id) {
        $paddedId = sprintf("%08s", dechex($id));
        $strArr = str_split($paddedId, 2);
        return implode(array_reverse($strArr));
    }

    /**
     * Convert from hash to int
     * @param has $urlId
     * @return int
     */
    public static function urlIdToId($urlId) {
        $strArr = str_split($urlId, 2);
        $paddedId = implode(array_reverse($strArr));
        return hexdec($paddedId);
    }

    /**
     * Convert number to int
     *     If number include letter throw exception
     *     If number is null ignore
     * @author Thu Nguyen <thu.nguyend@s3corp.com.vn>
     * @since  2015-02-12
     * @param  string|int   $number
     * @param  string       $fieldName
     * @param  boolean      $isPositive
     * @return int
     */
    public static function convertInt($number, $fieldName, $isPositive = true){
        if (is_null($number)) return $number;
        $number =  filter_var($number, FILTER_VALIDATE_INT);
        if ($number === false){
            throw new \Exception("$fieldName " . OutputMessage::BE_INTEGER, Codes::HTTP_BAD_REQUEST);
        }
        if ($isPositive && $number <= 0){
            throw new \Exception("$fieldName " . OutputMessage::BE_GREATER_THAN_0, Codes::HTTP_BAD_REQUEST);
        }
        return $number;
    }

    public static function filterInt($var) {
        return intval(filter_var($var, FILTER_VALIDATE_INT));
    }
}
?>
