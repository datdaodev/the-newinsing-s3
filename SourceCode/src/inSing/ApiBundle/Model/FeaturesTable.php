<?php

/**
 * This class contains some functions which use for querying db
*  Connect Session Token Table 
* @author Khuong Dang
*/

namespace inSing\ApiBundle\Model;

use Doctrine\ORM\Query;
use inSing\ApiBundle\Model\Base\NewinSingTable;

class FeaturesTable extends NewinSingTable {
    public function getFeaturesContent(){

        $result = array();
        $query = "SELECT * FROM `content`";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(Query::HYDRATE_ARRAY);
        return $result;

    }
    /*public function addNewSesstionToken($sessionTokenData) {
        $query = "INSERT INTO session_token(session_token,app_id,secret,expired_at,roles)
                    VALUES (
                        '{$sessionTokenData['sessiontoken']}',
                        '{$sessionTokenData['app_id']}',
                        '{$sessionTokenData['secret']}',
                        '{$sessionTokenData['expired_at']}',
                        '{$sessionTokenData['roles']}')
                ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
    }

    public function getSesstionToken($sessionToken) {
        $result = array();
        $query = "SELECT * FROM session_token WHERE session_token = '{$sessionToken}'";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch();
        if (!$result) {
            return array();
        }
        return $result;
    }*/
}
