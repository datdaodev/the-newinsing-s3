<?php
/**
 * Customize assetic filter
 *
 * @author Trung Nguyen
 */
namespace inSing\FrontendBundle\Assetic\Filter;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;

class InSingCssRewrite implements FilterInterface
{
	private $enviroment;

	public function __construct($enviroment)
	{
		$this->enviroment = $enviroment;
	}

    public function filterLoad(AssetInterface $asset)
    {
        $content = $asset->getContent();

        if ($this->enviroment == 'frontend_prod' || $this->enviroment == 'frontend_dev') {
        	$content = str_replace('../images/', '/bundles/insingfrontend/images/', $content);
        }

        $asset->setContent($content);
    }


    public function filterDump(AssetInterface $asset)
    {
        $content = $asset->getContent();
        $asset->setContent($content);
    }
}
?>