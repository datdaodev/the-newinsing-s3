<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MovieShowtimesController extends Controller
{
    public function indexAction()
    {
        //get list cinema chains
        $result = $this
        ->container
        ->get('bind_data_movie')
        ->getCinemaChains();
        echo '<pre>';
        print_r($result);
        //die;

        $result = $this
            ->container
            ->get('bind_data_movie')
            ->getCinemasBy(12);

        var_dump($result);

        die;

        //get showtimes cinema: /2.0/showtimes/cinemas
        //get showtimes: 2.0/showtimes/
        //get movie detail: 2.0/movie/{id}

        $this->get('bind_data_movie')->filterMovieShowTimes();

        return $this->render('inSingFrontendBundle:MovieShowtimes:index.html.twig', array(
                // ...
            ));    }

    public function restaurantNearByBlockAction(Request $request, $cinemaId)
    {
        //cinema detail: https://api.movies.test.insing.com/2.0/cinema/44?use_cache=N&session_token=85db79acc32ad1b815c56295b2687972&sig=a7099e3b4856e520334da0036c43043a
        //$lat, $long of business
        //$neighbourhood of current cinema
        //$this->get('bind_data_movie')->filterMovieShowTimes();

        return $this->render('inSingFrontendBundle:MovieShowtimes:_restaurantnearby.html.twig', array(
            // ...
        ));    }

    public function searchResultAction()
    {
        return $this->render('inSingFrontendBundle:MovieShowtimes:searchResult.html.twig', array(
                // ...
            ));    }

}
