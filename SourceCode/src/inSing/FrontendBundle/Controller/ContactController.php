<?php
namespace inSing\FrontendBundle\Controller;

use Symfony\Component\Form\FormError;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use inSing\FrontendBundle\Form\ContactUsType;
use inSing\FrontendBundle\Entity\ContactUs;

class ContactController extends Controller
{

    /**
     * @author: Vu.Luu
     * get parameter (feedbackType and subjects) from config file and send mail using spool swiftmail
     *   /info/contact-us
     */
    public function contactUsAction(Request $request)
    {
        $contactUs = new ContactUs();
        $contactUsForm = $this->createForm(new ContactUsType($this->container), $contactUs);

        $feedbackTypes = $this->container->getParameter('insing_frontend_feed_back_type');
        $feedbackSubjects = $this->container->getParameter('insing_frontend_feed_back_subject');

        if ($request->getMethod() == 'POST') {

            //binding form
            $contactUsForm->bind($request);

            //check captcha
            $recaptchaPrivateKey = $this->container->getParameter('insing_frontend_recaptcha_private_key');
            //captcha object
            $reCaptcha = $this->container->get('insing.frontend.recaptcha');
            $resp = $reCaptcha->recaptcha_check_answer($recaptchaPrivateKey,
                $request->server->get('REMOTE_ADDR'),
                $request->get('recaptcha_challenge_field'),
                $request->get('recaptcha_response_field'));

            //no input captcha
            if ( $request->get('recaptcha_response_field') == '') {
                $contactUsForm['error_captcha']->addError(new FormError( "Captcha must be required" ));
            } else if (!$resp->is_valid) {
                # set the error code so that we can display it
                $contactUsForm['error_captcha']->addError(new FormError( "Captcha is invalid" ));
            }

            if ($contactUsForm->isValid() && $resp->is_valid) {

                try {
                    $feedbackType = $feedbackTypes[$contactUs->getTypeOfFeedback()];
                    $feedbackSubject = explode('_', $contactUs->getSubject());

                    $emailFrom = $this->container->getParameter('insing_frontend_feedback_email_from');
                    $emailTo = $feedbackSubjects[$feedbackSubject[0]][$feedbackSubject[1]]['email'];
                    $emailSubject = $this->container->getParameter('insing_frontend_feedback_email_subject');

                    $feedback = array(
                        'name' => $contactUs->getName(),
                        'email' => $contactUs->getEmail(),
                        'mobile' => $contactUs->getMobile(),
                        'feedbackType' => $feedbackType,
                        'feedbackSubject' => $feedbackSubjects[$feedbackSubject[0]][$feedbackSubject[1]]['title'],
                        'comments' => html_entity_decode($contactUs->getComments(), ENT_QUOTES, 'UTF-8'),
                    );

                    // Send e-mail
                    $message = \Swift_Message::newInstance()
                        ->setFrom($emailFrom)
                        ->setTo($emailTo)
                        ->setReplyTo( array($feedback['email'] => $feedback['name']) )
                        ->setSubject($emailSubject)
                        ->setBody($this->renderView('inSingFrontendBundle:Contact:feedback-email.html.twig', $feedback));


                    //send mail to spool
                    $this->get('mailer')->send($message);

                    $this->get('session')->getFlashBag()->add('success', 'Your feedback is sent!');
                    return $this->redirect($this->generateUrl('insing_frontend_homepage_contact-us'));

                } catch (\Exception $e) {
                    $this->get('session')->getFlashBag()->add('error', 'We cannot find the subject for your feedback. Please try again.');
                    return $this->redirect($this->generateUrl('insing_frontend_homepage_contact-us'));
                }

            }
        }

        $data = array(
            'contactUsForm' => $contactUsForm->createView()
        );

        return $this->render('inSingFrontendBundle:Info:contact_us.html.twig', $data);
    }

}

