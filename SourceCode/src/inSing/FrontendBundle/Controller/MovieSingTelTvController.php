<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MovieSingTelTvController extends Controller
{
    //1800=30 minutes
    public static $MovieNewsBlockCache = array('key' => 'MOVIE_NEWS_BLOCK_KEY',
        'time' => 1800);

    public static $SingTelTvBlockCache = array('key' => 'SINGTEL_TV_BLOCK_KEY',
        'time' => 1800);

    /**
     * @author Dat.Dao
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $_cache = $this->get('new.insing.cache');
        if (empty($_cache->getCache(self::$SingTelTvBlockCache['key']))) {
            $data = $this->container->get('bind_data_movie')->getSingTelTvData();
            if(! empty($data)) {
                $_cache->setCache($data,
                    self::$SingTelTvBlockCache['key'],
                    self::$SingTelTvBlockCache['time'],
                    true
                );
            }
        } else {
            $data = $_cache->getCache(self::$SingTelTvBlockCache['key']);
        }

        list($a, $h) = $data;

        # TODO print raw data
        var_dump($a, $h);
        $this->_renderMoviesNewsBlockAction($request, 3);


        return $this->render('inSingFrontendBundle:MovieSingTelTv:index.html.twig',
            array('availableNow' => $a,
                'highlightsThisMonth' => $h));
    }

    public function _renderMoviesNewsBlockAction(Request $request, $limit = 3)
    {
        $_cache = $this->get('new.insing.cache');
        if (empty($_cache->getCache(self::$MovieNewsBlockCache['key']))) {
            $data = $this->container->get('bind_data_movie')->getDataMovieNews($limit);
            if(! empty($data)) {
                $_cache->setCache($data,
                    self::$MovieNewsBlockCache['key'],
                    self::$MovieNewsBlockCache['time'],
                    true
                );
            }
        } else {
            $data = $_cache->getCache(self::$MovieNewsBlockCache['key']);
        }

        # TODO print raw data
        var_dump($data);
        return $this->render('inSingFrontendBundle:MovieSingTelTv:_moviesNew.html.twig', array('data' => $data));
    }



}
