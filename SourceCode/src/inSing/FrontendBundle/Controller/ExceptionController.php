<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\TwigBundle\Controller\ExceptionController as BaseExceptionController;
use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;
use Symfony\Component\HttpKernel\Exception\FlattenException;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * A custom ExceptionController
 *
 * @author Vu Tran
 */
class ExceptionController extends BaseExceptionController
{
    protected $securityContext;
    
    protected $session;
    
    protected $logger;

    /**
     * Override [__construct]
     * 
     * @param \Twig_Environment $twig
     * @param bool $debug
     * @param Logger $logger
     */
    public function __construct(\Twig_Environment $twig, $debug, $logger)
    {
        $this->logger = $logger;
        
        parent::__construct($twig, $debug);
    }
    
    /**
     * Inject Security Context service
     * 
     * @param SecurityContext $security_context
     */
    public function setSecurityContext($security_context)
    {
        $this->securityContext = $security_context;
    }
    
    /**
     * Inject Session service
     * 
     * @param Session $session
     */
    public function setSession($session)
    {
        $this->session = $session;
    }
    
    /**
     * Override [showAction]
     * 
     * @author Vu Tran
     * @see \Symfony\Bundle\TwigBundle\Controller\ExceptionController::showAction()
     */
    public function showAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null, $format = 'html')
    {
        if ($token = $this->session->get('_security_secured_area')) {
            $this->securityContext->setToken(unserialize($token));
        }
        
        // log for tracking
        if ($exception->getStatusCode() != 404) {
            $str = strtr('[CODE] MESSAGE', array('CODE' => $exception->getStatusCode(), 'MESSAGE' => $exception->getMessage()));
            $this->logger->addErrorToFile($str);
        }
        
        return parent::showAction($request, $exception, $logger, $format);
    }
    
    /**
     * Override [findTemplate]
     * 
     * @author Vu Tran
     * @see \Symfony\Bundle\TwigBundle\Controller\ExceptionController::findTemplate()
     */
    protected function findTemplate(Request $request, $format, $code, $debug)
    {
        $name    = $debug ? 'exception' : 'error';
        $bundle  = $debug ? 'TwigBundle' : 'inSingFrontendBundle';
        if ($debug && 'html' == $format) {
            $name = 'exception_full';
        }
    
        // when not in debug, try to find a template for the specific HTTP status code and format
        if (!$debug) {

            // hard-code template name
            $templateName = $code == '404' ? $name.'.404' : $name.'.500'; 
            $template = new TemplateReference($bundle, 'Exception', $templateName, $format, 'twig');
            
            if ($this->templateExists($template)) {
                return $template;
            }
        }
        // try to find a template for the given format
        $template = new TemplateReference('TwigBundle', 'Exception', $name, $format, 'twig');
        if ($this->templateExists($template)) {
            return $template;
        }
    
        // default to a generic HTML exception
        $request->setRequestFormat('html');
    
        return new TemplateReference('TwigBundle', 'Exception', $name, 'html', 'twig');
    }
    
}
