<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\JsonResponse;

#use inSing\DataSourceBundle\Lib\UtilHelper;
#use inSing\DataSourceBundle\Utilities\Common;


class HomeController extends Controller
{
    public function indexAction()
    {
        $data= array();
        return $this->render('inSingFrontendBundle:Home:index.html.twig', array(
        ));

    }

    /**
     * @param Request $request
     * @return Response
     * @author Cuong.Au
     */
    public function renderHeaderAction(Request $request)
    {

        /**
         * render user info from profile API
         */
        $this->_renderProfile_user($request);
        /*
         * Get Data
         **/
            /*
             * Get config
             * */
            $hgw_input_text = $this->container->getParameter('new_insing_hgw_input_text');
            $hungry_deal_site = $this->container->getParameter('hungry_deal_site');

            $search_box = $this->get('insing.search_box');
            /*
             * Get HGW cuisine
             * */
            $hgw_cuisine_neighborhood = $search_box->renderHgw_cuisine_neighborhood();
            /*
            * Get Event Category
            * */
            $event_category = $search_box->renderEvent_category();
            /**
             * Get Movies Date
             */
            $movies_date = $search_box->renderMovie_date();
            /**
             * Get Movies showtime
             */
            $movies_showtime = $search_box->renderMovies_showtime();

        return $this->render('inSingFrontendBundle:Layout:_header.html.twig', array(
            'current_uri' => $request->get('current_uri'),
            'hgw_cuisine' => isset($hgw_cuisine_neighborhood['cuisineList']) ? $hgw_cuisine_neighborhood['cuisineList'] : "",
            'hgw_neighborhood' => isset($hgw_cuisine_neighborhood['neighbourhoodList']) ? $hgw_cuisine_neighborhood['neighbourhoodList'] : "",
            'event_category' => $event_category['data'],
            'hgw_input_text'=>$hgw_input_text,
            'movies_date'=>$movies_date,
            'movies_showtime'=>$movies_showtime,
            'hungry_deal_site'=>$hungry_deal_site
        ));

    }

    /**
     * @athor Au Cuong
     */
    private function _renderProfile_user($request)
    {
        if (($user = $this->getUser()) && !$user->isFetchedData()) {
            $userApi = $this->get('profile_api');
            $inputs = array("filter_user_ids" => $user->getId(), 'use_cache' => 'N');
            $message = '';
            $profile = $userApi->getListUsers($inputs, $message);

            if ($profile) {
                $user->setFirstName($profile['first_name']);
                $user->setLastName($profile['last_name']);

                if ($profile['nick_name'] != '') {
                    $user->setDisplayName($profile['nick_name']);
                } else {
                    if($profile['first_name'] !="" && $profile['last_name']){

                        $user->setDisplayName(trim($profile['first_name'] . ' ' . $profile['last_name']));
                    }
                    else{
                        if($profile['facebook_name'] !=""){

                            $user->setDisplayName(trim($profile['facebook_name']));
                        }
                    }
                }

                $user->setAvatar($profile['avatar']);
                $user->setEmail($profile['email']);
                $user->setMobile($profile['mobile']);
                $user->setIsFetchedData(true);
            } else {
            }

            $token = new UsernamePasswordToken($user, null, 'secured_area', array('ROLE_USER'));
            $request->getSession()->set('_security_secured_area', serialize($token));
            $this->get('security.context')->setToken($token);
        }
    }

    /**
     * @return Response
     * @author Cuong.Au
     */
    public function renderFooterAction()
    {
        $data= array();
        return $this->render('inSingFrontendBundle:Layout:_footer.html.twig', array(
        ));

    }

    /**
     *
     * @return Response
     *
     * @author Khai Vu
     */
    public function renderTopMovieAction()
    {
        $prepare_hp_cache = $this->get('insing.prepare_hp_cache');
        $movieModuleData = $prepare_hp_cache->cacheMovieModuleData();

        return $this->render('inSingFrontendBundle:_Partial:_topmovie.html.twig', array(
            'moviesData' => $movieModuleData['movies'],
            'newsData' => $movieModuleData['news']
        ));
    }

    /**
     *
     * @return Response
     *
     * @author Khai Vu
     */
    public function ajaxShowHiddenMovieAction(Request $request)
    {
        if ($request->isXmlHttpRequest())
        {
            $cache = $this->get('new.insing.cache');
            $hiddenKey = $this->container->getParameter('new_insing_hp_movie_module_hidden_cache_key');

            if($cache->checkCache($hiddenKey))
            {
                $movieModuleHiddenData = $cache->getCache($hiddenKey);

                return JsonResponse::create(array(
                    'status'  => Response::HTTP_OK,
                    'data' => $movieModuleHiddenData['news']
                ));
            }

            return JsonResponse::create(array(
                'status'  => Response::HTTP_BAD_REQUEST,
                'message' => 'No result found.'
            ));
        }

        return JsonResponse::create(array(
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'Invalid AJAX request.'
        ));
    }

    /**
     *
     * @return Response
     *
     * @author Khai Vu
     */
    public function renderEventHighLightAction()
    {
        $prepare_hp_cache = $this->get('insing.prepare_hp_cache');
        $eventModuleData = $prepare_hp_cache->cacheEventModuleData();

        return $this->render('inSingFrontendBundle:_Partial:_eventhighlight.html.twig', array(
            'topthingsData' => $eventModuleData['topthings'],
            'supermoduleData' => $eventModuleData['supermodule']
        ));
    }

    /**
     *
     * @return Response
     *
     * @author Khai Vu
     */
    public function ajaxShowHiddenEventAction(Request $request)
    {
        if ($request->isXmlHttpRequest())
        {
            $cache = $this->get('new.insing.cache');
            $hiddenKey = $this->container->getParameter('new_insing_hp_event_module_hidden_cache_key');

            if($cache->checkCache($hiddenKey))
            {
                $eventModuleHiddenData = $cache->getCache($hiddenKey);

                return JsonResponse::create(array(
                    'status'  => Response::HTTP_OK,
                    'data' => $eventModuleHiddenData['supermodule']
                ));
            }

            return JsonResponse::create(array(
                'status'  => Response::HTTP_BAD_REQUEST,
                'message' => 'No result found.'
            ));
        }

        return JsonResponse::create(array(
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'Invalid AJAX request.'
        ));
    }

    public function renderEventSpotAction()
    {
        return $this->render('inSingFrontendBundle:_Partial:_eventspot.html.twig');
    }

    // st Dat.Dao
    /**
     * @author Dat.Dao
     * @return Response
     */
    public function renderCarouselAction() {
        $_logger = $this->get('monolog.logger.homepage');
        $_cache = $this->get('new.insing.cache');        
        //get data from cache
        $cacheKey = $this->container->getParameter('new_insing_hp_carousel_module_cache_key');
        $cacheTime = $this->container->getParameter('new_insing_hp_carousel_module_cache_time');
        $data = $_cache->getCache($cacheKey);
        //debug $data = null;
        if(empty($data)) {
            //get new data and refesh data in cache
            $data = $this->get('insing.prepare_hp_cache')->getDataCarouselModule();
            //echo'<pre>';print_r($data);die;
            if(! empty($data)) {
                //set data into cache
                $_cache->setCache($data, $cacheKey, $cacheTime, true);
                $_logger->debug("renderCarouselAction: set data into cache with cacheKey=[{$cacheKey}], cacheTime=[{$cacheTime}]");
            }
        } else {
            //get data from cache
            $_logger->debug("renderCarouselAction: get data from cache with cacheKey=[{$cacheKey}], cacheTime=[{$cacheTime}]");
        }

        //echo '<pre>';print_r($data);die;
        return $this->render('inSingFrontendBundle:_Partial:_carousel.html.twig',
            array('data' => $data));
    }

    /**
     * @author Dat.Dao
     * @param Request $request
     * @return mixed
     */
    public function ajaxShowHiddenHgwAction(Request $request)
    {
        if ($request->isXmlHttpRequest())
        {
            $cache = $this->get('new.insing.cache');
            $cacheKeyHiden = $this->container->getParameter('new_insing_hp_hgw_module_hidden_cache_key');
            $data = $cache->getCache($cacheKeyHiden);
            $right6 = array();
            if( !empty($data) && !empty($data)) {
                $right6 = $data;
            } else {
                $right6 = $this->get('insing.prepare_hp_cache')->getDataHgwRight6Module();
            }
            $template = $this->renderView('inSingFrontendBundle:_Partial:_item_hgw.html.twig', array('data' => $right6));
            return JsonResponse::create(array(
                'status'  => Response::HTTP_OK,
                'data' => $template
            ));
        }
        return JsonResponse::create(array(
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'Invalid AJAX request.'
        ));
    }

    /**
     * @author Dat.Dao
     * @return Response
     */
    public function renderHgwDealAction() {
        /**
         * 	left: get sg hungry deal(tableDB)(1)
        right: homepage-api hgw module(2) => (list busnessId)
        if count (2) < 8 then {
        Slot 1-2: get from (1)
        Slot 3-5: get from (CMS 37 findContents API) => append (list busnessId)
        Slot 6-8: get from (1)
        }
        (list busnessId) => call businessDetail check "booknow"
         */
        $_logger = $this->get('monolog.logger.homepage');
        $_cache = $this->get('new.insing.cache');
        //get data from cache
        $cacheKey = $this->container->getParameter('new_insing_hp_hgw_module_cache_key');
        $cacheTime = $this->container->getParameter('new_insing_hp_hgw_module_cache_time');
        $data = $_cache->getCache($cacheKey);
        //debug:
        //$data = null;$data['left'] = null;$data['right2'] = null;$data['right6'] = null;
        if(empty($data) || empty($data['left']) || empty($data['right2'])) {
            //get new data and refesh data in cache
            $data = $this->get('insing.prepare_hp_cache')->getDataHgwModule();
            if(! empty($data) && ! empty($data['left']) && ! empty($data['right2']) ) {
                //set data into cache
                $_cache->setCache($data, $cacheKey, $cacheTime, true);
                $_logger->debug("renderHgwDealAction: set data into cache with cacheKey=[{$cacheKey}], cacheTime=[{$cacheTime}]");
            }
            if(! empty($data) && ! empty($data['right6'])) {
                //set hp_hgw_module_hidden
                $cacheKeyHiden = $this->container->getParameter('new_insing_hp_hgw_module_hidden_cache_key');
                $cacheTimeHiden = $this->container->getParameter('new_insing_hp_hgw_module_hidden_cache_time');
                $_cache->setCache($data['right6'], $cacheKeyHiden, $cacheTimeHiden, true);
                $_logger->debug("renderHgwDealAction: set data into cache with cacheKey=[{$cacheKeyHiden}], cacheTime=[{$cacheTimeHiden}]");
            }

        } else {
            //get data from cache
            $_logger->debug("renderHgwDealAction: get data from cache with cacheKey=[{$cacheKey}], cacheTime=[{$cacheTime}]");
        }

        return $this->render('inSingFrontendBundle:_Partial:_hgwdeal.html.twig',
            array('hgwPreparedData' => $data['right2'],
                'leftHgwPreparedData' => $data['left']));
    }
    //ed Dat.Dao
}
