<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class MovieController extends Controller
{
    public function searchAction()
    {
        $movies_api = $this->get('movies_api');
        $result = $movies_api->search();

        return new Response(var_dump($result));
    }

    /**
     * @param Request $request
     * @return Response
     * author: Cuong.au
     */

    public function autoCompleteAction(Request $request)
    {
        // Get request parram
        if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            return die('No direct access allowed.');
        }
        $str = $request->query->get('term');

        if(empty($str))
        {
            throw new NotFoundHttpException('Page Not Found');
        }
        // get service
        $movies_api = $this->get('movies_api');
        // Call apdapter an return String

        //$result = $movies_api->autoComplete($str);
        $result = $movies_api->autoComplete(array('search_term' => $str));
        $data = array();
        if(isset($result['status']) && $result['status']=="200"){
            if(isset($result['data']) && is_array($result['data'])){
                    foreach($result['data'] as $item){
                           $data[]=$item['title'];
                        }
            }
        }
        echo json_encode($result);
        exit();
//        return new Response(var_dump($result));
    }

    public function detailsAction($ids)
    {
        $movies_api = $this->get('movies_api');
        $result = $movies_api->getDetails($ids);

        return new Response(var_dump($result));
    }

    public function newsAction()
    {
        $movies_api = $this->get('movies_api');
        $result = $movies_api->getNews();

        return new Response(var_dump($result));
    }
}
