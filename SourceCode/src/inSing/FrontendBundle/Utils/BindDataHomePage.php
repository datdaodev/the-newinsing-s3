<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/28/15
 * Time: 1:43 PM
 */

namespace inSing\FrontendBundle\Utils;


use inSing\DataSourceBundle\Lib\UtilHelper;
use Symfony\Component\DependencyInjection\Container;

class BindDataHomePage
{
    private $_logger;
    private $_cache;
    private $_container;

    /**
     * @author Dat.Dao
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->_container = $container;
        $this->_cache = $this->_container->get('new.insing.cache');
        $this->_logger = $this->_container->get('monolog.logger.homepage');
    }

    /**
     * @author Dat.Dao
     * @return array|\inSing\ApiAdapterBundle\Channels\Helper\Ambigous
     */
    public function getDataForCarousel()
    {
        //$_logger = $this->get('monolog.logger.homepage');
        $carouselImageSize = $this->_container->getParameter('hp_carousel_module_image_size');
        $common = $this->_container->get('common.utils');
        $data = $this->_getCarouselModule();
        //var_dump($data);die;
        if(empty($data)) {
            return array();
        }
        $dataPrepared = array();
        $helperObject = $this->_container->get('insing.hellper');
        $businessId = array();
        foreach ($data as $item) {
            $desc = $item['description'];
            $item['tags'] = explode('|', $desc);
            //resize image
            $item['imageUrl'] =  $common->generatePhotoFromUrl($item['imageUrl'], $carouselImageSize, $carouselImageSize);
            //1 : movies, 2 : events, 3 : restaurant, 4 : restaurant deals, 5 : hungry deals, 6 : article, 7 : gallery
            //spending: 4 : restaurant deals
            switch($item['content_type_num']) {
                case 1: //movie
                    $item['movie_detail_url'] = $helperObject->urlMovieDetail($item['slug'], $item['item_id']);
                    $item['movie_review_url'] = $helperObject->urlReadReviewInMovie($item['slug'], $item['item_id']);
                    $item['movie_showtime_url'] = $helperObject->urlShowTimeInMovie($item['slug'], $item['item_id']);
                    break;
                case 2: //event
                    $item['event_detail_url'] = $helperObject->urlEventDetail($item['slug'], $item['item_id']);
                    //$item['event_data'] = '';
                    break;
                case 3: //restaurant
                    $item['restaurant_detail_url'] = $helperObject->generateUrlIBL($item['slug']);
                    $item['restaurant_book_url'] = '';
                    $businessId[] = $item['item_id'];
                    break;
                case 5: //hungry deal
                    //not clear
                    $item['hungrydeal_detail_url'] = $helperObject->generateUrlDealDetail($item['slug'], $item['deal_id']);
                    $item['hungrydeal_book_url'] = $helperObject->generateUrlDealDetail($item['slug'], $item['deal_id']);
                    break;
                case 6: //article
                    //only apply for even and movie channnel
                    $item['article_detail_url'] = $helperObject->urlArticleDetailByChannel($item['chanel_name'], $item['slug'], $item['item_id']);
                    break;
                case 7: //gallery
                    //only apply for even and movie channnel
                    $item['gallery_detail_url'] = $helperObject->urlGalleryDetailByChannel($item['chanel_name'], $item['slug'], $item['item_id']);
                    break;
            }
            $dataPrepared[] = $item;
        }
        //call busines detail api => check book-now-url for restaurant
        $businessDetail = $this->_getBusinessDetail($businessId);
        foreach ($dataPrepared as &$v) {
            switch($v['content_type_num']) {
                case 3: //restaurant
                    if( ! empty($businessDetail[$v['item_id']]) ) {
                        $detail = $businessDetail[$v['item_id']];
                        if(! empty($detail['tabledb']['restaurant_id'])) {
                            $v['restaurant_book_url'] = 'post';
                            $v['tabledb_id'] = $detail['tabledb']['restaurant_id'];
                            $v['call_business_detail'] = 1;
                        }
                    }
                    break;
            }
        }
        //var_dump($dataPrepared);die;
        return $dataPrepared;
    }

    /**
     * @author Dat.Dao
     * @return array|\inSing\ApiAdapterBundle\Channels\Helper\Ambigous
     */
    private function _getCarouselModule()
    {
        $apiCaller = $this->_container->get('new_insing_api');
        $params = array('use_cache' => 'N', 'filter' => 'published');

        $time_start = microtime(true);
        $result = $apiCaller->getHomePageCarouselModule($params);
        $time_end = microtime(true);
        $execution_time = $time_end - $time_start;
        $this->_logger->info('API-HOME-PAGE getHomePageCarouselModule Total Execution Time:'.$execution_time.' Seconds');

        if ( (! empty($result)) && ($result['status'] == 200) ) {
            $this->_logger->debug("_getHgwModule: Count: " . count($result));
            //save cache hidden: new_insing_hp_carousel_module_hidden_cache_key
            $_cache = $this->_container->get('new.insing.cache');
            $cacheKey = $this->_container->getParameter('new_insing_hp_carousel_module_hidden_cache_key');
            $cacheTime = $this->_container->getParameter('new_insing_hp_carousel_module_hidden_cache_time');
            $_cache->setCache($result, $cacheKey, $cacheTime, true);
            return $result['data'];
        }
        return null;
    }

    /**
     * @author Dat.Dao
     * @return array|\inSing\ApiAdapterBundle\Channels\Helper\Ambigous
     */
    private function _getSgHunryDeal()
    {
        $limit = 20;//10
        $dataFromCarousel = $this->_parseCarouselModule();
        $pickedNum = 0;
        if (!empty($dataFromCarousel) && !(empty($dataFromCarousel['hungrydeal']))) {
            $pickedNum = count($dataFromCarousel['hungrydeal']);
            $limit += $pickedNum;
            $this->_logger->debug("_getSgHunryDeal: dataFromCarousel " . json_encode($dataFromCarousel));
        }
        //http://sg.tabledb.com/tabledb-web/promotion/search/0/0?perPage=5&displayInHGW=true&partnerCode[]=hgw&countryCode[]=SG&featuredDeal=1&sortField=createdDate&order=DESC
        $params = array('perPage' => $limit,
            'displayInHGW' => 'true',
            'partnerCode[]' => 'hgw',
            'countryCode[]' => 'SG',
            'featuredDeal' => 1,
            'sortField' => 'featuredDeal',
            'order' => 'DESC'
        );

        $apiCaller = $this->_container->get('tabledb_deals_api');
        $caheKey = 'TABLE_DB_SEARCH_PROMOTION_CACHE_KEY';

        $time_start = microtime(true);
        $result = $this->_cache->getCache($caheKey);
        if( empty($result) ) {
            $result = $apiCaller->searchPromotion($params);

            $time_end = microtime(true);
            $execution_time = $time_end - $time_start;
            $this->_logger->info('API-TABLLEDB searchPromotion Total Execution Time:'.$execution_time.' Seconds');

            if (!empty($result['response']['data'])) {
                $caheTime = $this->_container->getParameter('new_insing_tabledb_cache_time');
                $this->_cache->setCache($result, $caheKey, $caheTime, true);
            }
        }

        if (!empty($result['response']['data'])) {
            //check duplicate date with carousel-moudle
            if (($pickedNum > 0)) {
                foreach ($result['response']['data'] as $key => $value) {
                    if (in_array($value['promotion']['id'], $dataFromCarousel['hungrydeal'])) {
                        $this->_logger->debug("_getSgHunryDeal: hunrydeal with id: " . $value['promotion']['id']);
                        unset($result['response']['data'][$key]);
                    }

                }
            }
            $this->_logger->debug("_getSgHunryDeal: Count: " . count($result['response']['data']));
            return $result;
        }
        return null;
    }

    /**
     * @author Dat.Dao
     * @return array|\inSing\ApiAdapterBundle\Channels\Helper\Ambigous
     */
    private function _getHgwModule()
    {
        $apiCaller = $this->_container->get('new_insing_api');
        //$result = $apiCaller->getHomePageCarouselModule();
        $params = array('use_cache' => 'N', 'filter' => 'published');

        $time_start = microtime(true);
        $result = $apiCaller->getHomePageHgwModule($params);
        $time_end = microtime(true);
        $execution_time = $time_end - $time_start;
        $this->_logger->info('API-HOME-PAGE getHomePageHgwModule Total Execution Time:'.$execution_time.' Seconds');

        if ((! empty($result)) && ($result['status'] == 200) ) {
            $this->_logger->debug("_getHgwModule: Count: " . count($result['data']));
            return $result['data'];
        }
        return null;
    }

    /**
     * @author Dat.Dao
     * @return array|\inSing\ApiAdapterBundle\Channels\Helper\Ambigous
     */
    private function _findContent()
    {
        $limit = 3;
        $dataFromCarousel = $this->_parseCarouselModule();
        $pickedNum = 0;
        if (!empty($dataFromCarousel)) {
            $pickedNum = count($dataFromCarousel['article']) + count($dataFromCarousel['gallery']);
            $limit += $pickedNum;
            $this->_logger->debug("_findContent: dataFromCarousel " . json_encode($dataFromCarousel));
        }

        $params = array('channel[]' => 'HungryGoWhere.com'
        , 'page_number' => 1
        , 'page_size' => $limit
        , 'sort_by' => 'publish_date'
//        , 'image_height' => '230'
//        , 'image_width' => '306'
        );
        $apiCaller = $this->_container->get('cms_api');

        $time_start = microtime(true);
        $result = $apiCaller->findContents($params);
        $time_end = microtime(true);
        $execution_time = $time_end - $time_start;
        $this->_logger->info('API-CMS findContents Total Execution Time:'.$execution_time.' Seconds');

        if ((empty($result)) || ($result['result_code'] != 200) ) {
            return null;
        }
        //check duplicate date with carousel-moudle
        if (($pickedNum > 0) && (!empty($result['results']))) {
            foreach ($result['results'] as $key => $value) {
                if ($value['type'] == 0) {
                    //article
                    if (in_array($value['id'], $dataFromCarousel['article'])) {
                        $this->_logger->debug("_findContent: gallery unset article_id: " . $value['id']);
                        unset($result['results'][$key]);
                    }
                }
                if ($value['type'] == 1) {
                    //gallery
                    if (in_array($value['id'], $dataFromCarousel['gallery'])) {
                        $this->_logger->debug("_findContent: gallery unset gallery_id: " . $value['id']);
                        unset($result['results'][$key]);
                    }
                }
            }
        }
        return $result['results'];
    }

    /**
     * @author Dat.Dao
     * @return string
     */
    private function _generateUrlDealDetail($name, $id)
    {
        //http://hungrydeals.sg/cafe-iguana-chinatown-latin-american-mexican/55c37e6fe4b05b12a6462ce9
        return $this->_container->get('insing.hellper')->generateUrlDealDetail($name, $id);
    }

    /**
     * @author Dat.Dao
     * @return array|\inSing\ApiAdapterBundle\Channels\Helper\Ambigous
     */
    private function _autFillFromHungryDeal($slot, $item, $hgwPreparedData)
    {
        //slot: 1-2
        //slot: 6-8
        $getRestaurantItemBy = function ($item, $fieldName, $root = 'restaurant') {
            if (!empty($item[$root][$fieldName])) {
                return $item[$root][$fieldName];
            }
            return '';
        };
        $title = $getRestaurantItemBy($item, 'title', 'promotion');
        $dealId = $getRestaurantItemBy($item, 'id', 'promotion');
        $restaurantSlug = $getRestaurantItemBy($item, 'restaurantSlug');
        //$=>COMFIRMED(CLEAR): the first item of restaurant.images
        //CONFIRM WITH QC: itemDeal->promotion.promotionImage
        $promotionImage = $getRestaurantItemBy($item, 'promotionImage', 'promotion');
        $imageUrl = ( !empty($promotionImage ) ) ? $promotionImage : '';
        //{#item.url_image, item.book_now_url, item.url_detail, item.title, item.description, item.data_from, item.slot#}
        $hgwPreparedData[$slot] = array(
            'url_image' => $imageUrl,
            'book_now_url' => $this->_generateUrlDealDetail($restaurantSlug, $dealId),
            'url_detail' => $this->_generateUrlDealDetail($restaurantSlug, $dealId),
            'title' => $title,
            'description' => $getRestaurantItemBy($item, 'description', 'promotion'),
            'data_from' => 'from_hungrydeal_api',
            'content_type_num' => 5,
            'slot' => $slot
        );
        return $hgwPreparedData;
    }

    /**
     * @author Dat.Dao
     * @return array|\inSing\ApiAdapterBundle\Channels\Helper\Ambigous
     */
    private function _getBusinessDetail(array $ids)
    {
        if (empty($ids)) {
            return null;
        }
        $ids = implode('|', $ids);
        //$ids = '131713';
        $apiCaller = $this->_container->get('search_api');

        $time_start = microtime(true);
        $result = $apiCaller->getBusinessDetails($ids);
        $time_end = microtime(true);
        $execution_time = $time_end - $time_start;
        $this->_logger->info('API-SERACH getBusinessDetails Total Execution Time:'.$execution_time.' Seconds');

        if ( (empty($result)) || ($result['status'] != 200) || empty($result['data']['business'])) {
            return null;
        }
        $data = array();
        foreach ($result['data']['business'] as $value) {
            $data[$value['id']] = $value;
        }
        return $data;
    }

    /**
     * @author Dat.Dao
     * @return array|\inSing\ApiAdapterBundle\Channels\Helper\Ambigous
     */
    private function _autoFillFromCms($slot, $item, $hgwPreparedData)
    {
        //slot: 3-5
        /**
         *=> confirm giup lay image tu (article, gallery) hay tu business da tag vao (article, gallery)
         * article:
         * $=>COMFIRMED(CLEAR): body.default_image
         * gallery:
         * $=>COMFIRMED(CLEAR):
         * neu co default image thi dung
         * neu khong co default image: the first "images"
         */
        //echo '<pre>';print_r($item);die;
        $getContentInItemBy = function ($item, $fieldName) {
            if (!empty($item[$fieldName])) {
                return $item[$fieldName];
            }
            return '';
        };
        $sbusinessIds = $getContentInItemBy($item, 'tags_business_ids');
        //var_dump($sbusinessIds);die;
        $businessIds = explode(",", $sbusinessIds);
        $businessId = (empty($businessIds)) ? '' : $businessIds[0];
        $type = $getContentInItemBy($item, 'type');
        $imageUrl = '';
        $detailPage = '';
        $id = $getContentInItemBy($item, 'id');
        $title = $getContentInItemBy($item, 'title');
        $slugTitle = UtilHelper::getSlug($title);
        $contentTypeNum = 6;
        switch ($type) {
            case 0: //article
                $contentTypeNum = 6;
                $body = $getContentInItemBy($item, 'body');
                if (!empty($body)) {
                    foreach ($body as $v) {
                        if (!empty($v['default_image'])) {
                            $imageUrl = $v['default_image'];
                            break;
                        }
                    }
                }
                $series = $getContentInItemBy($item, 'series');
                $seriesName = count($series) > 0 ? $series[0]['seo_url'] : '';
                $detailPage = $this->_container->get('insing.hellper')->generateUrlArticleDetailInGWH($seriesName, $slugTitle, $id);
                break;
            case 1: //gallery
                $contentTypeNum = 7;
                $images = $getContentInItemBy($item, 'images');
                $imageUrl = ((!empty($images)) && (!empty($images[0]))) ? $images[0]['image_url'] : '';
                $detailPage = $this->_container->get('insing.hellper')->generateUrlGalleryDetailInGWH($slugTitle, $id);
                break;
        }

        //resize image
        $common = $this->_container->get('common.utils');
        $hgwImageSize = $this->_container->getParameter('hp_hgw_module_image_size');
        $imageUrl = (! empty($imageUrl)) ? $common->generatePhotoFromUrl($imageUrl, $hgwImageSize, $hgwImageSize)
                        : '';
        //{#item.url_image, item.book_now_url, item.url_detail, item.title, item.description, item.data_from, item.slot#}
        $hgwPreparedData[$slot] = array(
            'url_image' => $imageUrl,
            'book_now_url' => '',
            'url_detail' => $detailPage,
            'title' => $getContentInItemBy($item, 'title'),
            'description' => $getContentInItemBy($item, 'summary'),
            'data_from' => 'from_cms_api',
            'content_type_num' => $contentTypeNum,
            'slot' => $slot,
            'business_id' => $businessId,
        );
        //var_dump($hgwPreparedData);die;
        return $hgwPreparedData;
    }

    private function getMultipleArticleDetails($articleId) {
        $params = array('article_ids' => implode(',', $articleId));
        $apiCaller = $this->_container->get('cms_api');

        $time_start = microtime(true);
        $result = $apiCaller->getMultipleArticleDetails($params);
        $time_end = microtime(true);
        $execution_time = $time_end - $time_start;
        $this->_logger->info('API-CMS getMultipleArticleDetails Total Execution Time:'.$execution_time.' Seconds');

        if ((empty($result)) || ($result['result_code'] != 200) ) {
            return null;
        }
        //var_dump($result);die;
        $businessId = array();
        //alway return list object
        foreach($result['results'] as $value) {
            $temp = explode(',', $value['tags_business_ids']);
            $id = '';
            if(! empty($temp)) {
                $id = $temp[0];
            }
            $businessId[$value['id']] = $id;
        }
        return $businessId;
    }

    private function getMultipleGalleryDetails($galleryId) {
        $params = array('gallery_id' => implode(',', $galleryId));
        //$params['gallery_id'] .= ',' . '15016502';
        $apiCaller = $this->_container->get('cms_api');

        $time_start = microtime(true);
        $result = $apiCaller->getGalleryDetails($params);
        $time_end = microtime(true);
        $execution_time = $time_end - $time_start;
        $this->_logger->info('API-CMS getGalleryDetails Total Execution Time:'.$execution_time.' Seconds');

        //echo'<pre>';print_r($result['details']);die;
        if ((empty($result)) || ($result['result_code'] != 200) ) {
            return null;
        }
        $businessId = array();
        if(! empty($result['details'][0])) {
            //list object
            foreach($result['details'] as $value) {
                $temp = explode(',', $value['tags_business_ids']);
                //var_dump($temp);
                $id = '';
                if(! empty($temp)) {
                    $id = $temp[0];
                }
                $businessId[$value['id']] = $id;
            }
        } else {
            //a object
            $temp = explode(',', $result['details']['tags_business_ids']);
            //var_dump($temp);
            $id = '';
            if(! empty($temp)) {
                $id = $temp[0];
            }
            $businessId[$result['details']['id']] = $id;
        }

        return $businessId;
    }

    /**
     * @author Dat.Dao
     * @return array|\inSing\ApiAdapterBundle\Channels\Helper\Ambigous
     */
    public function getDataForHgwModule()
    {
        /**
         *    left: get sg hungry deal(tableDB)(1)
         * right: homepage-api hgw module(2) => (list busnessId)
         * if count (2) < 8 then {
         * Slot 1-2: get from (1)
         * Slot 3-5: get from (CMS 37 findContents API) => append (list busnessId)
         * Slot 6-8: get from (1)
         * }
         * (list busnessId) => call businessDetail check "booknow"
         * HGW-MODULE
            content_type_num:
                hungry-deal: 5
                restaurant: 3
                article: 6
                gallery: 7
                restaurant deal: 4 not apply for phase#1
         */
        $leftHgwPreparedData = array();
        $hgwPreparedData = array();
        $common = $this->_container->get('common.utils');
        $hgwImageSize = $this->_container->getParameter('hp_hgw_module_image_size');
        $hgwLeftLimit = $this->_container->getParameter('hp_hgw_module_left_limit');
        $hgwRightSlotLimit = $this->_container->getParameter('hp_hgw_module_right_slot_limit');


        $dataToAutoFill = array();
        $leftDataRaw = $this->_getSgHunryDeal();
        $getRestaurantItemBy = function ($item, $fieldName, $root = 'restaurant') {
            if (!empty($item[$root][$fieldName])) {
                return $item[$root][$fieldName];
            }
            return '';
        };

        if (!empty($leftDataRaw)) {
            foreach ($leftDataRaw['response']['data'] as $value) {
                if (count($leftHgwPreparedData) < $hgwLeftLimit) {
                    $dealId = $getRestaurantItemBy($value, 'id', 'promotion');
                    $tableDbId = $getRestaurantItemBy($value, 'id');
                    $name = $getRestaurantItemBy($value, 'name');
                    $restaurantSlug = $getRestaurantItemBy($value, 'restaurantSlug');
                    $dealName = $getRestaurantItemBy($value, 'title', 'promotion');
                    $leftHgwPreparedData[] = array(
                        'tabledb_id' => $tableDbId,
                        'restaurant_name' => $name,
                        'deal_name' => $dealName,
                        'slug' => $restaurantSlug,
                        'url_deal_detail' => $this->_generateUrlDealDetail($restaurantSlug, $dealId),
                    );
                } else {
                    $dataToAutoFill[] = $value;
                }

            }
        }

        $this->_logger->debug("leftHgwPreparedData: Count: " . count($leftHgwPreparedData));
        $this->_logger->debug("dataToAutoFill: Count: " . count($dataToAutoFill));

        //echo'<pre>';print_r($leftDataRaw);die;
        //echo'<pre>';print_r($leftHgwPreparedData);die;
        //echo'<pre>';print_r($dataToAutoFill);die;
        $hgwPickedData = $this->_getHgwModule();
        //dmd debug
        //$hgwPickedData = null;
        //var_dump($hgwPickedData);die;
        $articleIds = array();
        $galleryIds = array();
        //1 : movies, 2 : events, 3 : restaurant, 4 : restaurant deals, 5 : hungry deals, 6 : article, 7 : gallery
        //apply resize and crop image default: 125%
        $factorZoom = $this->_container->getParameter('hp_factor_zoom');
        if (!empty($hgwPickedData)) {
            foreach ($hgwPickedData as $value) {
                //restaurant, rest deal, article, gallery
                //list articles, list gallery => CMS
                //{#item.url_image, item.book_now_url, item.url_detail, item.title, item.description, item.data_from, item.slot#}
                switch ($value['content_type_num']) {
                    case 6: //article
                        $articleIds[] = $value['item_id'];

                        //resize image
                        $value['url_image'] = $common->generatePhotoFromUrl($value['imageUrl'], $hgwImageSize, $hgwImageSize);
                        $value['business_id'] = '';
                        $value['url_detail'] = $value['contentUrl'];
                        $value['article_id'] = $value['item_id'];
                        $value['book_now_url'] = '';
                        $value['data_from'] = 'from_admin_picked';
                        $hgwPreparedData[$value['slot']] = $value;
                        break;
                    case 7: //gallery
                        $galleryIds[] = $value['item_id'];

                        //resize image
                        $value['url_image'] = $common->generatePhotoFromUrl($value['imageUrl'], $hgwImageSize, $hgwImageSize);
                        $value['business_id'] = '';
                        $value['url_detail'] = $value['contentUrl'];
                        $value['gallery_id'] = $value['item_id'];
                        $value['book_now_url'] = '';
                        $value['data_from'] = 'from_admin_picked';
                        $hgwPreparedData[$value['slot']] = $value;
                        break;
                    case 3: //restaurant
                        //resize image
                        $value['url_image'] = $common->generatePhotoFromUrlByZoom($value['imageUrl'], $hgwImageSize, $factorZoom);
                        $value['business_id'] = $value['item_id'];
                        $value['url_detail'] = $value['contentUrl'];
                        $value['book_now_url'] = '';
                        $value['data_from'] = 'from_admin_picked';
                        $hgwPreparedData[$value['slot']] = $value;
                        break;
                    case 4: //restaurant deal
                        break;

                    case 5: //hungry deal
                        //resize image
                        $value['url_image'] = $value['imageUrl'];
                        $value['business_id'] = $value['item_id'];
                        $value['url_detail'] = $value['contentUrl'];
                        $value['book_now_url'] = $this->_generateUrlDealDetail($value['slug'], $value['deal_id']);
                        $value['data_from'] = 'from_admin_picked';
                        $hgwPreparedData[$value['slot']] = $value;
                        break;
                }
            }
            //call CMS get first business
            //var_dump($articleIds);die;
            //var_dump($hgwPreparedData);die;
            //$articleIds[] = 82445;
            if (!empty($articleIds)) {
                $businessIdFromArticleDetail = $this->getMultipleArticleDetails($articleIds);
                //echo '<pre>';print_r($articleDetail);die;
                foreach($hgwPreparedData as &$v1) {
                    if(! empty($v1['article_id']) && ( ! empty($businessIdFromArticleDetail[$v1['article_id']])) ) {
                        $v1['business_id'] = $businessIdFromArticleDetail[$v1['article_id']];
                    }
                }
            }
            if (!empty($galleryIds)) {
                $galleryDetail = $this->getMultipleGalleryDetails($galleryIds);
                //echo '<pre>';print_r($galleryDetail);die;
                foreach($hgwPreparedData as &$v2) {
                    if(! empty($v2['gallery_id']) && ( ! empty($galleryDetail[$v2['gallery_id']])) ) {
                        $v2['business_id'] = $galleryDetail[$v2['gallery_id']];
                    }
                }
                //echo '<pre>';print_r($galleryDetail);die;
            }
        //echo '<pre>';print_r($hgwPreparedData);die;

        }
        $this->_logger->debug("start: hgwPreparedData: Count: " . count($hgwPreparedData));
        //echo '<pre>'; print_r($hgwPreparedData);die;
        //st auto fill
        if ((count($hgwPreparedData) < $hgwRightSlotLimit) || (! $hgwPreparedData) ) {
            //auto fill
            if ((empty($hgwPreparedData[1])) && count($dataToAutoFill) >= 1) {
                //slot 1
                //Shift an element off the beginning of array
                $item = array_shift($dataToAutoFill);
                $hgwPreparedData = $this->_autFillFromHungryDeal(1, $item, $hgwPreparedData);
                $this->_logger->debug("auto fill slot1: ");
            }
            if ((empty($hgwPreparedData[2])) && count($dataToAutoFill) >= 1) {
                //slot 2
                //Shift an element off the beginning of array
                $item = array_shift($dataToAutoFill);
                $hgwPreparedData = $this->_autFillFromHungryDeal(2, $item, $hgwPreparedData);
                $this->_logger->debug("auto fill slot2: ");
            }
            //echo '<pre>';print_r($hgwPreparedData);die;
            if ( (empty($hgwPreparedData[3])) || (empty($hgwPreparedData[4]))  || (empty($hgwPreparedData[5]))) {
                //auto fill slot : 3-5
                //get from (CMS 37 findContents API) => append (list busnessId)
                $contents = $this->_findContent();
                //echo '<pre>';var_dump($contents);die;
                if (!empty($contents)) {
                    if( empty($hgwPreparedData[3]) ) {
                        //slot 3
                        $item = array_shift($contents);
                        $hgwPreparedData = $this->_autoFillFromCms(3, $item, $hgwPreparedData);
                        $this->_logger->debug("auto fill slot3: ");
                    }

                    if( empty($hgwPreparedData[4]) ) {
                        if ((count($contents) >= 1)) {
                            //slot 4
                            $item = array_shift($contents);
                            $hgwPreparedData = $this->_autoFillFromCms(4, $item, $hgwPreparedData);
                            $this->_logger->debug("auto fill slot4: ");
                        }
                    }
                    if( empty($hgwPreparedData[5]) ) {
                        if ((count($contents) >= 1)) {
                            //slot 5
                            $item = array_shift($contents);
                            $hgwPreparedData = $this->_autoFillFromCms(5, $item, $hgwPreparedData);
                            $this->_logger->debug("auto fill slot5: ");
                        }
                    }
                }
            }

            //echo '<pre>';print_r($hgwPreparedData);die;
            if ((empty($hgwPreparedData[6])) && count($dataToAutoFill) >= 1) {
                //slot 6
                //Shift an element off the beginning of array
                $item = array_shift($dataToAutoFill);
                $hgwPreparedData = $this->_autFillFromHungryDeal(6, $item, $hgwPreparedData);
                $this->_logger->debug("auto fill slot6: ");
            }

            if ((empty($hgwPreparedData[7])) && count($dataToAutoFill) >= 1) {
                //slot 7
                //Shift an element off the beginning of array
                $item = array_shift($dataToAutoFill);
                $hgwPreparedData = $this->_autFillFromHungryDeal(7, $item, $hgwPreparedData);
                $this->_logger->debug("auto fill slot7: ");
            }

            if ((empty($hgwPreparedData[8])) && count($dataToAutoFill) >= 1) {
                //slot 8
                //Shift an element off the beginning of array
                $item = array_shift($dataToAutoFill);
                $hgwPreparedData = $this->_autFillFromHungryDeal(8, $item, $hgwPreparedData);
                $this->_logger->debug("auto fill slot8: ");
            }

        }
        //ed auto fill

        //var_dump($leftHgwPreparedData);
        //echo '<pre>'; print_r($hgwPreparedData);die;
        //checking url_image and url_book_now
        $listBusinessIds = array();
        //for($i = 1; $i <= count($hgwPreparedData); $i++) {
        foreach ($hgwPreparedData as $v3) {
            if( (!empty($v3['business_id'])) ) {
                //article or gallery from api admin picked and auto fill from CMS
                //call business detail api
                $listBusinessIds[] = $v3['business_id'];
            }
        }
        //echo '<pre>'; print_r($hgwPreparedData);die;
        //fill url_deal_detail, tabledb_id, restaurant_name_slug, is_book_now for{"admin-picked", "article", "gallery"}
        $this->_logger->debug("all business_id: " . json_encode($listBusinessIds));
        $businessDetail = $this->_getBusinessDetail($listBusinessIds);
        //echo '<pre>'; print_r($businessDetail);die;
        //echo '<pre>'; print_r($hgwPreparedData);die;
        if (!empty($businessDetail)) {
            //fields: book_now_url, tabledb_id, slug, deal_id,
            //var_dump($businessDetail);die;
            foreach ($hgwPreparedData as &$itemSlot) {
                if( empty($itemSlot['book_now_url'])) {
                    if(! empty($businessDetail[$itemSlot['business_id']]) ) {
                        $item = $businessDetail[$itemSlot['business_id']];
                        //var_dump($item);die;
                        $itemSlot['tabledb_id'] = $item['tabledb']['restaurant_id'];
                        //book the same web hgw
                        if(! empty($itemSlot['tabledb_id'])) {
                            $itemSlot['book_now_url'] = 'post';
                        }
                        //required field
                        $itemSlot['slug'] = $item['hgw']['hgw_url'];
                        $itemSlot['call_business_detail'] = 1;
                    }
                }

            }
        }

        //sort slot asc
        ksort($hgwPreparedData);
        //echo '<pre>'; print_r($hgwPreparedData);die;
        $this->_logger->debug("end: hgwPreparedData: Count: " . count($hgwPreparedData));
        //$this->_logger->debug("end: hgwPreparedData: Data: " . json_encode($hgwPreparedData));
        $this->_logger->debug("------------------------------------------------------");
        $right2 = array();
        $right6 = array();
        if (count($hgwPreparedData) >= 1) {
            $right2[] = array_shift($hgwPreparedData);
        }
        if (count($hgwPreparedData) >= 1) {
            $right2[] = array_shift($hgwPreparedData);
        }
        if(! empty($hgwPreparedData)) {
            $right6 = $hgwPreparedData;
        }
        return array('left' => $leftHgwPreparedData, 'right2' => $right2, 'right6' => $right6);
    }

    private function _parseCarouselModule()
    {
        //1 : movies, 2 : events, 3 : restaurant, 4 : restaurant deals, 5 : hungry deals, 6 : article, 7 : gallery
        //Article and Gallery
        $cacheKey = $this->_container->getParameter('new_insing_hp_carousel_module_cache_key');
        $data = $this->_cache->getCache($cacheKey);
        if (empty($data)) {
            return null;
        }
        $article = array();
        $gallery = array();
        $hungrydeal = array();//deal_id
        foreach ($data as $item) {
            switch ($item['content_type_num']) {
                case 5: //hungry deals
                    $hungrydeal[] = $item['deal_id'];
                    break;
                case 6: //Article
                    $article[] = $item['item_id'];
                    break;

                case 7: //Gallery
                    $gallery[] = $item['item_id'];
                    break;
            }
        }
        return array('article' => $article, 'gallery' => $gallery, 'hungrydeal' => $hungrydeal);
    }

}