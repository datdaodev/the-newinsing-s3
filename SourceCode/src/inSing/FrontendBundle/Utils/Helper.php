<?php
namespace inSing\FrontendBundle\Utils;
use inSing\DataSourceBundle\Lib\UtilHelper;
use Symfony\Component\DependencyInjection\Container;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/24/15
 * Time: 4:45 PM
 */
class Helper
{

    /**
     * @author Dat.Dao
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->_container = $container;
    }

    /**
     * @author Dat.Dao
     * @param $str
     * @param $length
     * @return string
     */
    public function truncateWordsWith($text, $length) {
        $length = abs((int)$length);
        if(strlen($text) > $length) {
            $text = preg_replace("/^(.{1,$length})(\\s.*|$)/s", '\\1', $text);
        }
        return($text);
    }

    public static function cleanUrl($url)
    {
        $url = str_replace(array("?", "?"), "a", $url); // Additional Swedish filter
        $url = str_replace(array("?", "?"), "a", $url); // Additional Swedish filter
        $url = str_replace(array("?", "?"), "o", $url); // Additional Swedish filter

        $url = preg_replace('/[^a-z0-9\s\-]/i', "", $url); // Remove special characters
        $url = preg_replace('/\s\s+/', " ", $url); // Replace multiple spaces with one space
        $url = trim($url); // Remove trailing spaces
        $url = preg_replace('/\s/', "-", $url); // Replace all spaces with hyphens
        $url = preg_replace('/\-\-+/', "-", $url); // Replace multiple hyphens with one hyphen
        $url = preg_replace('/^\-|\-$/', "", $url); // Remove leading and trailing hyphens
        $url = strtolower($url);
        return $url;
    }

    /**
     * @author Dat.Dao
     * @return string
     */
    public function generateUrlDealDetail($slug, $id) {
        //http://hungrydeals.sg/cafe-iguana-chinatown-latin-american-mexican/55c37e6fe4b05b12a6462ce9
        $format = $this->_container->getParameter('hungry_deal_detail');
        $url = str_replace('{slug_restaurant_name}', $slug, $format);
        return str_replace('{tabledb_id}', $id, $url);
    }

    public function generateUrlIBL($slug) {
        //insing_content_carousel_restaurant
        $format = $this->_container->getParameter('insing_content_carousel_restaurant');
        $url = str_replace('{restaurant_slug}', $slug, $format);
        return $url;
    }

    public function generateUrlArticleDetailInGWH($seriname, $slug, $id) {
        //http://insing.com/features/{slug}/id-{hexID}
        //http://sg.staging.hungrygowhere.com/dining-guide/{series_name}/{slug}-*aid-{hexID}/
        $id = UtilHelper::idToUrlId($id);
        $url = $this->_container->getParameter('insing_content_hgw_article_detail');
        $url = str_replace('{series_name}', $seriname, $url);
        $url = str_replace('{slug}', $slug, $url);
        $url = str_replace('{hexID}', $id, $url);
        return $url;
    }
    public function generateUrlGalleryDetailInGWH($slug, $id) {
        //http://insing.com/gallery/{slug}/id-{hexID}
        //http://sg.staging.hungrygowhere.com/gallery/{gallery_slug}-*gid-{hexID}/
        $id = UtilHelper::idToUrlId($id);
        $url = $this->_container->getParameter('insing_content_hgw_gallery_detail');
        $url = str_replace('{gallery_slug}', $slug, $url);
        $url = str_replace('{hexID}', $id, $url);
        return $url;
    }

    public function getChannelName ($channelName) {
        if (preg_match("/event/i", $channelName)) {
            return 'event';
        }
        if (preg_match("/movie/i", $channelName)) {
            return 'movie';
        }
        if (preg_match("/hungrygowhere/i", $channelName)) {
            return 'hungrygowhere';
        }
        if (preg_match("/insing/i", $channelName)) {
            return 'insing';
        }
        if (preg_match("/shopping/i", $channelName)) {
            return 'shopping';
        }
        return $channelName;
    }

    public function urlArticleDetailByChannel($channelName, $slug, $id) {
        /*
        Article:
        movie: http://movies.test.insing.com/feature/legend-of-the-guardians-heroes-to-hoot-for/id-6e192800/
            /feature/{title}/id-{reverseHexId}/
		event: http://events.test.insing.com/feature/things-to-do-with-kids-under-10-dollars/id-40ca0100/
			pattern:/feature/{title}/id-{reversedHexId}/
        */
        //default movie
        $url = $this->_container->getParameter('article_detail_movie');
        if( $this->getChannelName($channelName) == 'event') {
            $url = $this->_container->getParameter('article_detail_event');
        }
        $id = UtilHelper::idToUrlId($id);
        $url = str_replace('{slug}', $slug, $url);
        $url = str_replace('{hexID}', $id, $url);
        return $url;
    }

    public function urlGalleryDetailByChannel($channelName, $slug, $id) {
        /*
        Gallery:
		movie: http://movies.test.insing.com/gallery/divergent/id-aa2e3101/
			pattern:/gallery/{title}/id-{reversedHexId}/
		event: http://events.test.insing.com/gallery/test-evergreen-gallery-events/id-ba3c3101/
			pattern:/gallery/{title}/id-{reversedHexId}/
        */
        //default movie
        $url = $this->_container->getParameter('gallery_detail_movie');
        if( $this->getChannelName($channelName) == 'event') {
            $url = $this->_container->getParameter('gallery_detail_event');
        }
        $id = UtilHelper::idToUrlId($id);
        $url = str_replace('{slug}', $slug, $url);
        $url = str_replace('{hexID}', $id, $url);
        return $url;
    }

    public function urlMovieDetail($slug, $id) {
        /*
        Movie: external link movie detail
		ex:	http://movies.insing.com/movie/inside-out-2015/id-e8e50000/
		pattern: /movie/{title}/id-{id}/
		input: title, movieId
        */
        $url = $this->_container->getParameter('insing_content_carousel_movies_detail');
        $id = UtilHelper::idToUrlId($id);
        $url = str_replace('{slug}', $slug, $url);
        $url = str_replace('{hexID}', $id, $url);
        return $url;
    }

    public function urlReadReviewInMovie($slug, $id) {
        /*
        Read Review:
        ex: http://movies.insing.com/movie/inside-out-2015/id-e8e50000/reviews/
        pattern: /movie/{title}/id-{movieReverseHexId}/reviews/
        input: title, movieId
        */
        $url = $this->_container->getParameter('read_review_movie');
        $id = UtilHelper::idToUrlId($id);
        $url = str_replace('{slug}', $slug, $url);
        $url = str_replace('{hexID}', $id, $url);
        return $url;
    }

    public function urlShowTimeInMovie($slug, $id) {
        /*
        Show times available:
        ex: http://movies.insing.com/movie/inside-out-2015/id-e8e50000/showtimes/
        pattern: /movie/{title}/id-{id}/showtimes/
        input: title, movieId
        */
        $url = $this->_container->getParameter('show_time_movie');
        $id = UtilHelper::idToUrlId($id);
        $url = str_replace('{slug}', $slug, $url);
        $url = str_replace('{hexID}', $id, $url);
        return $url;
    }

    public function urlEventDetail($slug, $id) {
        /*
        Events: external link event detail
		ex: http://events.insing.com/event/heineken-cities-festival-2015/id-12980000/
		pattern:  /event/{title}/id-{reversedHexId}/
		input: title, id
        */
        $url = $this->_container->getParameter('insing_content_carousel_events_detail');
        $id = UtilHelper::idToUrlId($id);
        $url = str_replace('{slug}', $slug, $url);
        $url = str_replace('{hexID}', $id, $url);
        return $url;
    }

    /**
     * @author Dat.Dao
     * @param $url
     * @return null|\SimpleXMLElement
     */
    public function getDataRawFromXmlUrl($url) {
        //$url = 'http://singteltv.com.sg/xml/moviehighlight_insing.xml';
        //$url = 'http://movies.insing.com/movies-list.xml';
        try {
            $logger = $this->_container->get('monolog.logger.singteltv');
            $time_start = microtime(true);
            $xmlToString = file_get_contents($url);
            $time_end = microtime(true);
            $execution_time = $time_end - $time_start;
            $logger->info('st--------------------');
            $logger->info('API-SINGTEL-TV FULL URL: ' . $url);
            $logger->info('API-SINGTEL-TV getDataXml Total Execution Time:'
                . $execution_time.' Seconds');
            $logger->info('ed--------------------');
            $xml = simplexml_load_string($xmlToString, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $array = json_decode($json,TRUE);
            return $array;
        } catch (\Exception $ex) {
            $logger->info('API-SINGTEL-TV getDataXml FAILED:'
                . $ex->getMessage());
            return null;
        }
    }

    /**
     * @author Dat.Dao
     * @param array $raw
     * @return array|null
     */
    public function convertSingTelTvXmlToArrayIdIndex(array $raw) {
        if(empty($raw) || empty($raw['movie'])) {
            return null;
        }
        $output = array();
        foreach ($raw['movie'] as $v) {
            $output[$v['@attributes']['id']] = $v['@attributes'];
        }
        return $output;
    }

}