<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/2/15
 * Time: 4:46 PM
 */

namespace inSing\FrontendBundle\Utils;


use inSing\ApiAdapterBundle\Channels\MoviesUriMapping;
use inSing\DataSourceBundle\Utilities\UtilHelper;
use Symfony\Component\DependencyInjection\Container;

/**
 * @author Dat.Dao
 * Class BindDataMovie
 * @package inSing\FrontendBundle\Utils
 */
class BindDataMovie
{
    public static $availableNow = array('type' => 'available now',
        'max' => 4);
    public static $highLight = array('type' => 'highlight',
        'max' => 4);

    /**
     * @author Dat.Dao
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->_container = $container;
    }

    /**
     * @author Dat.Dao
     * @return array
     */
    public function getSingTelTvData()
    {
        $raw = $this->_container
            ->get('insing.hellper')
            ->getDataRawFromXmlUrl($this->_container
                ->getParameter('movie_singtel_tv_xml_url'));
        $result = $this->_container
            ->get('insing.hellper')
            ->convertSingTelTvXmlToArrayIdIndex($raw);
        if (empty($result)) {
            return null;
        }

        $idAvailableNow = array();
        $idHighlightsThisMonth = array();
        foreach ($result as $m) {
            if (strtolower($m['type']) == self::$availableNow['type']
                && count($idAvailableNow) < self::$availableNow['max']
            ) {
                $idAvailableNow[] = $m['id'];
            }
            if (strtolower($m['type']) == self::$highLight['type']
                && count($idHighlightsThisMonth) < self::$highLight['max']
            ) {
                $idHighlightsThisMonth[] = $m['id'];
            }
        }
        $data = $this->_getDataMoviePoster(array_merge($idAvailableNow, $idHighlightsThisMonth));

        $availableNow = array();
        $highlightsThisMonth = array();
        foreach ($data as $n) {
            if (in_array($n['id'], $idAvailableNow)) {
                $availableNow[] = $n;
            }
            if (in_array($n['id'], $idHighlightsThisMonth)) {
                $highlightsThisMonth[] = $n;
            }
        }
        return array($availableNow, $highlightsThisMonth);
    }

    /**
     * @author Dat.Dao
     * @param array $movieId
     * @param int $perPage
     * @return array|null
     */
    private function _getDataMoviePoster(array $movieId, $perPage = 8) {
        $movieId = array_unique($movieId);
        $params = array('use_cache' => 'N',
            'reviews_included' => 'y',
            'reviews_page' => 1,
            'reviews_page_size' => 1,
            'page' => 1,
            'per_page' => $perPage,
        );

        $result = $this
            ->_container
            ->get('movies_api')
            ->getDetailById($movieId, $params);
        if( empty($result) || $result['status'] != 200 || empty($result['data'])) {
            return null;
        }
        $output = array();
        //echo '<pre>';print_r($result['data']);die;
        $get = function ($item, $root, $sub = '') {
            if(empty($item[$root])) {
                return '';
            }
            if(! empty($sub)) {
                if(empty($item[$root][$sub])) {
                    return '';
                }
                return $item[$root][$sub];
            }
            return $item[$root];


        };
        /**
         *  1. Movie Poster: poster_image
            # TODO confirm: title xml or from api
            2. Movie Title: title (confirm: title xml or from api)
            3. Total No. of Reviews: rnr_stats.review_count
            4. Rating: rnr_stats.overall_rating
         *  5. Movie Id: id
         */
        foreach($result['data'] as $m) {
            # TODO confirm poster_image and default image
            $image  = $get($m, 'poster_image', 'url');
            if( empty($image)) {
                $casting = $get($m, 'casting');
                if(! empty($casting)) {
                    if(! empty($casting[0]['celebrity']['image_url'])) {
                        $image = $casting[0]['celebrity']['image_url'];
                    }
                }
            }

            # TODO resize image on CDN
            //WxH = 181x272
            //http://cdn.test.insing.com/cms/55/12/4b/8c/88791/8387d10266_190x272_cb5f215edc.jpg
//            if(! empty($image)) {
//                $hgwImageSize = $this->_container->getParameter('hp_hgw_module_image_size');
//                $image = $this->_container
//                    ->get('common.utils')
//                    ->generatePhotoFromUrl($image, $hgwImageSize, $hgwImageSize);
//            }
            $output[] = array(
                'id' => $m['id'],
                'image' => $image,
                'title' => $m['title'],
                'total_no_review' => $get($m, 'rnr_stats', 'review_count'),
                'rating' => $get($m, 'rnr_stats', 'overall_rating'),
                );
        }

        return $output;
    }

    /**
     * @author Dat.Dao
     * @param $limit
     * @return array|null
     */
    public function getDataMovieNews($limit) {
        #TODO call cms
        $params = array('channel[]' => 'Movies'
        , 'page_number' => 1
        , 'page_size' => $limit
        , 'sort_by' => 'last_updated_date'
        , 'tags[]' => 'movie-news'
        );
        $result = $this
            ->_container
            ->get('cms_api')
            ->findContents($params);
        //echo '<pre>';print_r($result);die;
        if( empty($result) || $result['result_code'] != 200 || empty($result['results'])) {
            return null;
        }

        $getContentInItemBy = function ($item, $fieldName) {
            if (!empty($item[$fieldName])) {
                return $item[$fieldName];
            }
            return '';
        };
        /**
         * 1. image
         * 2. title
         * 3. description
         * 4. photo
         * 5. link(article or gallery for MOVIES channel{slug, hexId})
         */
        $output = array();
        foreach($result['results'] as $value) {
            $title = $getContentInItemBy($value, 'title');
            $id = $getContentInItemBy($value, 'id');
            $imageUrl = '';
            if ($value['type'] == 0) { //article
                $body = $getContentInItemBy($value, 'body');
                if (!empty($body)) {
                    foreach ($body as $v) {
                        if (!empty($v['default_image'])) {
                            $imageUrl = $v['default_image'];
                            break;
                        }
                    }
                }
                $routerName = 'insing_frontend_article_details';
            } else if ($value['type'] == 1) { //gallery
                $images = $getContentInItemBy($value, 'images');
                $imageUrl = ((!empty($images)) && (!empty($images[0]))) ? $images[0]['image_url'] : '';
                $routerName = 'insing_frontend_gallery';
            }
            # TODO resize image on CDN
            if(! empty($imageUrl)) {
                $hgwImageSize = $this->_container->getParameter('hp_hgw_module_image_size');
                $imageUrl = $this->_container
                    ->get('common.utils')
                    ->generatePhotoFromUrl($imageUrl, $hgwImageSize, $hgwImageSize);
            }

            $linkDetail = $this->_container->get('router')->generate($routerName,
                array('slug' => \inSing\DataSourceBundle\Lib\UtilHelper::getSlug($title),
                    'hexId' => \inSing\DataSourceBundle\Lib\UtilHelper::idToUrlId($id)),
                true);
            # TODO confirm count words description
            $output[] = array('id' => $id,
                'title' => $title,
                'description' => $getContentInItemBy($value, 'summary'),
                'photo' => $imageUrl,
                'linkDetail' => $linkDetail
                );
        }
        return $output;
    }

    private function _getCinemaInfo($id) {
        $result = $this
            ->_container
            ->get('movies_api')
            ->getCinemaDetail($id,
                array('use_cache' => 'N',
                    'include_business_details' => 'y',
                ));

        echo '<pre>';print_r($result);die;
        if( empty($result) || $result['status'] != 200 || empty($result['data'])) {
            return null;
        }

    }

    public function getCinemasBy($chainId) {
        //get list cinema chains
        $result = $this
            ->_container
            ->get('movies_api')
            ->getListCinemas(array('filter_cinema_chain_id' => $chainId,
                'page' => 1,
                'per_page' => 20 ));

        //echo '<pre>';print_r($result);die;
        if( empty($result) ) {
            return null;
        }
        $output = array();
        foreach($result as $c) {
            if($c['status'] == MoviesUriMapping::ACTIVE_STATUS) {
                unset($c['aliases']);
                unset($c['cinema_chain']);
                $output[] = $c;
            }
        }
        return $output;
    }

    public function getCinemaChains($params = array()) {
        //get list cinema chains
        $result = $this
            ->_container
            ->get('movies_api')
            ->getListCinemaChains($params);

        //echo '<pre>';print_r($result);
        if( empty($result) || $result['status'] != 200 || empty($result['data'])) {
            return null;
        }
        $output = array();
        foreach($result['data'] as $c) {
            if($c['status'] == MoviesUriMapping::ACTIVE_STATUS) {
                unset($c['aliases']);
                $output[] = $c;
            }
        }
        return $output;
    }

    public function filterMovieShowTimes($cinemaId = null) {
        //routing old code movie: InsingMoviesBundle_movies_showtimes_by_cinema
        $cinemaId = '66000000';'2c000000';
        if(! empty($cinemaId)) {
            $cinemaId = \inSing\DataSourceBundle\Lib\UtilHelper::urlIdToId($cinemaId);
        } else {
            /**
             * $moviesData = $movies_api->search(array('use_cache' => 'N',
            'sort_fields' => 'screenings',
            'filter_showtimes' => 'now showing',
            'sort_reverse' => 'Y',
            'page' => 1,
            'per_page' => 5 + $numOfCarouselData
            ));
             */
        }

        //call show time
        //default_country_code: SG  # Default country, used in movie classification
        //default_country_id: 1
        // Check for today's showtimes
        $now = new \DateTime();
        $roundedMin = floor(intval($now->format('i')) / 5) * 5;
        $start_format = sprintf('Y-m-d H:%02d:00', $roundedMin);
        $end_format = 'Y-m-d 23:59:59';
        $params = array(
            'filter_country_id' => 1,
            'filter_cinema_ids' => $cinemaId,
            'filter_session_on_start' => $now->format($start_format),
            'filter_session_on_end' => $now->format($end_format),
            'page' => 1,
            'per_page' => 100,
        );

        $resultShowTimes = $this
            ->_container
            ->get('movies_api')
            ->getShowtmes($params);

        if( empty($resultShowTimes) || $resultShowTimes['status'] != 200 || empty($resultShowTimes['data'])) {
            return null;
        }

        //call movie details
        $movieIds = array();
        foreach ($resultShowTimes['data'] as $s) {
            $movieIds[] = $s['movie']['id'];
        }
        $movieIds = array_unique($movieIds);
        $resultMovies = $this
            ->_container
            ->get('movies_api')
            ->getDetailById($movieIds,
                array('use_cache' => 'N',
                'reviews_included' => 'y',
                'reviews_page' => 1,
                'reviews_page_size' => 1,
                    ));

        if( empty($resultMovies) || $resultMovies['status'] != 200 || empty($resultMovies['data'])) {
            return null;
        }

        if(! empty($cinemaId)) {
            //call CINEMA DETAILS API
            $cinemaInfo = $this->_getCinemaInfo($cinemaId);
            //call business detail => lat,long
        }



        echo '<pre>';
        print_r($resultShowTimes);
        //print_r($resultMovies);
        die;
    }
}