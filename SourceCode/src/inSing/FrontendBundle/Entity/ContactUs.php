<?php

    namespace inSing\FrontendBundle\Entity;

    class ContactUs
    {
        protected $name;
        protected $email;
        protected $mobile;
        protected $typeOfFeedback;
        protected $subject;
        protected $comments;

        public function __construct()
        {
        }

        public function __toString() {
            return $this->subject;
        }

        public function getName() {
            return $this->name;
        }
        public function setName($name) {
            $this->name = filter_var($name, FILTER_SANITIZE_STRING);
        }
        public function getEmail() {
            return $this->email;
        }
        public function setEmail($email) {
            $this->email = filter_var($email, FILTER_SANITIZE_EMAIL);
        }
        public function getMobile() {
            return $this->mobile;
        }
        public function setMobile($mobile) {
            $this->mobile = filter_var($mobile, FILTER_SANITIZE_STRING);
        }
        public function getTypeOfFeedback() {
            return $this->typeOfFeedback;
        }
        public function setTypeOfFeedback($typeOfFeedback) {
            $this->typeOfFeedback = filter_var($typeOfFeedback, FILTER_SANITIZE_STRING);
        }
        public function getSubject() {
            return $this->subject;
        }
        public function setSubject($subject) {
            $this->subject = filter_var($subject, FILTER_SANITIZE_STRING);
        }
        public function getComments() {
            return $this->comments;
        }
        public function setComments($comments) {
            $this->comments = filter_var($comments, FILTER_SANITIZE_STRING);
        }
    }
