jQuery(document).ready(function($) {
    // We only want these styles applied when javascript is enabled
    $('div.content-slideshow').css('display', 'block');
    // Initially set opacity on thumbs and add
    // additional styling for hover effect on thumbs
    var onMouseOutOpacity = 1.0;

    $('div.navigation a.pageLink').opacityrollover({
        mouseOutOpacity: onMouseOutOpacity,
        mouseOverOpacity: 1.0,
        fadeSpeed: 'fast',
        exemptionSelector: '.selected'
    });
    $('div.div_printer').click(function() {
        $('.content').show().printElement();
    });

    // Initialize Advanced Galleriffic Gallery
    var gallery = $('#thumbs').galleriffic({
        delay: 5000,
        numThumbs: 12,
        preloadAhead: 10,
        enableTopPager: false,
        enableBottomPager: false,
        imageContainerSel: '#slideshow',
        controlsContainerSel: '',
        captionContainerSel: '#caption',
        loadingContainerSel: '#loading',
        renderSSControls: true,
        renderNavControls: true,
        playLinkText: 'Play Slideshow',
        pauseLinkText: 'Pause Slideshow',
        prevLinkText: '&lsaquo; Previous Photo',
        nextLinkText: 'Next Photo &rsaquo;',
        nextPageLinkText: 'Next &rsaquo;',
        prevPageLinkText: '&lsaquo; Prev',
        enableHistory: true,
        autoStart: false,
        syncTransitions: true,
        defaultTransitionDuration: 900,
        isGrid: true,
        onSlideChange: function(prevIndex, nextIndex) {
            this.find('ul.thumbs').children()
                .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
                .eq(nextIndex).fadeTo('fast', 1.0);
        },
        onPageTransitionOut: function(callback) {
            this.fadeTo('fast', 0.0, callback);
        },
        onPageTransitionIn: function() {
            $('.nav-controls').find('span.photo-index').html(+(this.displayedPage + 1) + ' of ' + this.getNumPages());
            var prevPageLink = this.parent('.navigation-container').parent('.photo-gallery').find('a.prev').css('visibility', 'hidden');
            var nextPageLink = this.parent('.navigation-container').parent('.photo-gallery').find('a.next').css('visibility', 'hidden');

            // Show appropriate next / prev page links
            if (this.displayedPage > 0) prevPageLink.css('visibility', 'visible');

            var lastPage = this.getNumPages() - 1;
            if (this.displayedPage < lastPage) nextPageLink.css('visibility', 'visible');
            this.fadeTo('fast', 1.0);
        }
    });

    /**************** Event handlers for custom next / prev page links **********************/
    gallery.parent('.navigation-container').parent('.photo-gallery').find('.content-slideshow a.prev').click(function(e) {
        gallery.previousPage();
        e.preventDefault();
    });
    gallery.parent('.navigation-container').parent('.photo-gallery').find('.content-slideshow a.next').click(function(e) {
        gallery.nextPage();
        e.preventDefault();
    });

    /****************************************************************************************/
    /**** Functions to support integration of galleriffic with the jquery.history plugin ****/
    // PageLoad function
    // This function is called when:
    // 1. after calling $.historyInit();
    // 2. after calling $.historyLoad();
    // 3. after pushing "Go Back" button of a browser
    function pageload(hash) {
        // hash doesn't contain the first # character.
        if (hash) {
            $.galleriffic.gotoImage(hash);
        } else {
            gallery.gotoIndex(0, false, true);
        }


    }
    // Initialize history plugin.
    // The callback is called at once by present location.hash.
    $.historyInit(pageload);
    $("a[rel='history']").live('click', function(e) {
        if (e.button != 0) return true;
        return false;
    });
});