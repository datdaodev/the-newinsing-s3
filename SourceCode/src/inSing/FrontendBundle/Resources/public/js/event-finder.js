$selector = $('#events-list-container');
$counter = 0;
$maxAjaxLoad = 2;
$today_date = new Date();

$(document).ready(function() {
    $(window).scroll(function(){
        if(!$(window).data('infinite-scroll-async')) {
            if ($(window).scrollTop() + $(window).height() >= $selector.offset().top + $selector.height() - 100) {
                //only load more $maxAjaxLoad times
                if ($counter < $maxAjaxLoad) {
                    // Locks the function
                    $(window).data('infinite-scroll-async', true);

                    $counter++;
                    //add more 2 months when scroll to end of page
                    $today_date.setMonth($today_date.getMonth() + 2);

                    //load more events list content
                    $.ajax({
                        url: $('#event_finder_ajax_load_url').val(),
                        type: 'GET',
                        //async: false,
                        data: {
                            'call-time': $counter
                        },
                        beforeSend: function () {
                            $('.ajax-loader').show();
                        },
                        complete: function () {
                            $(window).data('infinite-scroll-async', false);
                            $('.ajax-loader').hide();
                        },
                        success: function (response) {
                            if(response != '') {
                                $('#events-list-container').append(response);
                            }
                        }
                    });
                }
            }
        }
    });
});