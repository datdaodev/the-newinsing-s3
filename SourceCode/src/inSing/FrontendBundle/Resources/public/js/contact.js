$(document).ready(function (){
    var subject = $('#contactus_subject');
    var feedback = $('#contactus_typeOfFeedback');
    $('.form-subject').hide();
    var options_type_subject = $('#contactus_subject option');

    options_type_subject.each(function (index, value) {
        var option = options_type_subject[index];
        $('#select_backup').append(
            $('<option>').text(option.text).val(option.value)
        );
    });

    //remove select
    //$('#contactus_subject').find('option:not(:first)').remove();

    feedback.change(function(){
        if( $(this).val() == '' ){
            $('.form-subject').hide();
        } else {

            $('#contactus_subject').find('option:not(:first)').remove();
            var options = [];
            var options_type_subject = $('#select_backup option');
            var feedback_value = $(this).val();
            options_type_subject.each(function (index, value) {
                var option = options_type_subject[index];
                var optionstring = options_type_subject[index];
                var value_type_subject = $.trim($(this).val());

                var idArray = new Array();
                idArray = value_type_subject.split("_");
                var feedback_id = idArray[0];
                if(feedback_id == feedback_value) {
                    //console.log(feedback_value);
                    $('#contactus_subject').append(
                        $('<option>').text(option.text).val(option.value)
                    );
                }
            });

            $('.form-subject').show();
        }
    });

});