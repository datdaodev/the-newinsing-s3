jQuery(document).ready(function($) {
    $('div.content-slideshow').css('display', 'block');
    var onMouseOutOpacity = 0.5;
    $('#thumbs ul.thumbs li').opacityrollover({
        mouseOutOpacity: onMouseOutOpacity,
        mouseOverOpacity: 1.0,
        fadeSpeed: 'fast',
        exemptionSelector: '.selected'
    });
    var gallery = $('#thumbs').galleriffic({
        delay: 5000,
        numThumbs: 4,
        preloadAhead: 10,
        enableTopPager: false,
        enableBottomPager: false,
        imageContainerSel: '#slideshow',
        controlsContainerSel: '#controls',
        captionContainerSel: '#caption',
        loadingContainerSel: '#loading',
        renderSSControls: true,
        renderNavControls: true,
        playLinkText: 'Play Slideshow',
        pauseLinkText: 'Pause Slideshow',
        prevLinkText: '&lsaquo; Previous Photo',
        nextLinkText: 'Next Photo &rsaquo;',
        nextPageLinkText: 'Next &rsaquo;',
        prevPageLinkText: '&lsaquo; Prev',
        enableHistory: true,
        autoStart: false,
        syncTransitions: true,
        defaultTransitionDuration: 900,
        onSlideChange: function(prevIndex, nextIndex) {
            $(".portrait").find(".photo-gallery .caption").css("height", "99px");
            $('.image-caption.current,.image-wrapper.current').remove();
            this.find('ul.thumbs').children().eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end().eq(nextIndex).fadeTo('fast', 1.0);
            if ($('.nav-controls').find('.photo-index').length == 0) {
                $('<span class="photo-index"></span>').insertAfter($('.nav-controls .prev'))
            }
            $('.nav-controls').find('span.photo-index').html(+(nextIndex + 1) + ' of ' + this.data.length);
            var prevPhotoLink = this.parent('.navigation-container').parent('.photo-gallery').find('#controls a.prev').css('visibility', 'visible');
            var nextPhotoLink = this.parent('.navigation-container').parent('.photo-gallery').find('#controls a.next').css('visibility', 'visible');
            var lastPhoto = this.data.length;
            if (nextIndex == 0) {
                prevPhotoLink.css('visibility', 'hidden');

                if (!location.hash) {
                    var firstCourtesy = $('.navigation-slide ul.thumbs li').eq(0).find("input.imageCredit").val();
                    $('p.courtesy').text(firstCourtesy);
                }
            }
            if (nextIndex == lastPhoto - 1) {
                nextPhotoLink.css('visibility', 'hidden');
            }
            setTimeout(moreCaption, 100);
        },
        onPageTransitionOut: function(callback) {
            this.fadeTo('fast', 0, callback);
        },
        onPageTransitionIn: function() {
            var prevPageLink = this.find('a.prev').css('visibility', 'hidden');
            var nextPageLink = this.find('a.next').css('visibility', 'hidden');
            if (this.displayedPage > 0)
                prevPageLink.css('visibility', 'visible');

            var lastPage = this.getNumPages() - 1;
            if (this.displayedPage < lastPage)
                nextPageLink.css('visibility', 'visible');
            this.fadeTo('fast', 1.0);
        }

    })
    gallery.find('a.prev').click(function(e) {
        gallery.previousPage();
        e.preventDefault();
    });
    gallery.find('a.next').click(function(e) {
        gallery.nextPage();
        e.preventDefault();
    });

    function pageload(hash) {
        $('p.courtesy').text($("a[href='#" + hash + "']").parent().find("input.imageCredit").val());
        if (hash) {
            $.galleriffic.gotoImage(hash);
        } else {
            gallery.gotoIndex(0, false, true);
            return false;
        }
    }
    $.historyInit(pageload);
    $("a[rel='history']").live('click', function(e) {
        if (e.button != 0) return true;
        var hash = this.href;
        hash = hash.replace(/^.*#/, '');
        $.historyLoad(hash);
        return false;
    });
    $('.more-caption').click(function() {
        var $this = $(this);
        $this.parent().parent().find('.caption').css('height', 'auto');
        $this.parent().parent().parent().parent('.slideshow-wrap').css("cssText", "height: auto !important;");
        var text = $.trim($this.html());
        if (text.toLowerCase() == "more") {
            $this.text("Less").addClass("less");
            $('.caption-container').addClass('expanded');
        } else {
            $this.text("More").removeClass("less");
            $('.caption-container').removeClass('expanded');
        }
    });

    function moreCaption() {
        var captionH = $('.caption-container .caption').height();
        if (captionH > 40) {
            $('.more-caption-wrap').addClass('visible');
        } else {
            $('.more-caption-wrap').removeClass('visible');

        }
    }
    /****************************************************************************************/
});