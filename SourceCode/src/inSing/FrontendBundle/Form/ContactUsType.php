<?php

    namespace inSing\FrontendBundle\Form;

    use Symfony\Component\Validator\Constraints\NotBlank;

    use Symfony\Component\Validator\Constraints\EmailValidator;

    use Symfony\Component\Validator\Constraints\Email;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;

    class ContactUsType extends AbstractType
    {

        protected $container;

        public function __construct($container)
        {
            $this->container = $container;
        }

        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $builder
                ->add('name', 'text', array(
                    'required' => false,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Name must be required')),
                    )
                ));

            $builder->add('email', 'email', array(
                'required' => false,
                'constraints' => array(
                    new NotBlank(array('message' => 'Email must be required')),
                    new Email(array('message' => 'Email is invalid')),
                )
            ));

            $builder->add('mobile', 'text', array(
                'required' => false,
                'constraints' => array(
                    new NotBlank(array('message' => 'Mobile must be required')),
                )
            ));
            /*
              ->add('typeOfFeedback', 'entity', array(
              'class' => 'InsingHomepageBundle:FeedbackType',
              'query_builder' => function(EntityRepository $er) {
              return $er->createQueryBuilder('ft')->orderBy('ft.title', 'ASc');
              },
              'empty_value' => 'Please select',
              ))
             */
            $feedbackType = array('' => 'Please select');
            $feedbackTypes = $this->container->getParameter('insing_frontend_feed_back_type');
            foreach ($feedbackTypes as $key => $item) {
                $feedbackType[$key] = $item;
            }
            $builder->add('typeOfFeedback',
                'choice', array(
                    'label' => 'Type of Feedback',
                    'required' => false,
                    'choices' => $feedbackType,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Type of feedback must be required'))
                    )
                )
            );

            $feedbackSubject = array('' => 'Please select');
            $feedbackSubjects = $this->container->getParameter('insing_frontend_feed_back_subject');
            foreach ($feedbackSubjects as $fbkey => $fbitem){
                foreach ($fbitem as $key => $subject) {
                    $feedbackSubject[$fbkey.'_'.$key] =  $subject['title'];
                }
            }

            $builder->add('subject', 'choice', array(
                'required' => false,
                'choices' => $feedbackSubject,
                'constraints' => array(
                    new NotBlank(array('message' => 'Subject must be required'))
                )
            ));

            $builder->add('comments', 'textarea', array(
                'required' => false,
                'constraints' => array(
                    new NotBlank(array('message' => 'Comments must be required'))
                )
            ));

            $builder->add('error_captcha', 'text', array(
                'required' => false,
                'mapped' => false
            ));

        }

        public function getDefaultOptions(array $options)
        {
            return array(
                'data_class' => 'inSing\FrontendBundle\Entity\ContactUs',
                'csrf_protection' => true,
                'csrf_field_name' => '_token',
                // a unique key to help generate the secret token
                'intention' => 'contactus_item',
            );
        }

        public function getName()
        {
            return 'contactus';
        }

    }
