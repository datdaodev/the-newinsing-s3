<?php
namespace inSing\FrontendBundle\Twig;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use inSing\DataSourceBundle\Lib\DataTracker;
use inSing\DataSourceBundle\Lib\Constant;
use inSing\DataSourceBundle\Lib\GoogleAnalytics;
use inSing\DataSourceBundle\Lib\UtilHelper;
use inSing\FrontendBundle\Lib\ReCaptcha;
use inSing\FrontendBundle\Utils\Utils;

/**
 * Custom twig extension
 *
 * @author Trung Nguyen
 */
class FrontendPageExtension extends \Twig_Extension
{
    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var container
     */
    protected $_container;

    /**
     * @var ReCaptcha
     */
    protected $reCaptcha;

    /**
     * @var generator
     */
    protected $_generator;

    protected $cdnResizeSecret;

    public function __construct(ContainerInterface $container, UrlGeneratorInterface $generator)
    {
        $this->_container = $container;
        $this->_generator = $generator;
    }

    /**
     * Inject Request
     *
     * @param Request $request
     * @author Trung Nguyen
     */
    public function setRequest(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * Inject DataTracker
     *
     * @param DataTracker $data_tracker
     * @author Trung Nguyen
     */
    public function setDataTracker(DataTracker $data_tracker)
    {
    	$this->dataTracker = $data_tracker;
    }

    /**
     * Inject SecurityContext
     *
     * @param Request $request
     * @author Trung Nguyen
     */
    public function setSecurityContext(SecurityContext $security_context)
    {
        $this->securityContext = $security_context;
    }

    /**
     * Set configurations of HomePageBundle
     *
     * @param array $config
     * @author Trung Nguyen
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * Inject ReCaptcha
     *
     * @param DataTracker $data_tracker
     * @author Vu Luu
     */
    public function setReCaptcha(ReCaptcha $recaptcha)
    {
        $this->reCaptcha = $recaptcha;
    }

    public function getCdnResizeSecret() {
        return $this->cdnResizeSecret;
    }

    public function setCdnResizeSecret($cdnResizeSecret) {
        $this->cdnResizeSecret = $cdnResizeSecret;
    }

    /**
     * (non-PHPdoc)
     * @see Twig_Extension::getFilters()
     */
    public function getFunctions()
    {
        return array(
       		'data_tracker_form'			=> new \Twig_Function_Method($this, 'dataTrackerForm', array('is_safe' => array('html'))),
            'search_data_tracker_form'	=> new \Twig_Function_Method($this, 'searchDataTrackerForm', array('is_safe' => array('html'))),
            'is_desktop'				=> new \Twig_Function_Method($this, 'isDesktop'),
            'is_mobile'					=> new \Twig_Function_Method($this, 'isMobile'),
            'is_mobile_ios'				=> new \Twig_Function_Method($this, 'isMobileIOs'),
            'is_tablet'					=> new \Twig_Function_Method($this, 'isTablet'),
            'is_safari_desktop'			=> new \Twig_Function_Method($this, 'isSafariDesktop'),
            'config_info'				=> new \Twig_Function_Method($this, 'getConfigInfo'),
            'ga_tracking'				=> new \Twig_Function_Method($this, 'googleAnalyticsTracking', array('is_safe' => array('html'))),
            'caption_max_length'        => new \Twig_Function_Method($this, 'getCaptionMaxLength'),
            'normal_string'             => new \Twig_Function_Method($this, 'deleteSpecialCharacterInString'),
            'recaptcha'                 => new \Twig_Function_Method($this, 'genRecaptcha', array('is_safe' => array('html'))),
            'get_css_by'		        => new \Twig_Function_Method($this, 'getCssByFunction'),
            'get_text_by'		        => new \Twig_Function_Method($this, 'getTextByFunction'),
            'change_date_update_for_artical'        => new \Twig_Function_Method($this, 'genDateAndUpdateForArtical'),
            'getParameter'              => new \Twig_Function_Method($this, 'getParameter'),
            'word_limiter'				=> new \Twig_Function_Method($this, 'wordLimiter'),
            'change_date_update_for_article'        => new \Twig_Function_Method($this, 'genDateAndUpdateForArticle'),
            'truncate_word'        => new \Twig_Function_Method($this, 'truncateWordFunction'),
        );
    }

    public function getParameter($param)
    {
        return $this->_container->getParameter($param);
    }

    /**
     * (non-PHPdoc)
     * @see Twig_Extension::getFilters()
     */
    public function getFilters()
    {
        return array(
            'removeUrlParams' => new \Twig_Filter_Method($this, 'removeUrlParams'),
            'idToReversedHex' => new \Twig_Filter_Method($this, 'convertIdToReversedHex'),
            'slug' => new \Twig_Filter_Method($this, 'convertToSlug'),
            'websiteFilter' => new \Twig_Filter_Method($this, 'websiteFilter'),
            'generateFullResourceUrl' => new \Twig_Filter_Method($this, 'generateFullResourceUrl'),
            'appendEndDate' => new \Twig_Filter_Method($this, 'appendEndDate'),
        );
    }

    /**
     * (non-PHPdoc)
     * @see Twig_Extension::getGlobals()
     */
    public function getGlobals()
    {
        return array(
            'insing_ga_tracking_id'     => $this->config['ga_tracking_id'],
            'facebook_app_id'           => $this->config['facebook_app_id'],
        );
    }

    /**
     * @author Dat.Dao
     * @param $str
     * @param $length
     * @return mixed
     */
    public function truncateWordFunction($str, $length) {
        return $this->_container->get('insing.hellper')->truncateWordsWith($str, $length);
    }

    public function getCssByFunction($type, $channelName) {
        $channelName = $this->_container->get('insing.hellper')->getChannelName($channelName);
        $css = '';
        //1 : movies, 2 : events, 3 : restaurant, 4 : restaurant deals(deplay), 5 : hungry deals, 6 : article, 7 : gallery
        switch($type) {
            case 1:
                $css = 'top-movie';
                break;
            case 2:
                $css = 'event-highlight';
                break;
            case 3:
                $css = 'hgw-deal';//restaurant->
                break;
//            case 4:
//                $css = 'restaurant-deals';//?
//                break;
            case 5:
                $css = 'hgw-deal';
                break;
            case 6:
            case 7:
                switch($channelName) {
                    case 'event':
                        $css = 'event-highlight';
                        break;
                    case 'movie':
                        $css = 'top-movie';
                        break;
                    case 'hungrygowhere':
                        $css = 'hgw-deal';
                        break;
                }
                break;
        }
        if(empty($css)) {
            //default
            $css = 'hgw-deal';
        }
        return $css;
    }

    public function getTextByFunction($type) {
        $text = array();
        //1 : movies, 2 : events, 3 : restaurant, 4 : restaurant deals, 5 : hungry deals, 6 : article, 7 : gallery
        /*
         i. Movie : Read Reviews | Show times available
        ii. Restaurant : View Restaurant | Book a Table
        iii. Deals : View Deal | Book a Table
        iv. Events : View Event | Buy Tickets / Free
         * */
        switch($type) {
            case 1:
                //movie
                $text = array('Read Reviews', 'Showtimes available');
                break;
            case 2:
                //event
                $text = array('View Event','Buy Tickets / Free');
                break;
            case 3:
                //Restaurant
                $text = array('View Restaurant','Book a Table');
                break;
//            case 4:
//                $text = array('Read Reviews?', 'Show times available?');//??
//                break;
            case 5:
                //hungry deals
                $text = array('View Deal','Book a Table');
                break;
            case 6:
            case 7:
                //article or gallery
                $text = array('View the Story');
                break;
        }
        return $text;
    }

    /**
     * Generate reCAPTCHA
     *
     * @param string $template_name
     * @return string
     * @author Vu Luu
     */
    public function genRecaptcha($template_name = '')
    {
        if ($template_name != '') {
            $ret[] = '<script type="text/javascript">';
            $ret[] = 'var RecaptchaOptions = {';
            $ret[] = "theme : '{$template_name}',";
            $ret[] = "lang : 'en',";
            $ret[] =  " custom_translations : { instructions_visual : \"Input two words\" } ";
            $ret[] = '};';
            $ret[] = '</script>';
        }

        $ret[] = $this->reCaptcha->recaptcha_get_html($this->_container->getParameter('insing_frontend_recaptcha_public_key'));

        return implode("\n", $ret);
    }

    /**
     * (non-PHPdoc)
     * @see Twig_ExtensionInterface::getName()
     */
    public function getName()
    {
        return 'frontend_page_extension';
    }

    /**
     * Generate html form based on DataTracker object
     *
     * @return string
     * @author Trung Nguyen
     */
    public function dataTrackerForm($path, $block_count)
    {
    	$htmlForm[] = '<form name="frm_data_tracker_[BLOCK_NO]" class="frm_data_tracker">';
    	$htmlForm[] = '<input type="hidden" name="url" value="[PATH]" />';
    	$htmlForm[] = '<input type="hidden" name="block" value="[BLOCK_NO]" />';
    	$htmlForm[] = '<input type="hidden" name="block_sidebar" value="[BLOCK_SIDEBAR_NO]" />';
    	$htmlForm[] = '<input type="hidden" name="block_count" value="[BLOCK_COUNT]" />';
    	$htmlForm[] = ( self::isMobileIOs() ) ? '<input type="hidden" name="ios" value="ios" />' : '';
    	$htmlForm[] = ( self::isDesktop() ) ? '<input type="hidden" name="desktop" value="desktop" />' : '';
    	$htmlForm[] = ( self::isTablet() ) ? '<input type="hidden" name="tablet" value="tablet" />' : '';
    	$htmlForm[] = ( self::isMobile() ) ? '<input type="hidden" name="mobile" value="mobile" />' : '';
    	$htmlForm[] = '<input type="hidden" name="data" value="[DATA]" />';
    	$htmlForm[] = '</form>';

    	$trans = array(
    			'[BLOCK_NO]' => $this->dataTracker->getCurrentMainBlock(),
    			'[BLOCK_SIDEBAR_NO]' => $this->dataTracker->getCurrentSidebarBlock(),
    			'[BLOCK_COUNT]' => $block_count,
    			'[DATA]' => implode(',', $this->dataTracker->getTrackData()),
    			'[PATH]' => $path
    	);

    	return strtr(implode("\n", $htmlForm), $trans);
    }

    /**
     * @author Cuong.Bui
     */
    public function searchDataTrackerForm($path, $q, $page, $block_count)
    {
        $htmlForm[] = '<form name="frm_data_tracker_[BLOCK_NO]" class="frm_data_tracker">';
    	$htmlForm[] = '<input type="hidden" name="url" value="[PATH]" />';
        $htmlForm[] = '<input type="hidden" name="q" value="[KEYWORDS]" />';
        $htmlForm[] = '<input type="hidden" name="page" value="[PAGE]" />';
    	$htmlForm[] = '<input type="hidden" name="block" value="[BLOCK_NO]" />';
    	$htmlForm[] = '<input type="hidden" name="block_sidebar" value="[BLOCK_SIDEBAR_NO]" />';
    	$htmlForm[] = '<input type="hidden" name="block_count" value="[BLOCK_COUNT]" />';
    	$htmlForm[] = ( self::isMobileIOs() ) ? '<input type="hidden" name="ios" value="ios" />' : '';
    	$htmlForm[] = ( self::isDesktop() ) ? '<input type="hidden" name="desktop" value="desktop" />' : '';
    	$htmlForm[] = ( self::isTablet() ) ? '<input type="hidden" name="tablet" value="tablet" />' : '';
    	$htmlForm[] = ( self::isMobile() ) ? '<input type="hidden" name="mobile" value="mobile" />' : '';
    	$htmlForm[] = '<input type="hidden" name="data" value="[DATA]" />';
    	$htmlForm[] = '</form>';

    	$trans = array(
            '[BLOCK_NO]' => $this->dataTracker->getCurrentMainBlock(),
            '[BLOCK_SIDEBAR_NO]' => $this->dataTracker->getCurrentSidebarBlock(),
            '[BLOCK_COUNT]' => $block_count,
            '[DATA]' => implode(',', $this->dataTracker->getTrackData()),
            '[PATH]' => $path,
            '[KEYWORDS]' => $q,
            '[PAGE]' => $page
    	);

    	return strtr(implode("\n", $htmlForm), $trans);
    }

    /**
     * Check if current device is desktop
     *
     * @return bool
     * @author Trung Nguyen
     */
    public function isDesktop()
    {
        return $this->request->attributes->get('device_type') == Constant::DEVICE_TYPE_DESKTOP;
    }

    /**
     * Check if current device is mobile
     *
     * @return bool
     * @author Trung Nguyen
     */
    public function isMobile()
    {
        return $this->request->attributes->get('device_type') == Constant::DEVICE_TYPE_MOBILE;
    }

    /**
     * Check if current device is mobile iOS
     *
     * @return bool
     * @author Vu Tran
     */
    public function isMobileIOs()
    {
        return $this->request->attributes->get('device_ios') == Constant::DEVICE_TYPE_MOBILE_IOS;
    }



    /**
     * Check if current device is tablet
     *
     * @return bool
     * @author Trung Nguyen
     */
    public function isTablet()
    {
        return $this->request->attributes->get('device_type') == Constant::DEVICE_TYPE_TABLET;
    }

    /**
     * Check if current browser is Safari
     *
     * @return bool
     * @author Vu Tran
     */
    public function isSafariDesktop()
    {
    	return $this->request->attributes->get('browser_safari') == Constant::BROWSER_SAFARI_DESKTOP;
    }

    /**
     * Get Config info
     *
     * @param string $config_name
     * @return string
     * @author Vu Tran
     */
    public function getConfigInfo($config_name = '')
    {
        return $this->config[$config_name];
    }


    /**
     * GA tracking
     *
     * @author Vu Tran
     */
   public function googleAnalyticsTracking($type, $value = '')
   {
       $objGa = new GoogleAnalytics();
       $arrResult = array();

       switch (strtolower($type)) {
           case strtolower(Constant::GA_HEADER):
               $arrResult = $objGa->getGAHeader();
               break;

           case strtolower(Constant::GA_HOMEPAGE):
               $arrResult = $objGa->getGAHomePage($value);
               break;

           case strtolower(Constant::GA_FOOTER):
               $arrResult = $objGa->getGAFooter();
               break;
       }

       return $arrResult;
   }

    public function appendEndDate($start, $end, $format = 'M j, Y', $separator = ' - ') {
        if (isset($start)) {
            if (is_string($start)) {
                $start = date($format, strtotime($start));
            } else {
                $start = $start->format($format);
            }
        }

        if (isset($end)) {
            if (is_string($end)) {
                $end = date($format, strtotime($end));
            } else {
                $end = $end->format($format);
            }
        }

        $result = $start;
        if (isset($start, $end) && $start !== $end) {
            $result = $result . $separator . $end;
        }

        return $result;
    }

    /**
     * @author Cuong.Bui
     */
    public function genPhotoCdnUrl($cdn_photo_url, $width, $height)
    {
        $secret = isset($this->config['cdn_secret']) ? $this->config['cdn_secret'] : '';
        $photo  = UtilHelper::generatePhotoFromUrl($cdn_photo_url, $width, $height, $secret);
        return $photo['photo_url'];
    }

    /**
     * Check current device environment and get suitable caption length
     *
     * @return integer
     * @author Vu Luu
     */
    public function getCaptionMaxLength(){
        if($this->request->attributes->get('device_type') == Constant::DEVICE_TYPE_DESKTOP){ //current device is PC
            return $this->_container->getParameter('insing_features_gallery_caption_maxlength_desktop');
        } else if($this->request->attributes->get('device_type') == Constant::DEVICE_TYPE_MOBILE){ //current device is mobile
            return $this->_container->getParameter('insing_features_gallery_caption_maxlength_mobile');
        } else if($this->request->attributes->get('device_type') == Constant::DEVICE_TYPE_TABLET){ //current device is tablet
            return $this->_container->getParameter('insing_features_gallery_caption_maxlength_tablet');
        } else { //default getting device is PC
            return $this->_container->getParameter('insing_features_gallery_caption_maxlength_desktop');
        }
    }
    /**
     * @author Vu Luu
     * Delete special characters differ a-z, A-z, 0-9
     *
     */
    public function deleteSpecialCharacterInString($string)
    {
//        $string = str_replace("'", '', $string);
        $string = preg_replace('/[^a-zA-Z0-9\s]+/i', '', $string);
        $string = trim(preg_replace('/\s+/', ' ', $string));

        return $string;
    }

    public function removeUrlParams($url) {
        return Utils::removeUrlParams($url);
    }

    public function convertToSlug($string) {
        return Utils::cleanUrl($string);
    }

    public function convertIdToReversedHex($id) {
        return Utils::toReverseHex($id);
    }

    public function websiteFilter($url) {

        if (!empty($url)) {
            $info = parse_url($url);
            $host = $info['host'];
            $host_names = explode(".", $host);
            for ($cnt = count($host_names); $cnt >= 1; $cnt--) {
                if (isset($bottom_host_name)) {
                    $bottom_host_name = $bottom_host_name . "." . $host_names[count($host_names) - $cnt];
                } else {
                    $bottom_host_name = $host_names[count($host_names) - $cnt];
                }

            }
            return $bottom_host_name;
        } else {
            return "";
        }

    }
    /**
     * @author Cuong.Au
     */
    public function genDateAndUpdateForArticle($publish_date,$update_date="")
    {
        if ( !is_null($publish_date) ) {
            //Convert Published Date to String if needed
            $publish_date   = is_string($publish_date) ? $publish_date : $publish_date->format('Y-m-d H:i:s');
            $update_date    = is_string($update_date) ? $update_date : $update_date->format('Y-m-d H:i:s');
            $pd= strtotime($publish_date);
            $d1 = date('d F Y g:i A',$pd);

            if($update_date) {
                $ud = strtotime($update_date);
                $str = $ud- $pd;
                if( $ud  > $pd )
                {
                    $date_diff = floor($str/3600/24);
                    $d2 = " | Updated ";
                    if($date_diff) {
                        $d2 .= date('d F Y ',$ud);

                    } else {
                        $d2 .= date('g:i A',$ud);
                    }
                }
                else
                {
                    return date('d F Y g:i A',$pd);;
                }
            }

            return $d1 . $d2;
        }

        return '';
    }

    public function generateFullResourceUrl($resourceUrl, $imageSize) {
        $urlPrefix = (strtolower($this->_container->getParameter('environment')) == 'live') ? '' : strtolower($this->_container->getParameter('environment')) . '.';
        if ($resourceUrl && strpos($resourceUrl, 'http') === false) {
            return Utils::generateImageUrl($resourceUrl, $urlPrefix, $imageSize);
        }
        //return Utils::generateImageUrl($resourceUrl, $urlPrefix, $imageSize);
        return $this->changeImageSizeCms($resourceUrl, $imageSize);
    }

    public function changeImageSizeCms($imageUrl, $dimension) {
        $this->cdnResizeSecret  = $this->_container->getParameter("cdn_secret");
        $finalImageUrl = '';
        if (isset($imageUrl)) {
            // check if image is a fallback image and just return as it is
            if (strpos($imageUrl, '/fallback/') != 0) {
                $finalImageUrl = $imageUrl;
            } else {
                // split to get the file part

                $urlParts = explode('/', $imageUrl);
                if ($urlParts !== false && count($urlParts) > 1) {
                    $imageFile = $urlParts[count($urlParts) - 1];
                    // split to get the file extension
                    $imageFileParts = explode('.', $imageFile);
                    if ($imageFileParts !== false && count($imageFileParts) > 1) {
                        $imageExt = $imageFileParts[count($imageFileParts) - 1];
                        unset($imageFileParts[count($imageFileParts) - 1]);
                        $imageFile = implode('.', $imageFileParts);
                        // split to get the cms base file name
                        $cmsFileInfoParts = explode('_', $imageFile);
                        if ($cmsFileInfoParts !== false) {
                            $tokenCount = count($cmsFileInfoParts);
                            if ($tokenCount >= 3) {
                                if (preg_match('/\d+x\d+/', $cmsFileInfoParts[$tokenCount - 2])) {
                                    // remove both signature and old dimension
                                    unset($cmsFileInfoParts[$tokenCount - 1]);
                                    unset($cmsFileInfoParts[$tokenCount - 2]);
                                }
                            } else if ($tokenCount >= 2) {
                                if ($cmsFileInfoParts[$tokenCount - 1] == '_o') {
                                    unset($cmsFileInfoParts[$tokenCount - 1]);
                                }
                            }
                            $baseFileName = implode('_', $cmsFileInfoParts);
                            // build hash
                            $hash = substr(md5($baseFileName . '_' . $dimension . '.' . $imageExt . $this->cdnResizeSecret), 0, 10);
                            $urlParts[count($urlParts) - 1] = $baseFileName . '_' . $dimension . '_' . $hash . '.' . $imageExt;
                            $finalImageUrl = implode('/', $urlParts);
                        }
                    }
                }
            }
        }
        return $finalImageUrl;
    }
    /**
     * @author Cuong.Au
     * Word Limiter
     *
     * Limits a string to X number of words.
     *
     * @access	public
     * @param	string
     * @param	integer
     * @param	string	the end character. Usually an ellipsis
     * @return	string
     */
    public function wordLimiter($str, $limit = 100, $end_char = '...')
    {

        if (trim($str) == '') {
            return $str;
        }
        preg_match('/^\s*+(?:\S++\s*+){1,' . (int)$limit . '}/', $str, $matches);

        if (strlen($str) == strlen($matches[0])) {
            $end_char = '';
        }
        return rtrim($matches[0]) . $end_char;
    }

    /**
     * @author Vu Luu
     */
    public function genDateAndUpdateForArtical($publish_date,$update_date="")
    {
        if ( !is_null($publish_date) ) {
            //Convert Published Date to String if needed
            $publish_date   = is_string($publish_date) ? $publish_date : $publish_date->format('Y-m-d H:i:s');
            $update_date    = is_string($update_date) ? $update_date : $update_date->format('Y-m-d H:i:s');

            $pd= strtotime($publish_date);
            $d1 = date('d F Y g:i A',$pd);

            if($update_date) {
                $ud = strtotime($update_date);
                $str = $ud- $pd;
                if( $ud  > $pd )
                {
                    $date_diff = floor($str/3600/24);
                    $d2 = " | Updated ";
                    if($date_diff) {
                        $d2 .= date('d F Y ',$ud);

                    } else {
                        $d2 .= date('g:i A',$ud);
                    }
                }
                else
                {
                    return date('d F Y g:i A',$pd);;
                }
            }

            return $d1 . $d2;
        }

        return '';
    }
}
