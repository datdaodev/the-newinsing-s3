<?php

namespace inSing\FrontendBundle\Twig;

/**
 * @author khai.vu <khai.vu@s3corp.com.vn>
 */
class EventExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'event_extension';
    }

    public function getTests()
    {
        return array(
            new \Twig_SimpleTest('free', array($this, 'freeTest')),
            new \Twig_SimpleTest('buy', array($this, 'buyTest'))
            /*new \Twig_SimpleTest('view', array($this, 'viewTest'))*/
        );
    }

    public function freeTest($event)
    {
        if (isset($event['price_min']) && isset($event['price_max'])
            && $event['price_min'] != "" && $event['price_max'] != ""
            && $event['price_min'] == 0 && $event['price_max'] == 0
        )
        {
            return true;
        }

        return false;
    }

    public function buyTest($event)
    {
        if (!$this->freeTest($event)
            && isset($event['vendor_url']) && $event['vendor_url'] != ""
            && ((isset($event['end_date']) && (date('Y-m-d') <= $event['end_date']))
                || (isset($event['on_going']) && $event['on_going'] == 1))
        )
        {
            return true;
        }

        return false;
    }

    /*public function viewTest($event)
    {
        if (!$this->freeTest($event)
            && isset($event['on_going']) && $event['on_going'] == 1
        )
        {
            return true;
        }

        return false;
    }*/
}