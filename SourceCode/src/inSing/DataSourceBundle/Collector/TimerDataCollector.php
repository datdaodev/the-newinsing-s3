<?php 

namespace InSing\DataSourceBundle\Collector;

use Symfony\Component\HttpKernel\DataCollector\DataCollector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TimerDataCollector extends DataCollector
{
  private static $_container;
  
  private static $_data = array();

  public function __construct(ContainerInterface $container) {
    self::$_container = $container;
  }
      
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $this->data = array(
            'timer' => $this->getData(),
        );
    }

    public function setData($arrTimerData) {
        if (self::$_container->getParameter('profiler_is_show_time_collector') == true ) {
            $action = key($arrTimerData);
            $point = key($arrTimerData[$action]);
            self::$_data[$action][$point] = $arrTimerData[$action][$point]; 
        }
    }
    
    public function getData() {
        return self::$_data;
    }
    
    public function getTimer()
    {
        $result = array();
        foreach ($this->data['timer'] as $action => $arrValue) {
            if (isset($arrValue['end']) && isset($arrValue['start'])) {
                $timer = round(($arrValue['end'] - $arrValue['start']) * 1000);
                $result[$action][] = $timer;  
            } else {
                $result[$action][] = 0;
            }
        }
        return $result;
    }

    public function getName()
    {
        return 'timer';
    }
}