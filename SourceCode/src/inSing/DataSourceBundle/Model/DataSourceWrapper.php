<?php 

namespace inSing\DataSourceBundle\Model;

use inSing\DataSourceBundle\Model\DataSource;
use inSing\DataSourceBundle\Lib\Pimple;
use inSing\DataSourceBundle\Lib\UtilHelper;
use inSing\DataSourceBundle\Lib\Exception\ResultIsEmptyException;
use inSing\DataSourceBundle\Lib\Exception\RuntimeErrorException;

/**
 * A wrapper class for DataSource
 * 
 * @author Trung Nguyen
 */
class DataSourceWrapper implements \SplSubject
{
    /**
     * @var DataSource
     */
    protected $ds;

    /**
     * @var Pimple
     */
    protected $container;
    
    /**
     * @var string
     */
    protected $pid;
    
    /**
     * @var array
     */
    private $observers = array();
    
    /**
     * @var int
     */
    protected $startTime;
    
    /**
     * @var int
     */
    protected $stopTime;
    
    /**
     * @var string
     */
    protected $currentMethod;
    
    /**
     * @var Exception
     */
    protected $exception;
    
    /**
     * @var array
     */
    protected $lastError = null;
    
    /**
     * Constructor
     * 
     * @param string $pid
     * @param DataSource $ds
     */
    public function __construct(Pimple $container = null)
    {
        if (!is_null($container)) {
            $this->container = $container;
        }
    }
    
    /**
     * Magic call 
     * 
     * @param string $method
     * @param array $args
     * @throws \Exception
     * @return Ambigous <boolean, mixed>
     * @author Trung Nguyen
     */
    public function __call($method, $args)
    {
        if (!is_callable(array($this->ds, $method))) {
            throw new \Exception('Call to undefined method');
        }
    
        try {            
            $this->currentMethod = $method;
            
            // get start time
            $this->startTime = UtilHelper::getMicroTimeInFloat();
            
            $result = call_user_func_array(array($this->ds, $method), $args);
            
            // get stop time
            $this->stopTime = UtilHelper::getMicroTimeInFloat();
            
            // notify SpeedMonitor if this method is registered for monitoring
            if (in_array($method, $this->ds->getMethods())) {            
                $this->notify('speed_monitor');
            }
            
        } catch (ResultIsEmptyException $e) {
            // notify SpeedMonitor if this method is registered for monitoring
            if (in_array($method, $this->ds->getMethods())) {            
                $this->notify('speed_monitor');
            }
            
            $result = null;
            
        } catch (\Exception $e) {
            $this->lastError = array(
                'class'  => get_class($this->ds),
                'method' => $method,
                'error_code' => $e->getCode(),
                'error_str'  => $e->getMessage()
            );
            
            $this->notify('exception_monitor');
            
            $result = false;
        }
    
        return $result;
    }
    
    /**
     * Get magic method
     * 
     * @param string $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    /**
     * Set process id
     * 
     * @param string $pid
     */
    public function setPid($pid) 
    {
       $this->pid = $pid; 
    }
    
    /**
     * Set data source
     *
     * @param DataSource $pid
     */
    public function setDatasource(DataSource $ds)
    {
        $this->ds = $ds;
    }
    
    /**
     * Get last error
     *
     * @return array
     */
    public function getLastError()
    {
        return $this->lastError;
    }
    
    /**
     * A shortcut for getting service by ID
     * 
     * @param unknown $id
     */
    public function get($id)
    {
       return $this->container->get($id);     
    }
    
    /**
     * Attaches an SplObserver to DataSource
     *
     * @author Trung Nguyen
     * @param  SplObserver $obs
     * @return void
     */
    public function attach(\SplObserver $obs)
    {
        $id = spl_object_hash($obs);
        $this->observers[$id] = $obs;
    }
    
    /**
     * Detaches the SplObserver from the DataSource
     *
     * @author Trung Nguyen
     * @param  SplObserver $obs
     * @return void
     */
    public function detach(\SplObserver $obs)
    {
        $id = spl_object_hash($obs);
        unset($this->observers[$id]);
    }
    
    /**
     * Notify all observers
     *
     * @author Trung Nguyen
     * @param  string $type
     * @return void
     */
    public function notify($type = '')
    {
        foreach($this->observers as $obs) {
            if ($obs instanceof \inSing\DataSourceBundle\Lib\Monitor\SpeedMonitor && $type == 'speed_monitor') {
                $obs->update($this);
            } elseif ($obs instanceof \inSing\DataSourceBundle\Lib\Monitor\ExceptionMonitor && $type == 'exception_monitor') {
                $obs->update($this);
            }
        }
    }
    
}