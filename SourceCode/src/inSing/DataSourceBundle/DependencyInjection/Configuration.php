<?php

namespace inSing\DataSourceBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('in_sing_data_source');

        $rootNode
            ->children()
                ->scalarNode('cache_type')
                    ->defaultValue('memcache')
                    ->info('Define the cache type will be used for Data Source')
                    ->example('cache_type: memcache')
                    ->validate()
                    ->ifNotInArray(array('apc', 'memcache', 'filecache'))
                        ->thenInvalid('The "%s" cache type hasnt been supported yet')
                    ->end()
                ->end()

                ->scalarNode('cache_timeout')
                    ->defaultValue(600)
                    ->info('Define the duration cache entry is available')
                    ->example('cache_timeout: 600')
                ->end()

                // cms_api node
                ->arrayNode('cms_api')
                    ->children()
                        ->scalarNode('consumer_key')
                            ->cannotBeEmpty()
                            ->info('Consumer key is used to connect to CMS API')
                            ->example('consumer_key: a0ff03d55e57740311ad8b7811373a')
                        ->end()
                        ->scalarNode('consumer_secret')
                            ->cannotBeEmpty()
                            ->info('Consumer secret is used to connect to CMS API')
                            ->example('consumer_secret: 20029c58ce')
                        ->end()
                        ->scalarNode('access_token')
                            ->cannotBeEmpty()
                            ->info('Access token is used to connect to CMS API')
                            ->example('consumer_key: a0ff03d55e57740311ad8b7811373a')
                        ->end()
                        ->scalarNode('access_token_secret')
                            ->cannotBeEmpty()
                            ->info('Access token secret is used to connect to CMS API')
                            ->example('access_token_secret: 20029c58ce')
                        ->end()
                        ->scalarNode('auth_url')
                            ->cannotBeEmpty()
                            ->info('URL is used to get session token')
                            ->example('auth_url: http://cms-api.newhp.s3.test.insing.com/authenticate')
                        ->end()
                        ->scalarNode('service_find_articles_url')
                            ->cannotBeEmpty()
                            ->info('URL is used to get articles data')
                            ->example('service_url: http://cms-api.newhp.s3.test.insing.com/find-articles')
                        ->end()
                        ->scalarNode('service_article_details_url')
                            ->cannotBeEmpty()
                            ->info('URL is used to get article details data')
                            ->example('service_url: http://cms-api.newhp.s3.test.insing.com/article-details')
                        ->end()
                        ->scalarNode('service_find_galleries_url')
                            ->cannotBeEmpty()
                            ->info('URL is used to get galleries data')
                            ->example('service_url: http://cms-api.newhp.s3.test.insing.com/find-galleries')
                        ->end()
                        ->scalarNode('service_gallery_detail_url')
                            ->cannotBeEmpty()
                            ->info('URL is used to get gallery details data')
                            ->example('service_url: http://cms-api.newhp.s3.test.insing.com/gallery-details')
                        ->end()
                        ->scalarNode('service_find_contents_url')
                            ->cannotBeEmpty()
                            ->info('URL is used to get contents data')
                            ->example('service_url: http://cms-api.newhp.s3.test.insing.com/find-contents')
                        ->end()
                        ->scalarNode('service_series_all_url')
                            ->cannotBeEmpty()
                            ->info('URL is used to get series all data')
                            ->example('service_url: http://cms-api.newhp.s3.test.insing.com/series')
                        ->end()
                        ->scalarNode('service_galleries_prev_next')
                            ->cannotBeEmpty()
                            ->info('URL is used to get galleries prev next data')
                            ->example('service_url: http://cms-api.newhp.s3.test.insing.com/galleries-prev-next')
                        ->end()
                        ->scalarNode('service_articles_prev_next')
                            ->cannotBeEmpty()
                            ->info('URL is used to get articles prev next data')
                            ->example('service_url: http://cms-api.newhp.s3.test.insing.com/articles-prev-next')
                        ->end()
                        ->scalarNode('timeout')
                            ->cannotBeEmpty()
                            ->info('Timeout is used for limit process time')
                            ->example('timeout: 60')
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
