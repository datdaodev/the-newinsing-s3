<?php

namespace inSing\DataSourceBundle\Utilities\GeoIp2\Exception;

/**
 * This class represents a generic error.
 */
class GeoIp2Exception extends \Exception
{
}
