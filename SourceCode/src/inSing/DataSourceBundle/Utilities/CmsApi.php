<?php
namespace inSing\DataSourceBundle\Utilities;

use inSing\DataSourceBundle\Utilities\Curl;
use inSing\DataSourceBundle\Utilities\Constant;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CmsApi
{
    public $container;
    public $listChannel;
    public $countryCode;

    const CMS_SESSION_TOKEN_CACHE_KEY = 'CMS_SESSION_TOKEN_CACHE_KEY';
    const CMS_GALLERY_DETAIL_CACHE_KEY = 'CMS_GALLERY_DETAIL_CACHE_KEY';
    const CMS_GALLERY_IDS_MIN = 15000000;
    const CMS_GALLERY_IDS_MAX = 16000000;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->listChannel = $container->getParameter('cms_channel');
        $this->listChannelId = $container->getParameter('cms_channel_id');
        $this->countryCode = $container->getParameter('country_code');
    }

    public function authenticate()
    {
        try {
            $result_authenticate = $this->doPostWrapper(
                $this->container->getParameter('cms_url_auth'),
                array(
                    'access_token' => $this->container->getParameter('cms_access_token'),
                    'token_secret' => $this->container->getParameter('cms_token_secret')
                )
            );

            if (Curl::HTTP_OK != $result_authenticate['result_code']) {
                return null;
            }

            return array(
                "session_token" => $result_authenticate['session_token'],
                "expire_at" => $result_authenticate['expire_at']
            );
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }
    }

    /**
     * Get session token
     * @author Khuong.Phan
     * @return string sessionToken
     */
    public function getSessionToken() {
        try {
            $cmsSessionTokenCacheKey = self::CMS_SESSION_TOKEN_CACHE_KEY;
            $sessionToken = $this->container->get('hgw.cache')->fetch($cmsSessionTokenCacheKey);

            if (empty($sessionToken)) {
                $result = $this->authenticate();

                $sessionToken = $result['session_token'];
                //calculate the period time to cache
                $now = new \DateTime();
                $expireDate = new \DateTime($result['expire_at']);
                $cacheTime = $expireDate->getTimestamp() - $now->getTimestamp() - 600;

                //cache data on $cacheTime
                $this->container->get('hgw.cache')->save($cmsSessionTokenCacheKey, $sessionToken, $cacheTime);
            }
            return $sessionToken;
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }

        return null;
    }

    public function getTagsByCategoryId($categoryId) {
        try {
            $params = array(
                'session_token' => $this->getSessionToken(),
                'category_id' => $categoryId,
            );

            $tags = $this->doGetWrapper(
                    $this->container->getParameter('cms_url_tags'), $params
            );

            return $tags;
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }
    }

    public function getChannels() {
        try {
            $params = array(
                'session_token' => $this->getSessionToken(),
            );

            $channels = $this->doGetWrapper(
                $this->container->getParameter('cms_url_channels'),
                $params
            );

            return $channels;
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }
    }

//    public function getCategoriesByChannelId($channelId) {
//        try {
//            $params = array(
//                'session_token' => $this->getSessionToken(),
//                'channel_id' => $channelId
//            );
//
//            $categories = $this->doGetWrapper(
//                $this->container->getParameter('cms_url_categories'),
//                $params
//            );
//
//            return $categories;
//        } catch (\Exception $exc) {
//            $this->cmsLog($exc);
//        }
//    }

    /**
     * Note that this request only accepts HTTP GET.
     * get Article Details By Id
     * @param int $articleId
     * @param int $expired If 0: get the un-expired articles, If 1: get the expired articles, If 2: get all articles, Default value is 0.
     * @param int $image_height
     * @param int $image_width
     */
    public function getArticleDetailsById($articleId, $expired = 0, $image_height = null, $image_width = null, $country_code = '', $isPublish = true) {
        try {
            $params = array(
                'session_token' => $this->getSessionToken(),
                'article_id' => $articleId
            );

            if (!empty($expired)) {
                $params['expired'] = $expired;
            }

            if (!empty($image_height)) {
                $params['image_height'] = $image_height;
            }

            if (!empty($image_width)) {
                $params['image_width'] = $image_width;
            }

            if($country_code == '')
            {
                $country_code = $this->countryCode;
            }

            $article = $this->doGetWrapper(
                $this->container->getParameter('cms_url_article_details'),
                $params
            );

            if (Curl::HTTP_OK == $article['result_code'] && !empty($article['details'])) {
                $article['details'] = $this->parseArticle($article['details']);

                if ($country_code) {
                    if (isset($this->listChannel[$country_code]) && $channel = $this->listChannel[$country_code]) {
                        if ($article['details']['channel'] != $channel || ($isPublish == true && $article['details']['status'] != 1)) {
                            $empty = array(
                                'result_code' => 201,
                                'result_message' => 'The search query returned no results.',
                                'detail' => array()
                            );

                            return $empty;
                        }
                    }
                }
            }

            return $article;
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }
    }

    /**
     *
     * The getMultipleArticleDetails API is used to retrieve multiple articles in one call.
     * @param string $articleIds List Ids
     * @param int $expired : If 0: get the un-expired articles; If 1: get the expired articles; If 2: get all articles; Default value is 0.
     * @param int $page_number
     * @param int $page_size
     * @param int $image_height
     * @param int $image_width
     */
    public function getArticles($articleIds, $expired = 0, $page_number = 1, $page_size = 20, $image_height = null, $image_width = null) {
        try {
            $params = array(
                'session_token' => $this->getSessionToken(),
                'article_ids' => $articleIds,
                'expired' => $expired,
                'page_number' => $page_number,
                'page_size' => $page_size
            );

            if (!empty($image_height)) {
                $params['image_height'] = $image_height;
            }

            if (!empty($image_width)) {
                $params['image_width'] = $image_width;
            }

            $articles = $this->doGetWrapper(
                $this->container->getParameter('cms_url_multiple_article_details'),
                $params
            );

            return $articles;
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }
    }

    /**
     *
     * The findArticles is used to retrieve articles matching the search request.
     * Only accepts HTTP GET.
     * Remember to add "[]" to when you need to filter channels or tags
     * @param array $filters
     * @param int $page_number
     * @param int $page_size
     * @param array $includes
     * @param int $image_height
     * @param int $image_width
     */
    public function findArticles($filters = null, $page_number = 1, $page_size = 20, $includes = null, $random = null, $image_height = null, $image_width = null, $country_code = '') {
        try {
            if ($country_code == '') {
                $country_code = $this->countryCode;
            }

            $params = array(
                'session_token' => $this->getSessionToken(),
                'page_number' => $page_number,
                'page_size' => $page_size,
                'channel[]' => $this->listChannel[$country_code],
                'sort_by' => 'publish_date'
            );

            if (!empty($filters)) {
                $params = array_merge($params, $filters);
            }

            if (!empty($includes)) {
                $params = array_merge($params, $includes);
            }

            if (!empty($random)) {
                $params['random'] = $random;
            }

            if (!empty($image_height)) {
                $params['image_height'] = $image_height;
            }

            if (!empty($image_width)) {
                $params['image_width'] = $image_width;
            }

            // filter to remove params is empty/null
            $params = array_filter($params, function($val) {
                if ($val != '') {
                    return true;
                }
            });
            $articles = $this->doGetWrapper(
                $this->container->getParameter('cms_url_find_articles'),
                $params
            );

            if (Curl::HTTP_OK == $articles['result_code'] && !empty($articles['results'])) {
                for ($i = 0, $n = count($articles['results']); $i < $n; $i++) {
                    $articles['results'][$i] = $this->parseArticle($articles['results'][$i]);
                }
            }

            return $articles;
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }

        return null;
    }

    /**
     * Currently, This function is not in using.
     * @param array $ids
     * @param int $page_number
     * @param int $page_size
     * @return array API Result
     */
    public function findRelatedArticles(array $ids, $page_number = 1, $page_size = 20, $country_code = '') {
        try {
            $ret = '';
            if($country_code == '')
            {
                $country_code = $this->countryCode;
            }
            $params = array(
                'session_token' => $this->getSessionToken(),
                'page_number' => $page_number,
                'page_size' => $page_size,
                'id' => implode(',', $ids),
                'channel' => $this->listChannel[$country_code],
            );
            $articles = $this->doGetWrapper(
                    $this->container->getParameter('cms_url_find_related_articles'), $params
            );
            if (Curl::HTTP_OK == $articles['result_code'] && !empty($articles['results'])) {
                $ret = $articles['results'];
            } else {
                $ret = array();
            }
            return $ret;
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }
    }

    protected function parseArticle($article) {
        if (!empty($article)) {
            $article['createdOn'] = $article['created_date'];
            unset($article['created_date']);
            $article['publishOn'] = $article['publish_date'];
            unset($article['publish_date']);
            $article['updatedOn'] = $article['last_updated_date'];

            //Parse Series
            foreach ($article['series'] as $key => $series) {
                $article['series'][$series['seo_url']] = $series;
                unset($article['series'][$key]);
            }

            //Parse Cuisine
            foreach ($article['cuisines'] as $key => $cuisine) {
                $article['cuisines'][$cuisine['description']] = $cuisine;
                unset($article['cuisines'][$key]);
            }

            $article['description'] = (!empty($article['summary'])) ? $article['summary'] : '';
            $article['default_image'] = '';
            $article['rich_text'] = '';

            if (!empty($article['body'])) {
                usort($article['body'], function($a, $b) {
                    return $a['sort'] - $b['sort'];
                });

                foreach ($article['body'] as $bodyContent) {
                    switch ($bodyContent['module']) {
                        case 'default_image':
                            $article['default_image'] = $bodyContent['default_image'];
                            break;
                    }
                }
            } else {
                $article['body'] = '';
            }
        }
        $article['defaultImage'] = $article['default_image'];
        return $article;
    }

    /**
     *
     * The findGalleries is used to retrieve galleries matching the search request.
     * Only accepts HTTP GET.
     * Remember to add "[]" to when you need to filter channels or tags
     * @param array $filters
     * @param int $page_number
     * @param int $page_size
     * @param array $includes
     * @param string $random
     * @param int $image_height
     * @param int $image_width
     * @return array
     */
    public function findGalleries(array $filters = null, $page_number = 1, $page_size = 20, array $includes = null, $random = null, $image_height = null, $image_width = null, $country_code = '') {
        try {
            if ($country_code == '') {
                $country_code = $this->countryCode;
            }

            $params = array(
                'session_token' => $this->getSessionToken(),
                'page_number' => $page_number,
                'page_size' => $page_size,
                'channel[]' => $this->listChannel[$country_code],
                'gallery_type' => Constant::CMS_GALLERY_TYPE_ALL, //If 0: return galleries contain images, If 1: return galleries contains videos, If 2 : return all galleries
                'sort_by' => 'publish_date'
            );

            if (!empty($filters)) {
                $params = array_merge($params, $filters);
            }

            if (!empty($includes)) {
                $params = array_merge($params, $includes);
            }

            if (!empty($random)) {
                $params['random'] = $random;
            }

            if (!empty($image_height)) {
                $params['image_height'] = $image_height;
            }

            if (!empty($image_width)) {
                $params['image_width'] = $image_width;
            }

            $galleries = $this->doGetWrapper(
                $this->container->getParameter('cms_url_find_galleries'),
                $params
            );

            if (Curl::HTTP_OK == $galleries['result_code'] && !empty($galleries['results'])) {
                for ($i = 0, $n = count($galleries['results']); $i < $n; $i++) {
                    $galleries['results'][$i] = $this->parseGallery($galleries['results'][$i]);
                }
            }

            return $galleries;
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }

        return null;
    }

    /**
     *
     * The getListGuides is used to retrieve all guides.
     *
     * @param int $pageNum
     * @param int $pageSize
     * @param String $guideIds: format [guideId1,guideId2,...]
     * @return array
     */
    public function getListGuides($pageNum = 1, $pageSize = 100, $guideIds = '', $country_code = '') {
        $data = array();
        $url = '';
        if($country_code == '')
        {
            $country_code = $this->countryCode;
        }

        $params = array(
            'session_token' => $this->getSessionToken()
        );

        if (trim($guideIds) != '') {
            $url = 'cms_url_gallery_detail';
            $params['gallery_id'] = $guideIds;
        } else {
            $url = 'cms_url_find_galleries';
            $params['page_number'] = $pageNum;
            $params['page_size'] = $pageSize;
            $params['sort_by'] = 'publish_date';

            if($country_code == 'all'){
                $index = 0;
                foreach ($this->listChannel as $channel)
                {
                    $params['channel[' . $index++ . ']'] = $channel;
                }
            }
            else{
                $params['channel[]'] = $this->listChannel[$country_code];
            }
            $params['gallery_type'] = Constant::CMS_GALLERY_TYPE_GUIDE;
        }

        $reTry = 0;
        do {
            $data = $this->doGetWrapper($this->container->getParameter($url), $params);
            if (trim($guideIds) != '') {
                $data['results'] = $data['details'];
                unset($data['details']);
            }
            if (isset($data['results']['id'])) {
                $data['results'] = array($data['results']);
            }
            $reTry++;

            // do not get data from cms if exceed 3 times of re-try continuously
            if ($reTry >= 3) {
                break;
            }
        } while (Curl::HTTP_OK != $data['result_code'] || empty($data['results']));

        return $data;
    }

    /**
     * Get gallery detail by gallery id
     * @param int $gallery_id
     * @param int $expired [0: un-expired, 1: expired, 2: all]
     * @param int $image_height
     * @param int $image_width
     * @return array
     */
    public function getGalleryById($gallery_id, $expired = 0, $image_height = null, $image_width = null, $country_code = '', $isPreview = false) {
        try {
            $cache = $this->container->get('hgw.cache');
            $cacheKey = self::CMS_GALLERY_DETAIL_CACHE_KEY . $gallery_id . $expired . $image_height . $image_width;
            $gallery = $cache->fetch($cacheKey);
            if (empty($gallery))
            {
                if ($gallery_id < self::CMS_GALLERY_IDS_MIN) {
                    $gallery_id += self::CMS_GALLERY_IDS_MIN;
                }

                $params = array(
                    'session_token' => $this->getSessionToken(),
                    'gallery_id' => $gallery_id
                );

                if (!empty($expired)) {
                    $params['expired'] = $expired;
                }

                if (!empty($image_height)) {
                    $params['image_height'] = $image_height;
                }

                if (!empty($image_width)) {
                    $params['image_width'] = $image_width;
                }

                if($country_code == '')
                {
                    $country_code = $this->countryCode;
                }

                $gallery = $this->doGetWrapper(
                        $this->container->getParameter('cms_url_gallery_detail'), $params
                );
                if (Curl::HTTP_OK == $gallery['result_code'] && !empty($gallery['details'])) {
                    $gallery['details'] = $this->parseGallery($gallery['details']);
                    if ($country_code) {
                        if (isset($this->listChannel[$country_code]) && $channel = $this->listChannel[$country_code]) {

                        	if(isset($gallery['details']['channel'])){
	                            if ($gallery['details']['channel'] != $channel || ($gallery['details']['status'] != 1 && !$isPreview)) {
		                                $empty = array(
		                                    'result_code' => 201,
		                                    'result_message' => 'The search query returned no results.',
		                                    'detail' => array(),
		                                );
		                                return $empty;
	                            }
                        	}
                        }
                    }
                    $cache->save($cacheKey, $gallery, 300);
                }
            }

            return $gallery;
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }
    }

    protected function parseGallery($gallery) {
        if (!empty($gallery)) {

            //Parse Series
            if(isset($gallery['series'])){
	            foreach ($gallery['series'] as $key => $series) {
	                $gallery['series'][$series['seo_url']] = $series['description'];
	                unset($gallery['series'][$key]);
	            }
            }

            if(isset($gallery['summary'])){
            	$gallery['description'] = $gallery['summary'];
            }

            $gallery['totalImage'] = 0;
            $boilerPlateIds = array();
            //Get Default Image
            $gallery['defaultImage'] = array();
            if (!empty($gallery['images'])) {
                $gallery['totalImage'] = count($gallery['images']);

                //Get Main image
                $i = 0;
                foreach ($gallery['images'] as $arrImage) {
                    if ($arrImage['is_main'] == 1 || $i == 0) {
                        $gallery['defaultImage'] = $arrImage;
                    }
                    if ($arrImage['is_main'] == 1) {
                        break;
                    }
                    if ($arrImage['biz_id']) {
                        $boilerPlateIds[] = $arrImage['biz_id'];
                    }
                    $i++;
                }
            }
            $gallery['boilerPlateIds'] = $boilerPlateIds;
        }
        return $gallery;
    }

    public function getSeriesList($page_size = 20, $page_number = 1, $countryCode = '')
    {
        $channel = $this->listChannel[Constant::COUNTRY_CODE_SINGAPORE];

        if ($countryCode == '') {
        	$countryCode = $this->countryCode;
        }

        $channel = $this->listChannel[$countryCode];

        try {
            $params = array(
                'session_token' => $this->getSessionToken(),
                'page_number' => $page_number,
                'page_size' => $page_size,
                'channel[]' => $channel
            );

            $series = $this->doGetWrapper(
                $this->container->getParameter('cms_url_series'),
                $params
            );

            return $series;
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }
    }

    public function getCategories($countryCode = '')
    {
//        $channel_id = $this->listChannel[Constant::COUNTRY_CODE_SINGAPORE];

        if ($countryCode == '') {
            $countryCode = $this->countryCode;
        }

        $channel_id = $this->listChannelId[$countryCode];

        try {
            $params = array(
                'session_token' => $this->getSessionToken(),
                'channel_id' => $channel_id
            );

            $categories = $this->doGetWrapper(
                $this->container->getParameter('cms_url_categories'),
                $params
            );

            return $categories;
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }
    }

    public function getHGWSeriesList($countryCode) {
        $cache = $this->container->get('hgw.cache');
        $result = $cache->fetch('cms_api_list_of_series_hgw' . $countryCode);

        if (!$result) {
            $series = $this->getSeriesList(100, 0, $countryCode);
            if (Curl::HTTP_OK == $series['result_code'] && !empty($series['details'])) {
                foreach ($series['details'] as $value) {
                    if (isset($value['seo_url']) && !empty($value['seo_url'])) {
                        $result[$value['seo_url']] = $value;
                    }
                }
                $cache->save('cms_api_list_of_series_hgw' . $countryCode, $result, 24 * 60 * 60);
            }
        }
        return $result;
    }

    public function getHGWCategoriesList($countryCode) {
        $cache = $this->container->get('hgw.cache');
        $result = $cache->fetch('cms_api_list_of_categories_hgw' . $countryCode);

        if (empty($result)) {
            $categories = $this->getCategories($countryCode);
            if (Curl::HTTP_OK == $categories['result_code'] && !empty($categories['results'])) {
                foreach ($categories['results'] as $value) {
                    $result[$value['id']] = $value['name'];
                }
                $cache->save('cms_api_list_of_categories_hgw' . $countryCode, $result, 24 * 60 * 60);
            }
        }
        return $result;
    }

    /**
     *
     * @param int $article_id
     * @param array $category
     * @param array $tags
     * @return array('next' => gallery, 'previous' => gallery)
     */
    public function getNextPrevArticles($article_id, $category = null, $tags = null) {
        $result = null;
        try {
            $params = array(
                'session_token' => $this->getSessionToken(),
                'article_id' => $article_id
            );
            if ($category) {
                $params['category'] = is_array($category) ? implode('|', $category) : $category;
            }
            if ($tags) {
                $params['tag'] = is_array($tags) ? implode('|', $tags) : $tags;
            }

            $articles = $this->doGetWrapper(
                    $this->container->getParameter('cms_url_articles_prev_next'), $params
            );

            if (Curl::HTTP_OK == $articles['result_code'] && !empty($articles['results'])) {
                $result['next'] = isset($articles['results']['next']) && !empty($articles['results']['next']) ? $this->parseArticle($articles['results']['next']) : null;
                $result['previous'] = isset($articles['results']['previous']) && !empty($articles['results']['previous']) ? $this->parseArticle($articles['results']['previous']) : null;
            } else {
                return null;
            }
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }
        return $result;
    }

    /**
     *
     * @param int $gallery_id
     * @param array $category
     * @param array $tags
     * @return array('next' => gallery, 'previous' => gallery)
     */
    public function getNextPrevGalleries($gallery_id, $category = null, $tags = null) {
        $result = null;
        try {
            $params = array(
                'session_token' => $this->getSessionToken(),
                'gallery_id' => $gallery_id
            );
            if ($category) {
                $params['category'] = is_array($category) ? implode('|', $category) : $category;
            }
            if ($tags) {
                $params['tag'] = implode('|', $tags);
            }

            $galleries = $this->doGetWrapper(
                    $this->container->getParameter('cms_url_galleries_prev_next'), $params
            );

            if (Curl::HTTP_OK == $galleries['result_code'] && !empty($galleries['results'])) {
                $result['next'] = isset($galleries['results']['next']) ? $this->parseGallery($galleries['results']['next']) : null;
                $result['previous'] = isset($galleries['results']['previous']) ? $this->parseGallery($galleries['results']['previous']) : null;
            } else {
                return null;
            }
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }
        return $result;
    }

    /**
     *
     * The findStories is used to retrieve stories matching the search request.
     * Only accepts HTTP GET.
     * Remember to add "[]" to when you need to filter channels or tags
     * @param array $filters
     * @param int $page_number
     * @param int $page_size
     * @param array $includes
     * @param string $random
     * @param int $image_height
     * @param int $image_width
     */
    public function findStories($filters = null, $page_number = 1, $page_size = 20, $includes = null, $random = null, $image_height = null, $image_width = null, $country_code = '') {
        try {
            if ($country_code == '') {
                $country_code = $this->countryCode;
            }

            $params = array(
                'session_token' => $this->getSessionToken(),
                'page_number' => $page_number,
                'page_size' => $page_size,
                'tag_operator' => 'OR',
                'sort_by' => 'publish_date'
            );

            if ($country_code == 'all') {
                $index = 0;
                foreach ($this->listChannel as $channel)
                {
                    $params['channel[' . $index++ . ']'] = $channel;
                }
            } else {
                $params['channel[]'] = $this->listChannel[$country_code];
            }

            if (!empty($filters)) {
                $params = array_merge($params, $filters);
            }

            if (!empty($includes)) {
                $params = array_merge($params, $includes);
            }

            if (!empty($random)) {
                $params['random'] = $random;
            }

            if (!empty($image_height)) {
                $params['image_height'] = $image_height;
            }

            if (!empty($image_width)) {
                $params['image_width'] = $image_width;
            }

            // filter to remove params is empty/null
            $params = array_filter($params, function($val) {
                if ($val != '') {
                    return true;
                }
            });

            $stories = $this->doGetWrapper(
                $this->container->getParameter('cms_url_find_contents'),
                $params
            );

            if (Curl::HTTP_OK == $stories['result_code'] && !empty($stories['results'])) {
                for ($i = 0, $n = count($stories['results']); $i < $n; $i++) {
                    if ($stories['results'][$i]['type'] == 0) {
                        //Article
                        $stories['results'][$i] = $this->parseArticle($stories['results'][$i]);
                    } else {
                        //Gallery
                        $stories['results'][$i] = $this->parseGallery($stories['results'][$i]);
                    }
                }
            }

            return $stories;
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }

        return null;
    }

    protected function cmsLog($exc) {
        $this->container->get('monolog.logger.cmsapi')->error($exc->getCode() . ' ' . $exc->getMessage());
    }

    public function doGetWrapper($url, $params)
    {
        $params['sig'] = $this->genSignature($url, $params);

        $result = $this->container->get('curl')
                ->setMethod(Curl::HTTP_GET)
                ->setChannel('monolog.logger.cmsapi')
                ->call($url, $params);

        if ($result['result_code'] == Curl::SESSION_TOKEN_TIMEOUT || $result['result_code'] == Curl::HTTP_UNAUTHORIZED) {
            $this->container->delete(self::CMS_SESSION_TOKEN_CACHE_KEY);
            $sessionToken = $this->getSessionToken();
            $params['session_token'] = $sessionToken;
            $result = $this->container->get('curl')->setMethod(Curl::HTTP_GET)->call($url, $params);
        } else if ($result['result_code'] != Curl::HTTP_OK) {
            if (is_array($result)) {
                $this->container->get('monolog.logger.cmsapi')->error($url, $result);
            } else {
                $this->container->get('monolog.logger.cmsapi')->error('Result NULL: ' . $url, $params);
            }
        }

        return $result;
    }

    protected function doPostWrapper($url, $params)
    {
        $params['sig'] = $this->genSignature($url, $params);

        $result = $this->container->get('curl')
                ->setMethod(Curl::HTTP_POST)
                ->setChannel('monolog.logger.cmsapi')
                ->call($url, $params);
//        $this->container->get('monolog.logger.cmsapi')->info($url, $params);

        if ($result['result_code'] == Curl::SESSION_TOKEN_TIMEOUT || $result['result_code'] == Curl::HTTP_UNAUTHORIZED) {
            $this->container->delete(Curl::CMS_SESSION_TOKEN_CACHE_KEY);
            $sessionToken = $this->getSessionToken();
            $params['session_token'] = $sessionToken;
            $result = $this->container->get('curl')->setMethod(Curl::HTTP_POST)->call($url, $params);
        } else if ($result['result_code'] != Curl::HTTP_OK) {
            if (is_array($result)) {
                $this->container->get('monolog.logger.cmsapi')->error($url, $result);
            } else {
                $this->container->get('monolog.logger.cmsapi')->error('Result NULL: ' . $url, $params);
            }
        }

        return $result;
    }

    protected function genSignature($url, $params) {
        $consumerSecret = $this->container->getParameter('cms_consumer_secret');
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);
        $pathinfo = $consumerSecret . $pathinfo;
        $result = $pathinfo;
        //ksort($params, SORT_NATURAL);
        uksort($params, 'strnatcmp');
        $result = $pathinfo . http_build_query($params, '', '');
        $result = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $result);
        $result = str_replace('=', '', $result);
        return md5($result);
    }

    /**
     * form up variables in the correct format for HTTP
     *
     * @param $fields
     * @return $fields_string ( '&' seperate string )
     */
    private function getCallingString($url, $params) {
        $fields_string = $url . '?' . http_build_query($params, '', '&');
        return $fields_string;
    }

    /**
     * Find all Location
     * @param type $include_no_article
     * @param type $country_code
     * @return array
     * @author VuTranQ
     */
    public function getCountLocationContent($include_no_article = 0, $country_code = '')
    {
        $result = null;
        $result['regions_array'] = array();
        try {
            $params = array(
                'session_token' => $this->getSessionToken(),
                'include_no_article' => $include_no_article
            );

            $location = $this->doGetWrapper(
                    $this->container->getParameter('cms_url_get_count_location_content'), $params
            );

            if (Curl::HTTP_OK == $location['result_code'] && !empty($location['results']) && !empty($country_code)) {
                $result = $location['results'];
            } else {
                $result = array();
            }
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }

        return $result;
    }

    /**
     * get Region by Api Count Location Content
     * @param type $include_no_article
     * @param type $country_code
     * @return array
     * @author VuTranQ
     */
    public function getRegionsLocationContent($include_no_article = 0, $country_code = '') {
        $result = array();
        try {
            $location = $this->getCountLocationContent($include_no_article, $country_code);
            if (!empty($location) && isset($location['regions'])) {
                foreach ($location['regions'] as $region) {
                    $result[$region['name']] = $region['name'];
                }
            }
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }

        return $result;
    }
    
    /**
     * get Cities
     * @param type $include_no_article
     * @param type $country_code
     * @return array
     * @author VuTranQ
     */
    public function getCitiesLocationContent($include_no_article = 0, $country_code = '') {
        $result = array();
        try {
            $location = $this->getCountLocationContent($include_no_article, $country_code);
            if (!empty($location) && isset($location['cities'])) {
                foreach ($location['cities'] as $region) {
                    $result[$region['name']] = $region['name'];
                }
            }
        } catch (\Exception $exc) {
            $this->cmsLog($exc);
        }

        return array_unique($result);
    }
}