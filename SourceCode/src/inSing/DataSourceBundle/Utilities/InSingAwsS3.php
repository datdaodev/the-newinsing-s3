<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\DataSourceBundle\Utilities;

use Aws\S3\S3Client as S3Client;
use Aws\Common\Enum\Region as Region;

/**
 * Description of InSingAwsS3
 *
 * @author Dat.dao
 */
class InSingAwsS3 {

    protected $s3_client;

    /**
     * Constructor to create the S3 client
     *
     * @param string $appid The appid for accessing the S3 bicket.
     * @param string $secret The secret for accessing the S3 bicket.
     */
    public function __construct($appid, $secret) {
        $this->s3_client = S3Client::factory(array(
                    'key' => $appid,
                    'secret' => $secret,
                    'region' => Region::AP_SOUTHEAST_1,
                    'default_cache_config' => '/tmp',
        ));
    }

    public function getInstance() {
        return $this->s3_client;
    }

    /**
     * Put a file into the S3 bucket
     *
     * @param string $bucket Name of the bucket, i.e. insing-homepage-test,
     * @param string $key Key to store the file, i.e. homepage/abc/123/logo.png.
     * @param string $file Where the source file is located in local drive.
     * @param bool $overwrite Whether to overwrite existing file. Default to false.
     * @param bool $wait_for_object Whether to poll till object is available before returning. Default to false.
     * @return mixed Guzzle object if successful. Otherwise false will be returned.
     */
    public function put($bucket, $key, $file, $overwrite = false, $wait_for_object = false) {
        if (!$this->exist($bucket, $key) || $overwrite) {
            $result = $this->s3_client->putObject(array(
                'Bucket' => $bucket,
                'Key' => $key,
                'SourceFile' => $file,
            ));

            if ($wait_for_object) {
                // Poll the object until it is accessible
                $this->s3_client->waitUntilObjectExists(array(
                    'Bucket' => $bucket,
                    'Key' => $key,
                ));
            }

            return $result;
        } else {
            return false;
        }
    }

    /**
     * Put a resource of file into the S3 bucket
     *
     * @param string $bucket Name of the bucket, i.e. insing-homepage-test,
     * @param string $key Key to store the file, i.e. homepage/abc/123/logo.png.
     * @param string $resource Resource of file.
     * @param bool $overwrite Whether to overwrite existing file. Default to false.
     * @param bool $wait_for_object Whether to poll till object is available before returning. Default to false.
     * @return mixed Guzzle object if successful. Otherwise false will be returned.
     */
    public function putResource($bucket, $key, $resource, $overwrite = false, $wait_for_object = false) {
        
        if (!$this->exist($bucket, $key) || $overwrite) {
            $result = $this->s3_client->putObject(array(
                'Bucket' => $bucket,
                'Key' => $key,
                'Body' => $resource,
            ));
            if ($wait_for_object) {
                // Poll the object until it is accessible
                $this->s3_client->waitUntilObjectExists(array(
                    'Bucket' => $bucket,
                    'Key' => $key,
                ));
            }

            return $result;
        } else {
            return false;
        }
    }

    /**
     * Get a file from S3
     *
     * @param string $bucket Name of the bucket, i.e. insing-homepage-test,
     * @param string $key Key of the file, i.e. homepage/abc/123/logo.png.
     * @param string $local_file Path and name to save the file to.
     * @return string If $local_file is null, the content of the S3 file will be returned. Otherwise, it will return true. false the file does not exist.
     */
    public function get($bucket, $key, $local_file = null) {
        if (!$this->exist($bucket, $key))
            return false;

        if (!$local_file) {
            $result = $this->s3_client->getObject(array(
                'Bucket' => $bucket,
                'Key' => $key,
            ));
            return strval($result['Body']);
        } else {
            $result = $this->s3_client->getObject(array(
                'Bucket' => $bucket,
                'Key' => $key,
                'SaveAs' => $local_file,
            ));
            return true;
        }
    }

    /**
     * Copy file between buckets on S3
     *
     * @param string $bucket Name of the bucket source, i.e. insing-homepage-test,
     * @param string $bucket Name of the bucket destination, i.e. insing-homepage-backup,
     * @param string $key Key of the file, i.e. homepage/abc/123/logo.png.
     * @return mixed Guzzle object if successful. Otherwise false will be returned.
     */
    public function copy($bucket_source, $bucket_destination, $key_source, $key_destination, $override = false) {
        if (!$this->exist($bucket_source, $key_source) || ($this->exist($bucket_destination, $key_destination) && !$override))
            return false;
        $result = $this->s3_client->copyObject(array(
            'Bucket' => $bucket_destination,
            'Key' => $key_destination,
            'CopySource' => $bucket_source . '/' . urlencode($key_source)
        ));
        return $result;
    }

    /**
     * Delete file on bucket on S3
     *
     * @param string $bucket Name of the bucket, i.e. insing-homepage-test,
     * @param string $key Key of the file, i.e. homepage/abc/123/logo.png.
     * @return mixed Guzzle object if successful. Otherwise false will be returned.
     */
    public function delete($bucket, $key) {
        if (!$this->exist($bucket, $key))
            return false;
        $result = $this->s3_client->deleteObject(array(
            'Bucket' => $bucket,
            'Key' => $key
        ));
        return $result;
    }

    /**
     * Delete files on bucket on S3
     *
     * @param string $bucket Name of the bucket, i.e. insing-homepage-test,
     * @param string $key Key of the file, i.e. homepage/abc/123/logo.png.
     * @return mixed Guzzle object if successful. Otherwise false will be returned.
     */
    public function deleteObjects($bucket, array $keys) {

        foreach ($keys as $key => $value) {
            $keys[$key] = array('Key' => $value);
        }
        $result = $this->s3_client->deleteObjects(array(
            'Bucket' => $bucket,
            'Objects' => $keys
        ));

        return $result;
    }

    /**
     * Get a url of file from S3
     *
     * @param string $bucket Name of the bucket, i.e. insing-homepage-test,
     * @param string $key Key of the file, i.e. homepage/abc/123/logo.png.
     * @param string $expire Time expires, i.e 10 minutes, 1 days, 2 days.
     * @return string If Url object get successfull, Otherwise, it will return true. false the file does not exist.
     */
    public function getURL($bucket, $key, $expire = null) {
        if (!$this->exist($bucket, $key))
            return false;

        $expire = $expire != null ? '+' . $expire : null;
        $url_image = $this->s3_client->getObjectUrl($bucket, $key, $expire);
        return $url_image;
    }

    /**
     * Check if file is already in S3 bucket
     *
     * @param string $bucket Name of the bucket, i.e. insing-homepage-test,
     * @param string $key Key of the file, i.e. homepage/abc/123/logo.png.
     * @return bool true is found, false otherwise
     */
    public function exist($bucket, $key) {
        return $this->s3_client->doesObjectExist($bucket, $key);
    }

}
