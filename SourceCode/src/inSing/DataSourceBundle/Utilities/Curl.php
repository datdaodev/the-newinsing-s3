<?php
namespace inSing\DataSourceBundle\Utilities;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A wrapper class of CURL lib.
 *
 */
class Curl
{
    // HTTP Status Code constants
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_ACCEPTED = 202;
    const HTTP_NO_CONTENT = 204;
    const HTTP_RESET_CONTENT = 205;
    const HTTP_MOVED_PERMANENTLY = 301;
    const HTTP_FOUND = 302;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_PAYMENT_REQUIRED = 402;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_METHOD_NOT_ALLOWED = 405;
    const HTTP_NOT_ACCEPTABLE = 406;
    const HTTP_REQUEST_TIMEOUT = 408;
    const SESSION_TOKEN_TIMEOUT = 419;

    const MAX_EXECUTE_TIME = 5;

    /**
     *
     * @var ContainerInterface
     */
    public static $_container;

    /**
     * @var int
     */
    protected $timeout = 10;

    /**
     * @var int
     */
    protected $method;

    /**
     * @var boolean
     */
    protected $post_json = false;

    /**
     * @var array
     */
    protected $headers;

    /**
     * @var resource
     */
    protected $ch;

    /**
     * @var int
     */
    protected $startTime;

    /**
     * @var int
     */
    protected $stopTime;

    /**
     * @var string
     */
    protected $currentUrl;

    /**
     * @var Exception
     */
    protected $exception;

    /**
     * @var array
     */
    protected $lastError = null;

    /**
     * @var string
     */
    protected $channel = 'logger'; //For logger

    // define constants
    const HTTP_GET = 'CURLOPT_HTTPGET';
    const HTTP_POST = 'CURLOPT_POST';
    const HTTP_PUT = 'CURLOPT_PUT';
    const HTTP_DELETE = 'DELETE';

    /**
     * Constructor
     */
    public function __construct(ContainerInterface $container)
    {
        self::$_container = $container;
        // create a new cURL resource
        $this->ch = curl_init();

        if (!$this->ch) {
            throw new \Exception('Cant create cURL resource');
        }
    }

    /**
     * Get magic method
     *
     * @param string $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /**
     * Set headers
     *
     * @param string $header
     */
    public function setHeader($header)
    {
        $this->headers[] = $header;
    }

    /**
     * Set HTTP method
     *
     * @param int $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * Set post_json
     *
     * @param boolean $post_json
     */
    public function setPostJson($post_json)
    {
        $this->post_json = $post_json;
        return $this;
    }

    /**
     * Set option for cURL
     *
     * @param int $option_key
     * @param mixed $value
     */
    public function setOption($option_key, $value)
    {
        curl_setopt($this->ch, $option_key, $value);
    }

    /**
     * Set timeout for cURL
     *
     * @param mixed $value
     */
    public function setTimeout($value)
    {
        $this->timeout = $value;
    }

    /**
     * Get last error
     *
     * @return array
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    public function setChannel($channel) {
        $this->channel = $channel;
        return $this;
    }

    /**
     *
     * @param string $url
     * @param array $fields
     * @throws Exception
     * @return Ambigous <\inSing\DataSourceBundle\Lib\Returns, mixed>
     */
    public function call($url, $fields = array(), $listStringCustomise = false)
    {
        if (count($fields) > 0 && $this->method == self::HTTP_GET) {
            if($listStringCustomise) {
                $url = $url . '?' . $this->convertFieldsToStringCustomise($fields);
            }
            else {
                $url = $url . '?' . $this->convertFieldsToString($fields);
            }
        }

        $this->currentUrl = $url;
        self::$_container->get($this->channel)->info($url);

        //Debug: collect API calls
        self::$_container->get('common.utils')->collectApiCall($this->channel, $url);
        
        // set URL and other appropriate options
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, $this->timeout);

        if (empty($this->method)) {
            $this->method = self::HTTP_GET;
        }

        if ($this->method == self::HTTP_DELETE) {
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        } else {
            curl_setopt($this->ch, constant($this->method), true);
        }

        if (count($fields) > 0 && $this->method == self::HTTP_POST) {
            if ($this->post_json) {
                $post_fields = json_encode($fields);
                curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($post_fields))
                );
            } else {
                $post_fields = $this->convertFieldsToString($fields);
            }

            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_fields);
        }

        // execute the given cURL session
        $result = curl_exec($this->ch);

        // get stop time
        //$this->stopTime = UtilHelper::getMicroTimeInFloat();

        if (curl_errno($this->ch) != CURLE_OK) {
            $this->lastError = array(
                'url'    => $url,
                'method' => $this->method == self::HTTP_GET ? 'GET' : 'POST',
                'fields' => $this->convertFieldsToString($fields),
                'error_code' => curl_errno($this->ch),
                'error_str'  => curl_error($this->ch),
            );
            $result = false;
        } else {
            $this->lastError = null;
            $result = json_decode($result, true);
            if (is_array($result)) {
                array_walk_recursive($result, function(&$value) {
                    if (!is_array($value)) {
                        $value = html_entity_decode($value, ENT_QUOTES, "UTF-8");
                    }
                });
            }
        }

        //$this->notify();
        return $result;
    }

    /**
     * @author Cuong.Bui
     */
    public function cURLCall($url, $fields = array())
    {
//        $this->startTime  = UtilHelper::getMicroTimeInFloat();
        $this->currentUrl = $url;

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($this->ch, ( $this->method == self::HTTP_POST ? CURLOPT_POST : CURLOPT_HTTPGET), true);

        if (count($fields) > 0 && $this->method == self::HTTP_POST) {
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $this->convertFieldsToString($fields));
        }

        $result = curl_exec($this->ch);
//        $this->stopTime = UtilHelper::getMicroTimeInFloat();

        return $result;
    }

    /**
     * Convert an array of fields into string
     *
     * @param array $fields
     * @return string
     */
    protected function convertFieldsToString(array $fields)
    {
        $xp = array();

        foreach ($fields as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $xp[] = $k . '=' . urlencode($v);
                }
            } else {
                $xp[] = $key . '=' . urlencode($value);
            }
        }

        return implode('&', $xp);
    }

    /**
     * Convert an array of fields into string customise
     * @param array $fields
     * @return string
     * @author tri.van
     */
    protected function convertFieldsToStringCustomise(array $fields)
    {
        $xp = array();
        foreach ($fields as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $v) {
                    $xp[] = $key . '=' . urlencode($v);
                }
            } else {
                $xp[] = $key . '=' . urlencode($value);
            }
        }
    
        return implode('&', $xp);
    }
}