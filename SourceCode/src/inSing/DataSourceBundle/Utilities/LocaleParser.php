<?php

/**
 * Description of Locale parser
 *
 * @author LapTo
 */

namespace inSing\DataSourceBundle\Utilities;

/**
* 
*/
class LocaleParser
{    
    static $instance = null;

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
    * Parse address by locale
    * @author LapTo
    * @param array $restaurant
    * @param string $countryCode
    * @return string
    */
    public function parseBusinessAddress($restaurant, $countryCode, $breakLine = true) {
        if ($countryCode == Constant::COUNTRY_CODE_SINGAPORE) {
            $address_arr = array($restaurant['address']['unit_number'], $restaurant['address']['building_name'], 
                $restaurant['address']['block_number']);
            $address = '';
            $address_arr = array_filter($address_arr, function ($item) {
                return $item != '' ? true : false;
            });
            $address .= trim(join(', ', $address_arr) . ' ' . $restaurant['address']['street_name']);
            if ($restaurant['address']['post_code']) {
                $address .= ', ' . $restaurant['address']['post_code'];
            }
            return trim($address);
        } else {
            $address = '';
            if ($restaurant['address']['unit_number']) {
                $address .= $restaurant['address']['unit_number'];
            }
            if ($restaurant['address']['building_name']) {
                $address .= ' ' . $restaurant['address']['building_name'];
            }
            if ($restaurant['address']['block_number']) {
                $address .= ', ' . $restaurant['address']['block_number'];
            }
            if ($restaurant['address']['street_name']) {
                $address .= ' ' . $restaurant['address']['street_name'];   
            }
            $phase2 = '';
            if ($restaurant['address']['neighbourhood']) {
                $phase2 .= $restaurant['address']['neighbourhood'];   
            }
            if ($restaurant['address']['city']) {
                $phase2 .= ', ' . $restaurant['address']['city'];   
            }
            if ($restaurant['address']['state']) {
                $phase2 .= ', ' . $restaurant['address']['state'];   
            }
            if ($restaurant['address']['post_code']) {
                $phase2 .= ' ' . $restaurant['address']['post_code'];   
            }
            if ($phase2) {
                $phase2 = trim($phase2, ', ');
                if ($address) {
                    $address .= ', ' . ($breakLine ? '<br>' : '') . $phase2;    
                } else {
                    $address .= $phase2;    
                }                
            }
            $address = trim($address, ', ');
            return trim($address);
        }
    }

}