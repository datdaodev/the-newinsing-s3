<?php

namespace inSing\DataSourceBundle\Utilities;

use inSing\DataSourceBundle\Utilities\Common;

class ModuleUtils {

    /**
     * Parse data api
     *
     * @author Lap To
     * @param Controller $controller
     * @param Container $container
     * @param Array $apiResult
     * @param String $itemType
     */
    public static function parseDataApi($controller, $container, $apiResult, $itemType) {
        if ($apiResult['result_code'] == 200) {
            $defaultImageUrl = '';
            $seriesName = '';
            $seriesUrl = '';
            $frontEndUrl = $container->getParameter('frontend_url');
            $frontEndUrl = trim($frontEndUrl[$controller->getSelectedCountry()], '/');
            if ($itemType == 'article') {
                $defaultImageUrl = $apiResult['details']['default_image'];
                if (is_array($apiResult['details']['series'])) {
                    $key = key($apiResult['details']['series']);
                    $seriesName = $apiResult['details']['series'][$key];
                    $seriesUrl = $controller->generateUrl('in_sing_admin_dining_guide', array('series_slug' => $key));
                }
            } else if ($itemType == 'gallery') {
                if (is_array($apiResult['details']['series'])) {
                    $key = key($apiResult['details']['series']);
                    $seriesName = $apiResult['details']['series'][$key];
                    $seriesUrl = $controller->generateUrl('in_sing_admin_dining_guide', array('series_slug' => $key));
                }
                if (is_array($apiResult['details']['images']) && $tmp = array_shift($apiResult['details']['images'])) {
                    $defaultImageUrl = $tmp['image_url'];
                }
            } else if ($itemType == 'guide') {
                $apiResult['details'] = $apiResult['results'][0];
                unset($apiResult['results']);
                if (is_array($apiResult['details']['images']) && $tmp = array_shift($apiResult['details']['images'])) {
                    $defaultImageUrl = $tmp['image_url'];
                }
                if (is_array($apiResult['details']['series']) && $tmp = array_shift($apiResult['details']['series'])) {
                    $seriesName = $tmp['series_name'];
                    $seriesUrl = $controller->generateUrl('in_sing_admin_dining_guide', array('series_slug' => $tmp['seo_url']));
                }
                $apiResult['details']['description'] = $apiResult['details']['summary'];
            }
            $apiResult['details']['default_image_url'] = $defaultImageUrl;
            $apiResult['details']['series_name'] = $seriesName;
            $apiResult['details']['series_url'] = $frontEndUrl . $seriesUrl;
            $apiResult['details']['story_url'] = $frontEndUrl .
                    $controller->generateUrl('in_sing_admin_article_details', array(
                        'series_name' => Common::convertToSlug($seriesName),
                        'slug' => Common::convertToSlug($apiResult['details']['title']),
                        'id' => Common::idToUrlId($apiResult['details']['id'])
                    ));
        }
        return $apiResult;
    }

    /**
     * Parse RnR Api Result
     *
     * @author Lap To
     * @param Controller $controller
     * @param Container $container
     * @param Array $apiResult
     * @return Array
     */
    public static function parseRnRApiResult($controller, $container, $apiResult) {
        $result = array('review_image_url' => '',
                    'restaurant_name' => '',
                    'restaurant_url' => '',
                    'review_url' => '',
                    'review_title' => '',
                    'reviewer_display_name' => '',
                    'reviewer_profile_url' => '',
                    'reviewer_profile_pic_url' => '',
                    'review_vote' => '',
                    'restaurant_id' => '',
                    'number_of_review' => '',
                );
        if (is_array($apiResult['photos']) && $apiResult['photos']) {
            $photo = array_pop($apiResult['photos']);
            $result['review_image_url'] = $photo['url'];
        }
        if ($apiResult['item'] && is_array($apiResult['item'])) {
            $result['restaurant_name'] = $apiResult['item']['name'];
            $frontEndUrl = $container->getParameter('frontend_url');
            $frontEndUrl = trim($frontEndUrl[$controller->getSelectedCountry()], '/');
            $result['restaurant_url'] = $frontEndUrl . $controller->generateUrl('in_sing_hgwr_ibl', array(
                    'country_code' => $controller->getSelectedCountry(),
                    'state' => $apiResult['item']['state'],
                    'slug' => $apiResult['item']['slug'],
                ));
            $result['review_url'] = $frontEndUrl . $controller->generateUrl('in_sing_hgwr_ibl_reviews_detail_page', array(
                    'country_code' => $controller->getSelectedCountry(),
                    'slug' => $apiResult['item']['slug'],
                    'state' => $apiResult['item']['state'],
                    'hash_id' => Common::idToUrlId($apiResult['review']['review_id'])
                ));
            $result['restaurant_id'] = $apiResult['item']['restaurant_id'];
        }
        $result['review_title'] = $apiResult['review']['title'];

        if ($apiResult['user'] && is_array($apiResult['user'])) {
            $result['reviewer_display_name'] = $apiResult['user']['user_name'];
            $result['reviewer_profile_url'] = $container->getParameter('url_user_profile_insing') . $apiResult['user']['user_name'] . '/id-' . $apiResult['user']['user_id'];
            $result['reviewer_profile_pic_url'] = $apiResult['user']['user_avatar'];
            $result['number_of_review'] = intval($apiResult['user']['user_review']);
        }
        if ($apiResult['vote'] && is_array($apiResult['vote'])) {
            $result['review_vote'] = intval($apiResult['vote']['thumbs_up']);
        }

        return $result;
    }


    public static function getTimeslotCurrent($relativeTime = '') {
        if($relativeTime) {
            $now = new \DateTime($relativeTime);
        } else {
            $now = new \DateTime();
        }

        $now = $now->format('G:i');
        $currTimeArr = explode(':', $now);
        if($currTimeArr[1] == 0) {
            $i = '00';
            $h = $currTimeArr[0];
        } elseif($currTimeArr[1] > 0 && $currTimeArr[1] <= 30) {
            $i = '30';
            $h = $currTimeArr[0];
        } elseif($currTimeArr[1] > 30) {
            $i = '30';
            $h = $currTimeArr[0];
            if($currTimeArr[0] == 23) {
                $i = '00';
                $h = '00';
            } else {
                $i = '00';
                $h = $currTimeArr[0] + 1;
            }
        }
        if($h < 10) {
            $h = '0' . $h;
        }
        $meridian = ($h >= 12) ? ' pm' : ' am';
        $timeslot = array(
            'text' => ($h > 12 ? $h - 12 : $h) . ':' . $i . $meridian,
            'value' => $h . $i
        );

        return $timeslot;
    }

}
