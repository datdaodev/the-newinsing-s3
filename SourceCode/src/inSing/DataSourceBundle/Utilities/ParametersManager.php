<?php
namespace inSing\DataSourceBundle\Utilities;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Works with parameters.yml at a low level, extracts and writes back value of assets_version
 * Because parsing is done using regular expressions insead of YamlParser, formatting is preserved when writing value to file.
 *
 * @author Alexander Kachkaev <alexander@kachkaev.ru>
 *
 */
class ParametersManager
{
    static protected $versionParameterMask = '[a-zA-Z0-9_-]+';
    static protected $versionValuesMask = '[a-zA-Z0-9_\.-]*';

    protected $fileInput;
    protected $fileOutput;
    protected $parametersName;

    protected $fileContents;
    protected $versionValues;
    protected $oldVersionValues;
    protected $versionStartPositions;

    public function __construct($fileInput, $fileOutput)
    {
        $this->fileInput = $fileInput;
        $this->fileOutput = $fileOutput;

        $this->readFile();
    }

    /**
     * Gets the value of assets version found in parameters file
     *
     * @param boolean $rereadFile - if true, re-reads the file first
     */
    public function getVersion($rereadFile = false)
    {
        if ($rereadFile)
            $this->readFile();

        return $this->versionValues;
    }

    /**
     * Sets a new value for assets version found in parameters file
     *
     * Assets version must consist only of letters, numbers and the following characters: .-_
     *
     * @param boolean $rereadFile - if true, re-reads the file first
     */
    public function setValues($values)
    {
        $versionStartPositions = 0;
        foreach ($values as $key => $value)
        {
            if (!preg_match('/^' . static::$versionParameterMask . '$/', $key)) {
                throw new \InvalidArgumentException(
                        'Wrong value for parameter name '
                        . var_export($key, true)
                        . ' - it should consist only of characters, numbers and dash or underscore');
            }

            // Finding a row with corresponding parameter
            preg_match(
                    '/(\s+' . $key . '\:[^\S\n]*)('
                    . static::$versionValuesMask . ')\s*(\n|#)/',
                    $this->fileContents . "\n", $matches);
            if (array_key_exists(2, $matches)) {
                $this->oldVersionValues[$key] = $matches[2];
                $versionStartPositions = strpos($this->fileContents."\n", $matches[0]) + strlen($matches[1]);
            }
            else {
                throw new \Exception(
                        'Could not find definition of "' . $parameter
                        . '". Make sure it exists in "' . $this->fileInput
                        . '".');
            }

            // Checking value
            if (!is_string($value) && !is_numeric($value)) {
                throw new \InvalidArgumentException(
                        'Wrong value for assets version: '
                                . var_export($value, true)
                                . ' - it must be string or numeric.');
            }

            if (!preg_match('/^' . static::$versionValuesMask . '$/', $value)) {
                throw new \InvalidArgumentException(
                        'Wrong value for assets version: '
                                . var_export($value, true)
                                . '. It must be empty or consist only of letters, numbers and the following characters: .-_');
            }

            // Updating contents
            $this->fileContents = substr_replace($this->fileContents, $value,
                    $versionStartPositions, strlen($this->oldVersionValues[$key]));
            $this->versionValues[$key] = $value;
        }

        // Writing to file
        try {
            file_put_contents($this->fileOutput, $this->fileContents);
        } catch (\Exception $e) {
            throw new FileException(
                    'Could not write write "' . $this->fileOutput
                            . '". Make sure it exists and you have enough permissions.');
        }
    }

    /**
     * Reads and parses file with parameters
     *
     * @throws InvalidConfigurationException
     * @throws FileException
     * @throws \Exception
     */
    protected function readFile()
    {
        // Checking if file extension is supported
        $fileExtension = pathinfo($this->fileInput, PATHINFO_EXTENSION);
        if ($fileExtension != 'yml' && $fileExtension != 'yaml')
            throw new InvalidConfigurationException(
                    'Could not use "' . $this->fileInput
                            . '" - only yml files are supported by ParametersManager');

        // Reading file
        try {
            $this->fileContents = file_get_contents($this->fileInput);
        } catch (\Exception $e) {
            throw new FileException(
                    'Could not read file "' . $this->fileInput
                            . '". Make sure it exists and you have enough permissions.');
        }
    }
}
