<?php

/**
 * Description of Hgw Utils
 *
 * @author Vu Tran
 */

namespace inSing\DataSourceBundle\Utilities;

use inSing\DataSourceBundle\Utilities\Common;
use inSing\DataSourceBundle\Utilities\Constant;
use Symfony\Component\HttpKernel\Exception\HttpException;

class HgwUtils {

    /**
     * Get Promotion List
     *
     * @author Vu Tran
     * @param string $promotionUrl
     */
    public static function getPromotionList($promotionUrl) {
        return self::doCurlGet($promotionUrl);
    }

    /**
     * Calling CURL GET ( not CURL GET for RnR API & Business API )
     *
     * @author Vu Tran
     * @param $url
     * @throws Exception
     * @return Ambigous <>|multitype:
     */
    static public function doCurlGet($url) {
        try {
            // initialize cURL
            $ch = curl_init();

            // set options for cURL
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPGET, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);

            // execute HTTP POST request
            $response = curl_exec($ch);
            // close connection

            curl_close($ch);
            $ret = json_decode($response, true);
            if (isset($ret) && $ret['response']['status'] == RestfulAPIHelper::TBL_STATUS_OK) {
                return $ret['response'];
            }
            return array();
        } catch (\Exception $exc) {
            throw $exc;
        }
    }

    /**
     * Get Reservation Reminder
     *
     * @author Vu Luu
     * @param string $reservationUrl
     */
    public static function getReservation($reservationUrl)
    {
        return self::doCurlGet($reservationUrl);
    }

    static public function getSelectedCountry($container)
    {
        $request = $container->get('request');
        $country = $request->cookies->get('country_code_cookie_key', 'en_SG');
        if($country == 'null')
        {
            $country = 'en_SG';
        }

        return $country;
    }

    static public function getToken($container)
    {
        if (!$container->has('security.context')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }
        return $container->get('security.context')->getToken();
    }

    static public function getRoles($container) {

         if (!$container->has('security.context')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        $token = $container->get('security.context')->getToken();
        if(is_object($token))
        {
            $roles = $token->getRoles();
            array_walk($roles, function (&$role){ $role = $role->getRole();});
            return $roles;
        }
        return array();
    }

    static public function getRolesAccess($container, $role_on) {
        $roles = self::getRoles($container);
        $accept_roles = array();
        $admin_roles = $container->getParameter('ADMIN_ROLES');
        if(empty($roles))
        {
            return false;
        }
        if(isset($admin_roles[self::getSelectedCountry($container)][$role_on]))
        {
            $accept_roles = array_intersect($admin_roles[self::getSelectedCountry($container)][$role_on], $roles);
        }
        return array_values($accept_roles);
    }

    static public function getRolesAllAccess($container) {
        $roles = self::getRoles($container);
        $accept_roles = array();
        $admin_roles = $container->getParameter('ADMIN_ROLES');
        if(empty($roles))
        {
            return false;
        }
        if(isset($admin_roles['all']))
        {
            $accept_roles = array_intersect($admin_roles['all'], $roles);
        }
        return array_values($accept_roles);
    }

    static public function granted($container, $role_on) {
        $accept_roles = self::getRolesAccess($container, $role_on);
        return !empty($accept_roles) ? true: false;
    }

    static public function grantedSale($container, $role_on, array $users_access = array()) {

        if($role_on == Constant::ROLEON_LANDING_PAGE_CONTENT || $role_on == Constant::ROLEON_THEME_PAGE_CONTENT)
        {
            $user_name = self::getToken($container)->getUsername();
            $accept_roles = self::getRolesAllAccess($container);

            if(empty($accept_roles))
            {
                return false;
            }
            if(count($accept_roles) == 1 && $accept_roles[0] == 'ROLE_SALES')
            {
                if(in_array($user_name, $users_access))
                {
                    return true;
                }
            }
        }
        return false;
    }

    static public function isAdminHGWRegional($container) {
        $roles = self::getRoles($container);
        $admin_roles = $container->getParameter('ADMIN_ROLES');
        $country_code = self::getSelectedCountry($container) ;
        if(empty($roles))
        {
            return false;
        }

        if(isset($admin_roles[$country_code.'_all']))
        {
            $accept_roles = array_intersect($admin_roles[$country_code.'_all'], $roles);
            if(!empty($accept_roles))
            {
                return true;
            }

        }
        return false;
    }

    public static function applyCMSShortcodes(&$page_content, $cms_object) {
        $short_codes['[story-title]'] = isset($cms_object['title']) ? $cms_object['title'] : '';
        $short_codes['[story-description]'] = isset($cms_object['summary']) ? $cms_object['summary'] : '';
        $short_codes['[story-keywords]'] = isset($cms_object['seo_meta_keywords']) ? $cms_object['seo_meta_keywords'] : '';

        foreach ($page_content as $key => $value) {
            if(is_string($value))
            {
                $page_content[$key] = strtr($value, $short_codes);
            }
        }
    }

    public static function applyIBLShortcodes(&$page_content, $ibl_object) {
        $short_codes['[business-name]'] = isset($ibl_object['trading_name']) ? $ibl_object['trading_name'] : '';
        $short_codes['[business-branch]'] = isset($ibl_object['outlet_name']) ? $ibl_object['outlet_name'] : '';
        $short_codes['[business-description]'] = isset($ibl_object['description']) ? $ibl_object['description'] : '';
        $short_codes['[business-keywords]'] = '';
        if(isset($ibl_object['premium_types']['kind']) && isset($ibl_object['premium_types']['status']) && $ibl_object['premium_types']['status'] != 'active')
        {
            $premium_types = array( 'Premium' => 'premium_keywords', 'Top of List' => 'top_of_list_keywords', 'Consumer Guide' => 'consumer_guide_keywords' );
            $short_codes['[business-keywords]'] = $ibl_object[$premium_types[$ibl_object['premium_types']['kind']]];
        }
        foreach ($page_content as $key => $value) {
            if(is_string($value))
            {
                $page_content[$key] = strtr($value, $short_codes);
            }
        }
    }

    //https://jira.insing.com/jira/browse/FOOD-1730
    public static function generateKeywordsCMSForGPT($content) {

        try {

            $slug = Common::convertToSlug($content['title']);
            $content_tags['occasions'] = $content['occasions'];
            $content_tags['meals'] = $content['meals'];
            $content_tags['cuisines'] = $content['cuisines'];

            $tags_temp = array();
            if(!empty($content_tags))
            {
                array_walk($content_tags, function($value, $key) use(&$tags_temp){
                    if(in_array($key, array('cuisines', 'occasions', 'meals')))
                    {
                        $tags_temp = array_merge($tags_temp, $value);
                    }
                });
                array_walk($tags_temp, function(&$value, $key) use (&$tags_temp){
                    if(isset($value['description']) && !empty($value['description']))
                    {
                        $value = $value['description'];
                    }
                    else
                    {
                        unset($tags_temp[$key]);
                    }
                });
            };

            $tags = array_merge(array($content['title'], $slug, $content['id']), $tags_temp);

            return Common::parseKeywordForGPT($tags, true);
        } catch (\Exception $exc) {
        }
        return '';
    }

    public static function getArticleDetail($controller, $container, $request, $articleId, $series_name = '') {
        $articleDetailData = array();
        try {
            $previewTime = $request->get('preview_time', '');
            $sigRequest = $request->get('sig', '');
            $isPreview = false;
            if ($previewTime && $sigRequest) {
                $previewCmsSecretKey = $container->getParameter('preview_cms_content_secret');
                $currentTime = time();
                $timeLimit = $currentTime - $previewTime;
                $sigGenerate = Common::generateCmsContentSecretSignature($articleId, $previewTime, $previewCmsSecretKey);
                if ($timeLimit >= 0 && $timeLimit <= 3600) {
                    if ($sigGenerate == $sigRequest) {
                        $isPreview = true;
                    }
                }
                if (!$isPreview) {
                    throw new HttpException(406, Constant::UNABLE_TO_LOAD_PREVIEW_PAGE);
                }
            }
            $articleId = Common::urlIdToId($articleId);
            $country = $container->getParameter('country_code');
            $hgwrProjectDomains = $container->getParameter('hgwr_project_domains');
            $articleDetailCache = Constant::ARTICLE_DETAIL . $country . $articleId;
            $cache = $container->get('hgw.cache');
            $articleDetailData = !$isPreview ? $cache->fetch($articleDetailCache) : '';
            $movideoPlayer = $bizApis = array();

            if (!$articleDetailData) {
                $articleDetail = $container->get('cms.api')->getArticleDetailsById($articleId, 0, null, null, $country, false);
                if ($articleDetail['result_code'] != 200) {
                    throw $controller->createNotFoundException('Page Not Found!');
                }
                $articleDetail = isset($articleDetail['details']) ? $articleDetail['details'] : '';
                if (isset($articleDetail['status']) && $articleDetail['status'] != 1 && !$isPreview) {
                    throw $controller->createNotFoundException('Page Not Found!');
                }
                $moduleBoilerIds = array();
                $relatedIds = array();
                $related = array();
                if (!empty($articleDetail['body'])) {
                    foreach ($articleDetail['body'] as $k => $blockContent) {
                        if ($blockContent['module'] == 'rich_text') {
                            $articleDetail['body'][$k]['rich_text'] = Common::iframeCodeParser($blockContent['rich_text']);
                        }
                        if ($blockContent['module'] == 'video' && $blockContent['provider'] == 'Movideo') {
                            $movideoPlayer[] = $blockContent['video_id'];
                        }
                        if ($blockContent['module'] == 'business_boiler_plate') {
                            $moduleBoilerIds[$k] = $blockContent['business_id'];
                        }
                        if($blockContent['module'] == 'related_stories' && isset($blockContent['stories']) && !empty($blockContent['stories'])) {
                            $i = 1;
                            foreach ($blockContent['stories'] as $story) {
                                $aid = isset($story["related_story_{$i}_article_id"]) ? $story["related_story_{$i}_article_id"] : '';
                                if(isset($story['title']) && !empty($story['title'])&& !empty($aid) && ($aid != $articleId) && !in_array($aid, $relatedIds)) {
                                    $relatedIds[] = $aid;

                                    if($story['url']) {
                                        /**
                                         * [Articles] CMS Related Stories module
                                         * - fill in old format URL with food.insing.com domain
                                         * ticket : https://jira.insing.com/jira/browse/HGW-271
                                         * @author tri.van
                                         */
                                        if(substr($story['url'],0,8) == '/feature'){
                                            $story['url'] = ' http://food.insing.com'.$story['url'];
                                        }
                                    }

                                    $related[] = array(
                                        'text' => $story['title'],
                                        'url' => $story['url']
                                    );
                                }
                                $i++;
                            }
                        }
                    }
                }
                $articleDetail['objectMap'] = array();
                if (count($moduleBoilerIds)) {
                    $bizApis = $container->get('bizsearch.api')->getBusinessDetails($moduleBoilerIds, '256x218');
                    // Check this is restaurant or list of restaurants
                    $bizApis = isset($bizApis['id']) ? array($bizApis) : $bizApis;
                    foreach ($bizApis as $key => $biz) {
                        $bizApis[$biz['id']] = $biz;
                        unset($bizApis[$key]);
                    }
                    foreach ($articleDetail['body'] as $k => $blockContent) {
                        $biz = isset($blockContent['business_id']) && isset($bizApis[$blockContent['business_id']]) ? $bizApis[$blockContent['business_id']] : null;
                        if ($biz) {
                            $articleDetail['body'][$k]['bizApi'] = $biz;
                            $articleDetail['objectMap'][$biz['id']] = array(
                                'bizId' => $biz['id'],
                                'latitude' => $biz['latitude'],
                                'longitude' => $biz['longitude'],
                                'long_address' => $biz['long_address'],
                                'extendName' => $biz['name'],
                                'url' => ($biz['slug'] ? $container->get('router')->generate('in_sing_hgwr_ibl', array('state' => $biz['state'], 'slug' => $biz['slug']), true) : ''),
                            );
                        }
                    }
                }

                $articleDetail['gpt_keyword'] = self::generateKeywordsCMSForGPT($articleDetail);
                $articleDetail['imageUrl'] = isset($articleDetail['defaultImage']) ? $articleDetail['defaultImage'] : '';
                $articleDetail['largeImageUrl'] = '';
                if($articleDetail['imageUrl']) {
                    $articleDetail['largeImageUrl'] = $container->get('common.utils')->generatePhotoFromUrl($articleDetail['defaultImage'], '600x450', '600x450');
                }

                $series_slugs = array_keys($articleDetail['series']);
                if($series_name == '' && count($series_slugs))
                {
                    $series_name = $series_slugs[0];
                }
                $url = $container->get('router')->generate('in_sing_admin_article_details', array(
                    'series_name' => $series_name,
                    'slug' => Common::convertToSlug($articleDetail['title']),
                    'id' => Common::idToUrlId($articleId)
                ), true);
                $series_name_url = $series_name;
                //check if series name in url is in series name of article.
                if (!array_key_exists($series_name, $articleDetail['series'])) {
                    $series_name = null;
                    $seriesDescription = 'n-a';
                } else {
                    $seriesDescription = isset($articleDetail['series'][$series_name]['description'])
                        ? $articleDetail['series'][$series_name]['description'] : '';
                }
                $filters = array();
                if(isset($articleDetail['series']) && !empty($articleDetail['series'])) {
                    $idx = 0;
                    foreach ($articleDetail['series'] as $key => $item) {
                        $filters["tags[{$idx}]"] = $item['series_name'];
                        $idx++;
                    }
                }
                if(!empty($filters)) {
                    $filters['tag_operator'] = 'OR';
                    $seriesArticles = $container->get('cms.api')->findArticles($filters, 1, 10);
                    if(isset($seriesArticles['results']) && !empty($seriesArticles['results'])) {
                        $ii = 0;
                        foreach ($seriesArticles['results'] as $item) {
                            if($item['id'] != $articleId && !in_array($item['id'], $relatedIds)) {
                                $title = isset($item['title']) ? $item['title']: '';
                                $seri = isset($item['series']) ? key($item['series']) : '';
                                if(!$seri) {
                                    $seri = 'n-a';
                                }
                                $link = $container->get('router')->generate('in_sing_admin_article_details', array(
                                    'series_name' => $seri,
                                    'slug' => Common::convertToSlug($title),
                                    'id' => Common::idToUrlId($item['id'])
                                ), true);
                                $related[] = array(
                                    'text' => $title,
                                    'url' => $link
                                );
                                $ii++;
                                if($ii == 5) {
                                    break;
                                }
                            }
                        }
                    }
                }

                $twitterText = "{$articleDetail['title']} View on {$hgwrProjectDomains[$country]} ";
                $footerTextSeo = $container->getParameter('footer_text_seo_for_article_page');
                $articleDetailData = array(
                    'isPreview' => $isPreview,
                    'articleDetail' => $articleDetail,
                    'seriesName' => $series_name,
                    'series_name_url' => $series_name_url,
                    'seriesDescription' => $seriesDescription,
                    'twitterText' => $twitterText,
                    'url' => $url,
                    'related' => $related,
                    'articleId' => $articleId,
                    'movideoPlayer' => $movideoPlayer,
                    'getObjectMap' => $articleDetail['objectMap'],
                    'footerTextSeo' => $footerTextSeo[$controller->getCountry()],

                );
                if(!$isPreview) {
                    $cache->save($articleDetailCache, $articleDetailData, $container->getParameter('page_data_cache_time'));
                }
            }


        } catch (\Exception $e) {
            throw $controller->createNotFoundException($e->getMessage());
        }

        return $articleDetailData;
    }

    /**
     *
     * @param array $restaurant
     * @param int $time_slot, this is timeslot id
     * @param string $date, format Y-m-d
     * @return boolean
     * true if timeslot id is available
     * false if timeslot id is unavailable
     * @author Tri Van move from HGW old to HGWR
     */
    public static function timeslots_is_availables($restaurant, $time_slot, $date = null) {

        /*
         * Don't use this, The ticket is HGW-366
         * Also change function BusinessApiUtils::parseRestaurantDetails()
         $restaurant['hours_available'] = array();
         $restaurant['hours_unavailable'] = array();
         $restaurant['hours_unavailable_str'] = '';
         */
        return true;


        if(empty($restaurant['hours_available']))
        {
            return true;
        }
        $date = $date ?: date('Y-m-d');
        $key_date = '';

        // If hours_available exists public holiday
        if(isset($restaurant['hours_available'][$date]))
        {
            $key_date = $date;
        }
        else
        {
            $date = new \DateTime($date);
            $key_date = $date->format('D');
        }
        // if Day of Week exists in hour_available array
        if(!isset($restaurant['hours_available'][$key_date]))
        {
            return false;
        }

        $time_open = $restaurant['hours_available'][$key_date];
        if(empty($time_open))
        {
            return false;
        }
        foreach ($time_open as $value) {
            if(empty($value))
            {
                continue;
            }

            // If close hour is 00:00, it meant 24:00(No need to check for open hours)
            $close = intval(str_replace(':', '', $value['close']));
            if($close == 0)
            {
                $close = 2400;
            }
            if(intval($time_slot) >= intval(str_replace(':', '', $value['open']))
                    && intval($time_slot) <= $close)
            {
                return true;
            }
        }
        return false;
    }

}

