<?php
namespace inSing\DataSourceBundle\Utilities;

use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * RnRApiUtils class to help in calling RnR Api
 */
class RnRApiUtils
{
    const SUBMISSION_TYPE_REVIEW = 'review';
    const SUBMISSION_TYPE_LATEST_REVIEW = 'latest_review'; // only use for get submission list with multi user ids
    const SUBMISSION_TYPE_FAVOURITE = 'favourite';
    const SUBMISSION_TYPE_BLOGGER_REVIEW = 'blogger_review';
    const SUBMISSION_TYPE_RECOMENDATION = 'recommendation';
    const SUBMISSION_TYPE_PHOTOS = 'photo';

    const RNR_API_SESSION_TOKEN_CACHE_KEY = 'RNR_API_SESSION_TOKEN_CACHE_KEY';
    const CACHE_KEY_IBL_REVIEW_PHOTOS = 'CACHE_KEY_IBL_REVIEW_PHOTOS';
    const CACHE_KEY_USER_STATISTICS = 'CACHE_KEY_USER_STATISTICS';

    const USER_TYPE_BLOGGER = 'blogger';

    const USER_STATUS_ACTIVE = 'Active';
    const USER_STATUS_INACTIVE = 'Inactive';
    const USER_INACTIVE_NAME_MY = 'HGW user';

    public $container;
    public $listChannel;
    public $listUserType;
    public $countryCode;
    public $list_state;
    public $rnr_auth_id = null;
    public $rnr_auth_secret = null;
    public $rnr_auth_url = null;
    public $rnr_url = null;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->countryCode = $this->container->getParameter('country_code');
        $this->init();

    }

    private function init()
    {
        $this->listChannel = $this->container->getParameter('rnr_channel');
        $this->listUserType = $this->container->getParameter('rnr_user_type');

        //get App id and secrect by country
        $rnr_app = $this->container->getParameter('rnr_api_for_country');
        $this->rnr_auth_id = isset($rnr_app[$this->countryCode]['rnr_auth_id']) ? $rnr_app[$this->countryCode]['rnr_auth_id'] : null;
        $this->rnr_auth_secret = isset($rnr_app[$this->countryCode]['rnr_auth_secret']) ? $rnr_app[$this->countryCode]['rnr_auth_secret'] : null;
        $this->rnr_auth_url = isset($rnr_app[$this->countryCode]['rnr_auth_url']) ? $rnr_app[$this->countryCode]['rnr_auth_url'] : null;
        $this->rnr_url = isset($rnr_app[$this->countryCode]['rnr_url']) ? $rnr_app[$this->countryCode]['rnr_url'] : null;

        $state_default_locales = $this->container->getParameter('country_route_path_locale');
        $this->list_state = $state_default_locales;
    }

    public function switchCountry($country_code)
    {
        $this->countryCode = $country_code;
        $this->init();
        return $this;
    }

    /**
     * 4 SUBMISSION CREATE API
     *
     * Production - https://api.rnr.insing.com/submissions
     * Test - https://api.rnr.test.insing.com/submissions
     *
     * @author Vu Tran
     * @param Review Submit Data $data
     *
     * @return Submission ID
     */
    public function createSubmission($data)
    {
        try {
            $session_token = $this->getSessionToken();
            $data['session_token'] = $session_token;

            //Get Recommendation Item List from API
            $result = $this->doPostJsonWrapper(
                $this->rnr_url . 'submissions',
                $this->rnr_auth_secret,
                $data
            );

            // Return if not SUCCESS
            if (!is_array($result)) {
                return array(
                    'status' => 0,
                    'resultMessage' => 'Request failed!'
                );
            }

            return $result;
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }
    }

    /**
     * 6 SUBMISSION GET API - Get Review Information
     *
     * https://api.rnr.insing.com/submission/{submission_id}
     *
     * @author Vu Tran
     * @param string $submission_id ( review_id )
     */
    public function getSubmissionDetails($submission_id, $more_info = false, &$message = '', $avartarSize = '48x48' ) {
        try {
            if(is_array($submission_id)) {
                $submission_id = implode('|', $submission_id);
            }
            $session_token = $this->getSessionToken();

            if (is_null($session_token))
                return array();

            //Get Recommendation Item List from API
            $apiResult = $this->doGetWrapper(
                    $this->rnr_url . "submission/$submission_id",
                    $this->rnr_auth_secret,
                    array(
                            'session_token' => $session_token,
                            'more_info' => $more_info ? 'true' : 'false'
                    ));
            //Return if not SUCCESS
            if (RestfulAPIHelper::HTTP_OK != $apiResult['status'] || $apiResult['submission'] == null) {
                $message = $apiResult['status'];
                return array();
            }
            $country_code = '';
            $submissionList = array();
            $apiResult['submission'] = isset($apiResult['submission'][0]) ? $apiResult['submission'] : array($apiResult['submission']);
            foreach($apiResult['submission'] as $key => $result) {
                $ret = $result;
                unset($result);
                $result['submission'] = $ret;
                foreach($this->listChannel as $country => $itemType)
                {
                    if($itemType == $result['submission']['item_type'])
                    {
                        $country_code = $country;
                        break;
                    }
                }

                $userId = $result['submission']['user_id'];
                $user = isset($result['submission']['user']) ? $result['submission']['user'] : array();
                $avatar = $this->container->getParameter('default_user_avatar_url');
                if (isset($user['avatar_url']) && $user['avatar_url']) {
                    $avatar = $this->container->get('common.utils')->generatePhotoFromUrl($user['avatar_url'], $avartarSize);
                } elseif (isset($user['avatar_id'])) {
                    $avatar = $user['avatar_id'] ? $this->container->get('common.utils')->generateMediaUrl($user['avatar_id'], 'pc_' . $avartarSize) : $avatar;
                }

                $nick_name = isset($user['name']) ? trim($user['name']) : '';
                if(empty($nick_name) && isset($user['insing_user_name']))
                {
                    $nick_name = trim($user['insing_user_name']);
                }
                if(empty($nick_name) && isset($user['facebook_name']))
                {
                    $nick_name = trim($user['facebook_name']);
                }
                $user['name'] = $nick_name;

                $fullName = $nick_name;

                if (isset($result['submission']['review']['response']) && !empty($result['submission']['review']['response'])) {
                    $charBreakDown   = array("\\n\\r", "\\r\\n","\\r","\\n", "\n\r", "\r\n","\r","\n");
                    foreach ($result['submission']['review']['response'] as $k=>$response) {
                        $result['submission']['review']['response'][$k]['body'] = str_replace($charBreakDown, '<br />', "$response[body]");
                    }
                }

                isset($result['submission']['user']['avatar_url']) && !empty($result['submission']['user']['avatar_url'])?
                    $avata_url = $this->container->get('common.utils')->generatePhotoFromUrl($result['submission']['user']['avatar_url'], $avartarSize)  :
                    $avata_url = $this->container->getParameter('default_user_avatar_url') ;
                $items_type = $this->container->getParameter('rnr_channel');

                $state = $this->list_state[$this->countryCode];

                foreach ($items_type as $country_code => $item_type)
                {
                    if ($item_type == $result['submission']['item_type'])
                    {
                        $state = $this->list_state[$country_code];
                        break;
                    }
                }

                if(isset($result['submission']['item']) && !empty($result['submission']['item']) && (!isset($result['submission']['item']['state']) || empty($result['submission']['item']['state'])))
                {
                    $result['submission']['item']['state'] = $state;
                }

                $userStatus = isset($result['submission']['user']['status']) ? $result['submission']['user']['status'] : '';
                $isActive = (strtolower($userStatus) == strtolower(self::USER_STATUS_ACTIVE)) ? true : false;
                $isBlogger = false;
                if (isset($result['submission']['user_type']) && $result['submission']['user_type'] == 'blogger' && $isActive == false) {
                    $isBlogger = true;
                }
                $result['submission']['user'] = array(
                    'user_id' => !empty($userId) ? $userId : 0,
                    'user_avatar' => $avatar,
                    'avatar_url' => $avata_url,
                    'user_name' => $nick_name,
                    'user_fullname' => $fullName,
                    'user_review' => isset($user['reviews_count']) ? $user['reviews_count'] : 0,
                    'user_follow' => isset($user['followers_count']) ? $user['followers_count'] : 0,
                    'is_blogger' => $isBlogger,
                    'is_active' => $isActive
                );
                $submissionList[$key] = $result['submission'];
            }
            $submissionList = count($submissionList) > 1 ? $submissionList : $submissionList[0];

            return $submissionList;
        } catch (\Exception $exc) {
            $message = $exc->getCode();
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }
        return array();
    }


    /**
     * 7 SUBMISSION UPDATE API
     *
     * Production - https://api.rnr.insing.com/submissions/{submission_id}
     * Test - https://api.rnr.test.insing.com/submissions/{submission_id}
     *
     * @author Vu Tran
     * @param Review Submit Data $data
     *
     * @return Submission ID
     */
    public function updateSubmission($submission_id, $data ) {
        try {
            $session_token = $this->getSessionToken();

            if (is_null($session_token))
                return array();

            $data['session_token'] = $session_token;

            //Get Recommendation Item List from API
            $result = $this->doPostJsonWrapper(
                    $this->rnr_url . "submission/$submission_id",
                    $this->rnr_auth_secret,
                    $data,
                    RestfulAPIHelper::RESTFUL_API_PUT
            );

            //Return if not SUCCESS
            if (RestfulAPIHelper::HTTP_OK != $result['status'])
                return array();

            return $result;
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }
    }

    /**
     * 18 ITEM LIST GET API
     *
     * https://api.rnr.insing.com/items
     *
     * @author thai.pham@s3corp.com.vn
     * @param string $item_type ( Hgw : food )
     * @param Array $item_id ( Hgw : restaurant_id )
     */
    public function getItemListApi(array $item_id_list, $category='', $page = 1, $perpage = 10, $more_info = false, $country_code = '' ) {
        try {
            $session_token = $this->getSessionToken();

            if (is_null($session_token)) {
                return array();
            }

            if($country_code == '')
            {
                $country_code = $this->countryCode;
            }
            $item_type = $this->listChannel[$country_code];

            $rnr_more_info = $this->container->getParameter('rnr_use_more_info');
            $use_more_info = isset($rnr_more_info[$country_code]) ? $rnr_more_info[$country_code] : false;

            $more_info_str = $more_info ? 'true' : 'false';
            if(!$use_more_info)
            {
                $more_info_str = 'false';
            }

            $params = array(
                'session_token' => $session_token,
                'item_type' => $item_type,
                'page' => $page,
                'per_page' => $perpage,
                'item_id' => implode(',', $item_id_list),
                'more_info' => $more_info_str
            );
            if (!empty($category)) {
                $params['category'] = $category;
            }

            //Get Recommendation Item List from API
            $result = $this->doGetWrapper(
                $this->rnr_url . "items",
                $this->rnr_auth_secret,
                $params
            );

            //Return if not SUCCESS
            if (RestfulAPIHelper::HTTP_OK != $result['status'])
                return array();

            if(isset($result['items']) && !empty($result['items'])) {
                foreach ($result['items'] as $key => $item)
                {
                    if(isset($item['item']) && !empty($item['item']) && (!isset($item['item']['state']) || empty($item['item']['state'])))
                    {
                        $result['items'][$key]['item']['state'] = $this->list_state[$country_code];
                    }



                    //HGW-99 MY use insing_user_name if empty, use name.

                    // Remove logic from HGW-99, because we're using the Account API, it should be the same logic for all locales

                    // Lastest positive review
                    if(!empty($result['items'][$key]['latest_positive_review']))
                    {
                        $nick_name = isset($result['items'][$key]['latest_positive_review']['user']['name']) ? trim($result['items'][$key]['latest_positive_review']['user']['name']) : '';
                        if(empty($nick_name) && isset($result['items'][$key]['latest_positive_review']['user']['insing_user_name']))
                        {
                            $nick_name = trim($result['items'][$key]['latest_positive_review']['user']['insing_user_name']);
                        }
                        if(empty($nick_name) && isset($result['items'][$key]['latest_positive_review']['user']['facebook_name']))
                        {
                            $nick_name = trim($result['items'][$key]['latest_positive_review']['user']['facebook_name']);
                        }
                        $result['items'][$key]['latest_positive_review']['user']['name'] = $nick_name;
                    }

                    // Latest review
                    if(!empty($result['items'][$key]['latest_review']))
                    {
                        $nick_name = isset($result['items'][$key]['latest_review']['user']['name']) ? trim($result['items'][$key]['latest_review']['user']['name']) : '';
                        if(empty($nick_name) && isset($result['items'][$key]['latest_review']['user']['insing_user_name']))
                        {
                            $nick_name = trim($result['items'][$key]['latest_review']['user']['insing_user_name']);
                        }
                        if(empty($nick_name) && isset($result['items'][$key]['latest_review']['user']['facebook_name']))
                        {
                            $nick_name = trim($result['items'][$key]['latest_review']['user']['facebook_name']);
                        }
                        $result['items'][$key]['latest_review']['user']['name'] = $nick_name;
                    }

                    // Favourite review
                    if(!empty($result['items'][$key]['fav_review']))
                    {
                        $nick_name = isset($result['items'][$key]['fav_review']['user']['name']) ? trim($result['items'][$key]['fav_review']['user']['name']) : '';
                        if(empty($nick_name) && isset($result['items'][$key]['fav_review']['user']['insing_user_name']))
                        {
                            $nick_name = trim($result['items'][$key]['fav_review']['user']['insing_user_name']);
                        }
                        if(empty($nick_name) && isset($result['items'][$key]['fav_review']['user']['facebook_name']))
                        {
                            $nick_name = trim($result['items'][$key]['fav_review']['user']['facebook_name']);
                        }
                        $result['items'][$key]['fav_review']['user']['name'] = $nick_name;
                    }

                    if (isset($item['fav_review']) && !empty($item['fav_review'])) {
                        if (isset($item['fav_review']['user']['name'])) {
                            $userStatus = isset($item['fav_review']['user']['status']) ? $item['fav_review']['user']['status'] : '';
                            $isActive = (strtolower($userStatus) == strtolower(self::USER_STATUS_ACTIVE)) ? true : false;
                            $result['items'][$key]['fav_review']['user']['is_active'] = $isActive;

                            $isBlogger = false;
                            if (isset($item['fav_review']['user_type']) && $item['fav_review']['user_type'] == 'blogger' && $isActive == false) {
                                $isBlogger = true;
                            }
                            $result['items'][$key]['fav_review']['user']['is_blogger'] = $isBlogger;
                        }
                    } else if (isset($item['latest_positive_review']) && !empty($item['latest_positive_review'])) {
                        if (isset($item['latest_positive_review']['user']['name'])) {
                            $userStatus = isset($item['latest_positive_review']['user']['status']) ? $item['latest_positive_review']['user']['status'] : '';
                            $isActive = (strtolower($userStatus) == strtolower(self::USER_STATUS_ACTIVE)) ? true : false;
                            $result['items'][$key]['latest_positive_review']['user']['is_active'] = $isActive;

                            $isBlogger = false;
                            if (isset($item['latest_positive_review']['user_type']) && $item['latest_positive_review']['user_type'] == 'blogger' && $isActive == false) {
                                $isBlogger = true;
                            }
                            $result['items'][$key]['latest_positive_review']['user']['is_blogger'] = $isBlogger;
                        }
                    }
                }
            } else {
                $result['items'] = array();
            }

            return $result['items'];
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }

        return array();

    }


    /**
     * 20 ITEM RECOMMENDATION LIST API
     *
     * https://api.rnr.insing.com/item/{item_type}/{item_id}/recommendations
     *
     * @author Vu Tran
     * @param string $item_type ( Hgw : food )
     * @param string $item_id ( Hgw : restaurant_id )
     */
    public function getItemRecommendationListApi( $item_type, $item_id, $options = null ) {
        //Add some defaults search Values if Options is NULL
        if (is_null($options)) {
            $options = array(
                'per_page' => 10,
                'keyword' => ''
            );
        }

        try {
            $session_token = $this->getSessionToken();

            if (is_null($session_token))
                return array();

            //Get Recommendation Item List from API
            $result = $this->doGetWrapper(
                $this->rnr_url . "item/$item_type/$item_id/recommendations",
                $this->rnr_auth_secret,
                array(
                    'session_token' => $session_token,
                    'per_page' => $options['per_page'],
                    'keyword' => $options['keyword']
            ));

            //Return if not SUCCESS
            if (RestfulAPIHelper::HTTP_OK != $result['status'])
                return array();

            foreach ($result['recommendations'] as $key => $item)
            {
                if(isset($item['item']) && !empty($item['item']) && (!isset($item['item']['state']) || empty($item['item']['state'])))
                {
                    $result['recommendations'][$key]['item']['state'] = $this->list_state[$this->countryCode];
                }
            }

            return $result['recommendations'];
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }
        return array();
    }


    /**
     * 24 SUITABLE FOR LIST API
     *
     * https://api.rnr.insing.com/suitable_for/{item_type}
     *
     * @param string $item_type
     */
    public function getSuitableForListApi($item_type = Constant::FOOD)
    {
        try {
            $session_token = $this->getSessionToken();

            if (is_null($session_token) ) return array();

            //Get Suitable For List from API
            $suitable_for_list = $this->doGetWrapper(
                $this->rnr_url . "suitable_for/$item_type",
                $this->rnr_auth_secret,
                array(
                            'session_token' => $session_token
                            )
            );
            //Return if not SUCCESS
            if (RestfulAPIHelper::HTTP_OK != $suitable_for_list['status'])
                return array();

            return $suitable_for_list['suitable_fors'];
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }

        return null;
    }

    public function authenticate()
    {
        try {
            $result_authenticate = $this->doPostWrapper(
                $this->rnr_auth_url,
                $this->rnr_auth_secret,
                array(
                    'app_id' => $this->rnr_auth_id,
                    'secret' => $this->rnr_auth_secret
                )
            );

            if (RestfulAPIHelper::HTTP_OK != $result_authenticate['status']) {
                return null;
            }

            return array(
                'session_token' => $result_authenticate['session_token'],
                'expire_at' => $result_authenticate['expire_at']
            );
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->info($exc->getCode() . ':' . $exc->getMessage());
        }
    }

    /**
     * Get session token
     * @author Date Tran
     * @return string sessionToken
     */
    public function getSessionToken()
    {
        try {
            $cache = $this->container->get('hgw.cache');
            $sessionToken = $cache->fetch(self::RNR_API_SESSION_TOKEN_CACHE_KEY . $this->rnr_auth_id);
            if (empty($sessionToken)) {
                $result = $this->authenticate();
                if (!empty($result) && count($result)) {
                    $sessionToken = $result['session_token'];
                    $cache->save(self::RNR_API_SESSION_TOKEN_CACHE_KEY . $this->rnr_auth_id, $sessionToken);
                }
            }
            return $sessionToken;
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }

        return null;
    }


    /**
     *
     * @param int $per_page
     * @param int $page
     * @param int $item_id
     * @param string $submission_type
     * @param int $user_id
     * @param boolean $more_info
     * @param string $category
     * @return array
     */
    public function getSubmissionList($per_page = 3, $page = 1, $item_id = 0, $submission_type = self::SUBMISSION_TYPE_REVIEW, $user_id = '', $more_info = false, $category = '', $country_code = '', $draft = false, $other_field = array())
    {
        if($country_code == '')
        {
            $country_code = $this->countryCode;
        }
        $this->switchCountry($country_code);
        $token = $this->getSessionToken();
        $all_data = array('submission' => array(), 'total' => 0);

        $user_type = 'insing';

        $item_type = $this->listChannel[$country_code];
        $user_type = $this->listUserType[$country_code];

        $rnr_more_info = $this->container->getParameter('rnr_use_more_info');
        $use_more_info = isset($rnr_more_info[$country_code]) ? $rnr_more_info[$country_code] : false;

        $more_info_str = $more_info ? 'true' : 'false';
        if(!$use_more_info)
        {
            $more_info_str = 'false';
        }

        try {
            $fields = array(
                'session_token' => $token,
                'page' => $page,
                'item_type' => $item_type,
                'per_page' => $per_page,
                'more_info' => $more_info_str,
            );

            if (!empty($other_field)) {
                $fields = array_merge($fields, $other_field);
            }

            if ($draft == true) {
                $fields['draft'] = 'true';
            }

            if ($category) {
                $fields['item_category'] = $category;
            }
            if ($user_id) {
                if(is_array($user_id))
                {
                    $user_id = implode(',', $user_id);
                }
                $fields['user_id'] = $user_id;
                $fields['user_type'] = $user_type;
            }

            if ($submission_type != '') {
                if($submission_type == self::SUBMISSION_TYPE_FAVOURITE)
                {
                    $per_page = 1;
                }
                $fields['filter'] = $submission_type;
            }

            if($item_id)
            {
              $fields['item_id'] = $item_id;
            }

            $data = $this->doGetWrapper($this->rnr_url . 'submissions', $this->rnr_auth_secret, $fields);

            $all_data['total'] = isset($data['total']) ? $data['total'] : 0;
            $all_data['submission'] = isset($data['submission']) ? $data['submission'] : array();
            $isParseMore = ($country_code == Constant::COUNTRY_CODE_MALAYSIA );

            if(!empty($all_data['submission'])) {
                foreach ($all_data['submission'] as $key => $submission)
                {
                    if(isset($submission['item']) && !empty($submission['item']) && (!isset($submission['item']['state']) || empty($submission['item']['state'])))
                    {
                        $all_data['submission'][$key]['item']['state'] = $this->list_state[$country_code];
                    }

                    $nick_name = isset($all_data['submission'][$key]['user']['name']) ? trim($all_data['submission'][$key]['user']['name']) : '';
                    if(empty($nick_name) && isset($all_data['submission'][$key]['user']['insing_user_name']))
                    {
                        $nick_name = trim($all_data['submission'][$key]['user']['insing_user_name']);
                    }
                    if(empty($nick_name) && isset($all_data['submission'][$key]['user']['facebook_name']))
                    {
                        $nick_name = trim($all_data['submission'][$key]['user']['facebook_name']);
                    }
                    $all_data['submission'][$key]['user']['name'] = $nick_name;

                    if (isset($submission['user']['name'])) {
                        $userStatus = isset($submission['user']['status']) ? $submission['user']['status'] : '';
                        $isActive = (strtolower($userStatus) == strtolower(self::USER_STATUS_ACTIVE)) ? true : false;
                        $all_data['submission'][$key]['user']['is_active'] = $isActive;

                        $isBlogger = false;
                        if (isset($submission['user_type']) && $submission['user_type'] == 'blogger' && $isActive == false) {
                            $isBlogger = true;
                        }
                        $all_data['submission'][$key]['user']['is_blogger'] = $isBlogger;
                    }
                }
            }

        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }

        return $all_data;
    }

    /**
     * get User Photos By restaurant Id
     * @param $itemId
     * @param $per_page
     * @param $page
     */
    public function getUserPhotosByRestaurantId($itemId = '', $per_page = 10, $page = 1, $more_info = true, $country_code = '', $no_cache = false) {
        $results = array();
        try {
            if($country_code == '')
            {
                $country_code = $this->countryCode;
            }
            $this->switchCountry($country_code);
            $token = $this->getSessionToken();
            $imageCache = $this->container->get('hgw.cache');
            $cacheConstant = $country_code . self::CACHE_KEY_IBL_REVIEW_PHOTOS . $itemId . '_perpage' . $per_page . '_page' . $page . strval($more_info);

            $rnr_more_info = $this->container->getParameter('rnr_use_more_info');
            $use_more_info = isset($rnr_more_info[$country_code]) ? $rnr_more_info[$country_code] : false;

            $more_info_str = $more_info ? 'true' : 'false';
            if(!$use_more_info)
            {
                $more_info_str = 'false';
            }

            if (!$imageCache->contains($cacheConstant) || $no_cache) {
                $item_type = $this->listChannel[$country_code];
                $fields = array(
                    'session_token' => $token,
                    'item_type'     => $item_type,
                    'item_id'       => $itemId,
                    'per_page'      => $per_page,
                    'page'          => $page,
                    'more_info' => $more_info_str
                );
                $results = $this->doGetWrapper($this->rnr_url . 'photos', $this->rnr_auth_secret, $fields);
                if($results) {
                    if(isset($results['photos']) && !empty($results['photos'])) {
                        foreach ($results['photos'] as $key => $photo)
                        {
                            if(isset($photo['item']) && !empty($photo['item']) && (!isset($photo['item']['state']) || empty($photo['item']['state'])))
                            {
                                $results['photos'][$key]['item']['state'] = $this->list_state[$country_code];
                            }

                            $nick_name = isset($results['photos'][$key]['user']['name']) ? trim($results['photos'][$key]['user']['name']) : '';
                            if(empty($nick_name) && isset($results['photos'][$key]['user']['insing_user_name']))
                            {
                                $nick_name = trim($results['photos'][$key]['user']['insing_user_name']);
                            }
                            if(empty($nick_name) && isset($results['photos'][$key]['user']['facebook_name']))
                            {
                                $nick_name = trim($results['photos'][$key]['user']['facebook_name']);
                            }
                            $results['photos'][$key]['user']['name'] = $nick_name;
                        }
                    }
                    if(!$no_cache)
                   {
                        $imageCache->save($cacheConstant, $results);
                   }
                }
            } else {
                $results = $imageCache->fetch($cacheConstant);
            }
            return $results;
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
            if ($this->container->get('kernel')->getEnvironment() == 'dev') {
                echo $exc->getMessage();
            }
            return null;
        }
    }

    /**
     * get User Photos List
     * @param $itemId
     * @param $per_page
     * @param $page
     * @param $sortDesc: Sort in descending order – Y (default) or N.
     * @param moreInfo: true/false
     */
    public function getPhotosList($userId = 0, $itemId = '', $per_page = 10, $page = 1, $sortDesc = 'Y', $moreInfo = true, $country_code = '') {
        $results = array();
        try {
            $token = $this->getSessionToken();
            $imageCache = $this->container->get('hgw.cache');
            $cacheConstant = self::CACHE_KEY_IBL_REVIEW_PHOTOS . $userId . $itemId . '_perpage' . $per_page . '_page' . $page . $country_code;
            if (!$imageCache->contains($cacheConstant)) {

                if($country_code == '')
                {
                    $country_code = $this->countryCode;
                }
                $item_type = $this->listChannel[$country_code];
                $user_type = $this->listUserType[$country_code];

                $rnr_more_info = $this->container->getParameter('rnr_use_more_info');
                $use_more_info = isset($rnr_more_info[$country_code]) ? $rnr_more_info[$country_code] : false;

                $more_info_str = $moreInfo ? 'true' : 'false';
                if(!$use_more_info)
                {
                    $more_info_str = 'false';
                }

                $fields = array(
                    'session_token' => $token,
                    'item_type'     => $item_type,
                    'user_type'     => $user_type,
                    'per_page'      => $per_page,
                    'page'          => $page,
                    'sortDesc'      => $sortDesc,
                    'more_info'     => $more_info_str
                );

                if (!empty($itemId)) {
                    $fields['item_id'] = $itemId;
                }

                if (!empty($userId)) {
                    $fields['user_id'] = $userId;
                }

                $results = $this->doGetWrapper($this->rnr_url . 'photos' , $this->rnr_auth_secret,$fields);
                if(isset($results['photos']))
                {
                    foreach ($results['photos'] as $key => $photo)
                    {
                        if(isset($photo['item']) && !empty($photo['item']) && (!isset($photo['item']['state']) || empty($photo['item']['state'])))
                        {
                            $results['photos'][$key]['item']['state'] = $this->list_state[$country_code];
                        }
                    }
                }

                $imageCache->save($cacheConstant, $results);
            } else {
                $results = $imageCache->fetch($cacheConstant);
            }
            return $results;
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }
        return null;
    }

    public function getUserStatisticsApi(array $userId, $userType = '')
    {
        try {

            if($userType == '')
            {
                $userType = $this->listUserType[$this->countryCode];
            }

            $cacheKey = self::CACHE_KEY_USER_STATISTICS . md5(implode('|', $userId)) . $userType;
            $result = $this->container->get('hgw.cache')->fetch($cacheKey);

            if (empty($result)) {
                $token = $this->getSessionToken();
                $fields = array(
                    'session_token' => $token,
                    'user_id' => implode(',', $userId),
                    'user_type' => $userType
                );
                $data = $this->doGetWrapper($this->rnr_url . 'users/statistics', $this->rnr_auth_secret, $fields);
                $result = array();
                if ($data['status'] == 200) {
                    $result = $data['users'];
                    $this->container->get('hgw.cache')->save($cacheKey, $result);
                }
            }
            return $result;
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }
        return array();
    }

    /**
     *
     * @param int $userId
     * @param string $userType: 'insing' or 'moble'
     * @param string $category: – hawker, restaurant etc
     * @return array
     */
    public function getReviewsByUserId($userId, $category = null, $userType = 'insing', $per_page = 10, $page = 1, $moreInfo = true, $country_code = '')
    {
        if($country_code == '')
        {
            $country_code = $this->countryCode;
        }
        $item_type = $this->listChannel[$country_code];

        $rnr_more_info = $this->container->getParameter('rnr_use_more_info');
        $use_more_info = isset($rnr_more_info[$country_code]) ? $rnr_more_info[$country_code] : false;

        $more_info_str = $moreInfo ? 'true' : 'false';
        if(!$use_more_info)
        {
            $more_info_str = 'false';
        }

        $token = $this->getSessionToken();
        $fields = array(
            'session_token' => $token,
            'user_id'       => $userId,
            'user_type'     => $userType,
            'page'          => $page,
            'per_page'      => $per_page,
            'item_type'     => $item_type,
            'more_info'     => $more_info_str
        );

        if (!empty($category)) {
            $fields['item_category'] = $category;
        }

        $data = $this->doGetWrapper($this->rnr_url . 'reviews', $this->rnr_auth_secret, $fields);
        if(isset($data['reviews']))
        {
            foreach ($data['reviews'] as $key => $reviews)
            {
                if(isset($reviews['item']) && !empty($reviews['item']) && (!isset($reviews['item']['state']) || empty($reviews['item']['state'])))
                {
                    $data['reviews'][$key]['item']['state'] = $this->list_state[$country_code];
                }
            }
        }
        return $data;
    }

    public function postManagementResponse($reviewId, $response) {
        //review/{review_id}/response

        $token = $this->getSessionToken();
        $fields = array('session_token' => $token, 'name' => $response->getName(), 'email' => $response->getEmail(), 'contact' => $response->getPhone(), 'body' => $response->getContent());

        try {
            $data = $this->doPostJsonWrapper($this->rnr_url . 'review/' . $reviewId . '/response', $this->rnr_auth_secret, $fields);
            if ($data['status'] == 200 && isset($data['response_id']) && $data['response_id']) {
                return true;
            }
        } catch (Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }

        return false;
    }

    public function getListViolationTerms($termType = 'review') {
        //review/{review_id}/response

        $token = $this->getSessionToken();

        $fields = array('session_token' => $token);

        try {
            $data = $this->doGetWrapper($this->rnr_url . 'violation_terms/' . $termType, $this->rnr_auth_secret, $fields);
            if ($data['status'] == 200 && isset($data['violation_terms']) && count($data['violation_terms'])) {
                return $data['violation_terms'];
            }
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }

        return array();
    }

    public function postAbusesReport($reviewId, $userId, $violationTerm, $body, $userType = 'insing') {
        //review/{review_id}/response

        $token = $this->getSessionToken();

        $fields = array('session_token' => $token, 'user_id' => $userId, 'user_type' => $userType, 'review_id' => $reviewId, 'violation_term' => $violationTerm, 'body' => $body);

        try {
            $data = $this->doPostJsonWrapper($this->rnr_url . 'abuses', $this->rnr_auth_secret, $fields);
            if ($data['status'] == 200 && isset($data['abuse_id']) && $data['abuse_id']) {
                return true;
            }
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }
        return false;
    }

    public function postHelpfulReview($reviewId, $userId, $userType = 'insing') {
        //review/{review_id}/response

        $token = $this->getSessionToken();

        $fields = array('session_token' => $token, 'user_id' => $userId, 'user_type' => $userType);
        $data = null;
        try {
            $data = $this->doPostJsonWrapper($this->rnr_url . 'review/' . $reviewId . '/helpful', $this->rnr_auth_secret, $fields);

        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }

        return $data;
    }

    public function postLikePhoto($photoId, $userId, $userType = 'insing') {
        $token = $this->getSessionToken();

        $fields = array('session_token' => $token, 'user_id' => $userId, 'user_type' => $userType);

        try {
            $data = $this->doPostJsonWrapper($this->rnr_url . 'photo/' . $photoId . '/like', $this->rnr_auth_secret, $fields);
            if ($data['status'] == 200 && isset($data['photo_id']) && $data['photo_id']) {
                return true;
            }
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
        }

        return false;
    }

    private function getCallingString($url, $secret, $fields, $is_return_array = true)
    {
        // Generate API Signature
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);
        $pathinfo = $secret . $pathinfo;

        $fields['sig'] = $this->genSignature($fields, $pathinfo);

        if (!$is_return_array) {
            // form up variables in the correct format for HTTP POST
            $fields_string = '';
            foreach ($fields as $key => $value) {
                $fields_string .= $key . '=' . urlencode($value) . '&';
            }

            $fields_string = rtrim($fields_string, '&');

            return $fields_string;
        }

        return $fields;
    }

    private function genSignature($params, $pathinfo)
    {
        $result = $pathinfo;

        ksort($params);
        foreach ($params as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $result .= urlencode($k . $v);
                }
            } else {
                $result .= urlencode($key . $value);
            }
        }

        return md5($result);
    }

    public function doGetWrapper($url, $secret, $fields)
    {
        $curl = new Curl($this->container);
        $curl->setMethod(Curl::HTTP_GET);
        $curl->setChannel('monolog.logger.rnrapi');
        $fields = $this->getCallingString($url, $secret, $fields);
        $res = $curl->call($url, $fields);

        if ($res['status'] == Curl::SESSION_TOKEN_TIMEOUT || $res['status'] == Curl::HTTP_UNAUTHORIZED) {
            $sessionTokenCache = $this->container->get('hgw.cache');
            $sessionTokenCache->delete(self::RNR_API_SESSION_TOKEN_CACHE_KEY . $this->rnr_auth_id);
            $sessionToken = $this->getSessionToken();
            $fields['session_token'] = $sessionToken;

            $fields = $this->getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
        } elseif ($res['status'] != RestfulAPIHelper::HTTP_OK) {
            if (is_array($res)) {
                $this->container->get('monolog.logger.rnrapi')->error($url, $res);
            } else {
                $this->container->get('monolog.logger.rnrapi')->error('Result NULL: ' . $url, $fields);
            }
        }

        return $res;
    }

    protected function doPostWrapper($url, $secret, $fields)
    {
        $curl = new Curl($this->container);
        $curl->setMethod(Curl::HTTP_POST);
        $curl->setChannel('monolog.logger.rnrapi');

        $fields = $this->getCallingString($url, $secret, $fields);
        $res = $curl->call($url, $fields);

        if ($res['status'] == RestfulAPIHelper::SESSION_TOKEN_TIMEOUT || $res['status'] == RestfulAPIHelper::HTTP_UNAUTHORIZED) {
            $sessionTokenCache = $this->container->get('hgw.cache');
            $sessionTokenCache->delete(self::RNR_API_SESSION_TOKEN_CACHE_KEY . $this->rnr_auth_id);
            $sessionToken = $this->getSessionToken();
            $fields['session_token'] = $sessionToken;

            $fields = $this->getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
        } elseif ($res['status'] != RestfulAPIHelper::HTTP_OK) {
            if (is_array($res)) {
                $this->container->get('monolog.logger.rnrapi')->error($url, $res);
            } else {
                $this->container->get('monolog.logger.rnrapi')->error('Result NULL: ' . $url, $fields);
            }
        }

        return $res;
    }

    protected function doPostJsonWrapper($url, $secret, $fields) {

        $curl = new Curl($this->container);
        $curl->setChannel('monolog.logger.rnrapi');
        $curl->setMethod(Curl::HTTP_POST);
        $curl->setPostJson(true);
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);

        $url_call = $url . '?sig=' . $this->genSignatureJson($secret, $fields, $pathinfo);
        $this->container->get('monolog.logger.rnrapi')->info($url_call, $fields);

        $res = $curl->call($url_call, $fields);

        if ($res['status'] == Curl::SESSION_TOKEN_TIMEOUT || $res['status'] == Curl::HTTP_UNAUTHORIZED) {
            $sessionTokenCache = $this->container->get('hgw.cache');
            $sessionTokenCache->delete(self::RNR_API_SESSION_TOKEN_CACHE_KEY . $this->rnr_auth_id);
            $fields['session_token'] = $this->getSessionToken();
            $url_call = $url . '?sig=' . $this->genSignatureJson($secret, $fields, $pathinfo);
            $this->container->get('monolog.logger.rnrapi')->info($url_call, $fields);
            $res = $curl->call($url_call, $fields);
        } else if ($res['status'] != Curl::HTTP_OK) {
            if (is_array($res)) {
                $this->container->get('monolog.logger.rnrapi')->error($url, $res);
            } else {
                $this->container->get('monolog.logger.rnrapi')->error('Result NULL: ' . $url, $fields);
            }
        }
        return $res;
    }

    private function genSignatureJson($secret, $params, $pathinfo)
    {
        $params = json_encode($params);
        $params = str_replace("\n", '', $params);
        $params = str_replace("\r", '', $params);

        $pathinfo = $secret . $pathinfo;

        return md5( $pathinfo . $params );
    }

    public function getReviewDetailApi($reviewId, $more_info = false) {
        try {
            $session_token = $this->getSessionToken();
            if (is_null($session_token))
            {
                return array();
            }
            $result = $this->doGetWrapper(
                    $this->rnr_url . "review/$reviewId",
                    $this->rnr_auth_secret,
                    array(
                            'session_token' => $session_token,
                            'more_info' => $more_info ? 'true' : 'false'
                    ));
            if(isset($result['review']['item']) && !empty($result['review']['item']) && (!isset($result['review']['item']['state']) || empty($result['review']['item']['state'])))
            {
                $items_type = $this->container->getParameter('rnr_channel');

                $state = $this->list_state[$this->countryCode];

                foreach ($items_type as $country_code => $item_type)
                {
                    if ($item_type == $result['review']['item_type'])
                    {
                        $state = $this->list_state[$country_code];
                        break;
                    }
                }
                $result['review']['item']['state'] = $state;
            }
        } catch (\Exception $exc) {
            $this->container->get('monolog.logger.rnrapi')->error($exc->getCode() . ':' . $exc->getMessage());
            return array();
        }

        return $result;
    }
}