<?php

namespace inSing\DataSourceBundle\Utilities;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BusinessDB {

    /**
     *
     * @var ContainerInterface
     */
    public $container;

    /**
     *
     * @var MySQLConn
     */
    protected $db_conn = null;

    protected $country_code = '';

    protected $logger = null;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->logger = new HgwLogger($this->container, 'business_db_info');
    }

    public function __destruct(){
        $this->db_conn->close();
    }

    public function initDB($country_code)
    {
        $this->country_code = $country_code;
        $db_info = $this->container->getParameter('business_db_info');

        $this->db_conn = new MySQLConn($db_info['host'], $db_info['name'], $db_info['user'], $db_info['password'], $db_info['port']);

        return $this;
    }
    public function getListBusinesses($category, $page, $per_page)
    {
        try {

            $limit = ($page - 1) * $per_page;

            $query = "SELECT DISTINCT b.business_id AS id, b.hgw_url AS hgw_url, sta.state AS state
                        FROM businesses b
                        INNER JOIN business_multiple_category bmc ON b.business_id = bmc.business_id
                        INNER JOIN business_category AS bc ON bc.category_id = bmc.category_id AND bc.path LIKE '{$category}%'
                        INNER JOIN states AS sta ON sta.id = b.state_id
                        WHERE b.primary_status = 'Active' AND b.secondary_status != 'Closed'
                        LIMIT {$limit}, {$per_page}";

            // For SG, don't have the states table
            if($this->country_code == Constant::COUNTRY_CODE_SINGAPORE)
            {
                $query = "SELECT DISTINCT b.business_id AS id, b.hgw_url AS hgw_url, '' AS state
                            FROM businesses b
                            INNER JOIN business_multiple_category bmc ON b.business_id = bmc.business_id
                            INNER JOIN business_category AS bc ON bc.category_id = bmc.category_id AND bc.path LIKE '{$category}%'
                            WHERE b.primary_status = 'Active' AND b.secondary_status != 'Closed'
                            LIMIT {$limit}, {$per_page}";
            }



            $businesses = $this->db_conn->executeResultQuery($query, \PDO::FETCH_ASSOC);
            return $businesses;
        } catch (\Exception $exc) {
            $this->logger->error("Query: {$query}");
            $this->logger->exp_err($exc);
        }
        return array();
    }

    public function getTotalBusiness($category) {
        try{
            $query = "SELECT COUNT(DISTINCT b.business_id) AS total
                        FROM businesses b
                        INNER JOIN business_multiple_category bmc ON b.business_id = bmc.business_id
                        INNER JOIN business_category AS bc ON bc.category_id = bmc.category_id AND bc.path LIKE '{$category}%'
                        WHERE b.primary_status = 'Active' AND b.secondary_status != 'Closed'";
            $total = $this->db_conn->fetchQuery($query, \PDO::FETCH_COLUMN);
            return $total;
        } catch (\Exception $exc) {
            $this->logger->error("Query: {$query}");
            $this->logger->exp_err($exc);
        }
        return 0;
    }

    public function getTotalMediasByItemIds(array $business_ids)
    {
        if(!empty($business_ids))
        {
            $media_types = array(BusinessListingApi::MEDIA_TYPE_DISH, BusinessListingApi::MEDIA_TYPE_GALLERY);
            $media_types = implode(",", $media_types);

            $business_ids =  implode(",", $business_ids);
            try {
                $query = "SELECT COUNT(DISTINCT bm.business_id)
                        FROM business_media bm
                        INNER JOIN businesses b ON bm.business_id = b.business_id AND b.business_id IN ({$business_ids})
                        WHERE bm.media_type_id IN ({$media_types}) AND bm.status = 1";
                $total = $this->db_conn->fetchQuery($query, \PDO::FETCH_COLUMN);
                return $total;
            } catch (\Exception $exc) {
                $this->logger->error("Query: {$query}");
                $this->logger->exp_err($exc);
            }
        }
        return 0;
    }

    public function getListMediasByItemIds(array $business_ids, $page, $per_page)
    {
        if(!empty($business_ids))
        {
            $limit = ($page - 1) * $per_page;

            $media_types = array(BusinessListingApi::MEDIA_TYPE_DISH, BusinessListingApi::MEDIA_TYPE_GALLERY);
            $media_types = implode(",", $media_types);

            $business_ids = implode(",", $business_ids);
            try {
                $query = "SELECT DISTINCT bm.business_id AS business_id, bm.media_id AS media_id
                            FROM business_media bm
                            INNER JOIN businesses b ON bm.business_id = b.business_id AND b.business_id IN({$business_ids})
                            WHERE bm.media_type_id IN({$media_types}) AND bm.status = 1 LIMIT {$limit}, {$per_page}";

                $photos = $this->db_conn->executeResultQuery($query, \PDO::FETCH_ASSOC);
                return $photos;
            } catch (\Exception $exc) {
                $this->logger->error("Query: {$query}");
                $this->logger->exp_err($exc);
            }
        }
        return array();
    }
}