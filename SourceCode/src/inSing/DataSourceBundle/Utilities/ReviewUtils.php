<?php

/**
 * Review Utils for review module
 *
 * @author  Vu Tran
 */

namespace inSing\DataSourceBundle\Utilities;

//use inSing\DataSourceBundle\Utilities\FacebookSDK\Facebook;
use Facebook;
use FacebookApiException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use inSing\DataSourceBundle\Utilities\Common;
use inSing\DataSourceBundle\Utilities\RnRApiUtils;

class ReviewUtils {

    const REVIEW_PHOTO = 'REVIEW_PHOTO';
    const REVIEW_PHOTO_STATUS_NEW = 'NEW';
    const REVIEW_PHOTO_STATUS_LIVE = 'LIVE';
    const REVIEW_PHOTO_STATUS_DELETED = 'DELETED';

    const SUBMISSION_TYPE_REVIEW = 10;
    const SUBMISSION_TYPE_FAVOURITE = 11;
    const SUBMISSION_TYPE_RECOMENDATION = 12;
    const SUBMISSION_TYPE_ALL = 13;
    const SUBMISSION_TYPE_PHOTOS = 14;

    /**
     * Copy Review Photo Into SERVER ( Amazon S3 Server )
     *
     * @author Vu Tran
     * @Customize LapTo
     * @param $container
     * @param $uploadToken
     */
    public static function storeReviewPhotoIntoServer($container, $review_photos) {
        $result = true;
        $key_files = array();

        //Copy each photo into Server
        if (is_array($review_photos)) {
            $aws_id = $container->getParameter('aws_s3_id');
            $aws_secret = $container->getParameter('aws_s3_secret');
            $dir_source = $container->getParameter('aws_s3_dir_name_rnr_temp');
            $bucket = $container->getParameter('aws_s3_bucket_name_rnr');

            $s3_client = new InSingAwsS3($aws_id, $aws_secret);
            foreach ($review_photos as $photo) {

                //Current File path
                $key_file = isset($photo['path']) ? $photo['path'] : null;

                if (ReviewUtils::REVIEW_PHOTO_STATUS_NEW == $photo['status'] && $key_file) {
                    // Copy file from bucket temp to bucket rnr
                    $copy_result = $s3_client->copy($bucket, $bucket, $key_file, str_replace($dir_source, '', $key_file), true); // Remove temp folder on key_file destination
                }
                if ($key_file) {
                    $key_files[] = $key_file;
                }
            }

            // Delete file on bucket temp
            $delete_result = count($key_files) ? $s3_client->deleteObjects($bucket, $key_files) : false;
        }

        return $result;
    }

    /**
     * Create photo array object from Uploaded Photo
     *
     * @author  Vu Tran
     * @param $container
     * @param UploadedFile $photo
     * @param $upload_token
     */
    public static function createPhotoObject($container, UploadedFile $photo, $upload_token, $item_id, $user_id, $photo_status, $countryCode) {
        try {
            //Generate Save Folder
            //$save_folder = substr(strrev(bin2hex($photo_name)), 0, 4);
            $channel = $container->getParameter('rnr_channel');
            $channel = $channel[$countryCode];
            $save_folder = "$channel/$item_id/$user_id"; // <biz_id>/<user_id>
            // Get Resource file uploaded
            $resource = fopen($photo->getPathname(), 'r+');

            $mime_type = Common::getMimeType($photo->getClientOriginalName(), $resource);
            $photo_extension = ReviewUtils::getFileExtensionFromMimeType($mime_type);

            if (!$photo_extension) {
                throw new \Exception('Photo - We dont suppport this image type! - ReviewUtils.php, line: ' . __LINE__);
            }

            $aws_id = $container->getParameter('aws_s3_id');
            $aws_secret = $container->getParameter('aws_s3_secret');
            $dir_source = $container->getParameter('aws_s3_dir_name_rnr_temp');
            $bucket = $container->getParameter('aws_s3_bucket_name_rnr');

            $client = new InSingAwsS3($aws_id, $aws_secret);
            $md5Raw = md5(stream_get_contents($resource), true);
            $md5Hex = bin2hex($md5Raw); // Used as part of file storage name, if this one is file name, we can check duplicate file what is uploaded by user
            $md5Base64 = base64_encode($md5Raw); // Amazon requires this format, using for MD5 checksum

            $file_name = $md5Hex . '_' . time(); // Accept file duplicate
            // Key file sample: hgwweb_temp/food/item_id/user_id/file_name.jpg, hgwweb_temp is temp folder
            $key_file = $dir_source . join('/', array($save_folder, $file_name . '.' . $photo_extension));

            // Upload to server and wait to upload successfully
            $guzzleObject = $client->putResource($bucket, $key_file, $resource, true, true);

            // Check upload successfully
            if (!$guzzleObject) {
		throw new \Exception('Photo - Cannot upload to server successfully - ReviewUtils.php line: ' . __LINE__);
            }

            $photo_size = getimagesize($photo->getPathname());

            //Create Object for this Photo & store it into SESSION
            $photo_arr = array();
            $photo_arr['upload_token'] = $upload_token;
            $photo_arr['path'] = $key_file;
            $photo_arr['originalname'] = $photo->getClientOriginalName();
            $photo_arr['filename'] = $file_name . '.' . $photo_extension;
            $photo_arr['basename'] = $file_name;
            $photo_arr['extension'] = $photo_extension;
            $photo_arr['size'] = $photo->getClientSize();
            $photo_arr['mimetype'] = $photo->getClientMimeType();
            $photo_arr['status'] = $photo_status;

            //Store width & height of the Image uploaded
            if (!is_null($photo_size)) {
                $photo_arr['width'] = $photo_size[0];
                $photo_arr['height'] = $photo_size[1];
            }
        } catch (\Exception $exc) {
            throw new \Exception('Photo - ' . $exc->getMessage());
	}
        return $photo_arr;
    }

    /**
     * Publish Submission into Facebook
     *
     * Function will be SHARE
     * + IBL url when NOT have Review Information
     * + Review url when have Review Information
     *
     * @author Vu Tran
     * @param $container
     * @param $reviewSubmitData
     * @param $submission_id
     */
    public static function publishReviewToFacebook($caller, $container, $reviewSubmitData, $submission_id, $restaurant, $restaurant_slug, $use_hash_id = false) {
        /* If has permission -> Post review to user's facebook */
        try {
            // Create our Application instance (replace this with your appId and secret).
            $facebook = new Facebook(array(
                'appId' => $container->get('common.utils')->getFacebookAcount('app_id'),
                'secret' => $container->get('common.utils')->getFacebookAcount('app_serect'),
                'cookie' => true,
            ));

            if ($facebook->getUser()) {
                // Check permission "publish_actions"
                $permissions = $facebook->api(array(
                    "method"    => "users.hasAppPermission",
                    "ext_perm"   => "publish_actions",
                    "uid"       => $facebook->getUser()
                    ));
                //Can use this way to get specific permission
                //$permissions = $facebook->api("/" . $facebook->getUser() . '/permissions/publish_actions');
                //$permissions = $facebook->api("/me/permissions");
                $caller->get('monolog.logger.review')->info(json_encode($permissions));
                $caller->get('monolog.logger.review')->info('isGrant: ' . $permissions);
                if ($permissions == 1) {
                    if ($use_hash_id) {
                        $submission_id = Common::urlIdToId($submission_id);
                    }
                    $facebook_message = $share_facebook_url = $entity_name = '';

                    if (!empty($reviewSubmitData)) {
                        #var_dump($reviewSubmitData);
                        //Create url for sharing facebook
                        if (isset($reviewSubmitData['review']) && !empty($reviewSubmitData['review'])) {

                            //Get Review URL by review_id ( <- NOT submission_id, that is id of review[title, body] )
                            $review_url = $caller->generateUrl('in_sing_hgwr_ibl_reviews_detail_page', array(
                                'slug' => $restaurant_slug,
                                'state' => $restaurant['state'],
                                'hash_id' => Common::idToUrlId($submission_id)
                                    ), true);

                            //Add share facebook url
                            $share_facebook_url = $review_url;
                        } else {
                            //Get Restaurant URL
                            $restaurant_url = $caller->generateUrl('in_sing_hgwr_ibl', array(
                                'state' => $restaurant['state'],
                                'slug' => $restaurant_slug
                                    ), true);

                            //Add share facebook url
                            $share_facebook_url = $restaurant_url;
                        }

                        $entity_name = $restaurant['name'];

                        if (isset($reviewSubmitData['recommendations']) && count($reviewSubmitData['recommendations']) > 0) {
                            //Create recommendations string
                            $review_recommendations_str = implode(', ', $reviewSubmitData['recommendations']);

                            //I recommend <recommended for> at <entity name>, if not
                            $facebook_message = "I recommend {$review_recommendations_str} at {$entity_name}";
                        } else if (isset($reviewSubmitData['suitable_for']) && count($reviewSubmitData['suitable_for']) > 0) {
                            //Create suitable for
                            $review_suitable_for = implode(', ', $reviewSubmitData['suitable_for']);

                            //I recommend <entity name> for <suitable for>, if not
                            $facebook_message = "I recommend {$entity_name} for {$review_suitable_for}";
                        } elseif (isset($reviewSubmitData['vote'])) {
                            if ($reviewSubmitData['vote']) {
                                //Thumb Up - I recommend <entity name>
                                $facebook_message = "I recommend {$entity_name}";
                            } else {
                                //Thumb Down - I don't recommend <entity name>
                                $facebook_message = "I don't recommend {$entity_name}";
                            }
                        }
                    } else {
                        //Get Review Information from review_id
                        $review_info = $container->get('rnr.api')->getSubmissionDetails($submission_id, true);
                        #var_dump($review_info);
                        $entity_name = isset($review_info['item']['name']) ? $review_info['item']['name'] : '';

                        if (isset($review_info['recommendations']) && count($review_info['recommendations']) > 0) {
                            //Create recommendations string
                            $review_recommendations_str = $prefix = '';
                            foreach ($review_info['recommendations'] as $recommendation) {
                                $review_recommendations_str .= $review_recommendations_str . $prefix . $recommendation;
                                $prefix = ', ';
                            }

                            //I recommend <recommended for> at <entity name>, if not
                            $facebook_message = "I recommend {$review_recommendations_str} at {$entity_name}";
                        } elseif (isset($review_info['suitable_for']) && count($review_info['suitable_for']) > 0) {
                            //Create suitable for
                            $review_suitable_for = implode(', ', $review_info['suitable_for']);

                            //I recommend <entity name> for <suitable for>, if not
                            $facebook_message = "I recommend {$entity_name} for {$review_suitable_for}";
                        } elseif (isset($review_info['vote']['thumbs_up'])) {
                            if ($review_info['vote']['thumbs_up']) {
                                //Thumb Up - I recommend <entity name>
                                $facebook_message = "I recommend {$entity_name}";
                            } else {
                                //Thumb Down - I don't recommend <entity name>
                                $facebook_message = "I don't recommend {$entity_name}";
                            }
                        }
                        /*
                          //Get Restaurant URL
                          $restaurant_url = $caller->generateUrl('HgwBundle_IBL', array(
                          'slug'		=> $restaurant_slug
                          ), true);

                          //Add share facebook url
                          $share_facebook_url = $restaurant_url;
                         */
                        $review_url = $caller->generateUrl('in_sing_hgwr_ibl_reviews_detail_page', array(
                            'state' => $restaurant['state'],
                            'slug' => $restaurant_slug,
                            'hash_id' => Common::idToUrlId($submission_id)
                                ), true);

                        //Add share facebook url
                        $share_facebook_url = $review_url;
                    }

                    //Message format
                    //I recommend <recommended for> at <entity name>, if not
                    //I recommend <entity name> for <suitable for>, if not
                    //I <vote> <entity name>
                    if ($facebook_message && $share_facebook_url) {
                        $picture = $caller->getRequest()
                                ->getUriForPath('/bundles/insinghgwr/images/default_300x224.jpg');
                        if (!empty($reviewSubmitData['photos'])) {
                            $picture = $caller->get('common.utils')
                                    ->generatePhotoFromUrl($reviewSubmitData['photos'][0]['url'], '600x600');
                        } else if ($restaurant['no_image'] == false) {
                            //$review['og_review_photo'] = $restaurant['image_url'];
                            $picture = $restaurant['image_url'];
                        } else {
                            $arrUserPhotos = $container->get('rnr.api')->getUserPhotosByRestaurantId($restaurant['id'], 1);
                            if (!empty($arrUserPhotos) && $arrUserPhotos['total'] > 0) {
                                $picture = $caller->get('common.utils')
                                        ->generatePhotoFromUrl($arrUserPhotos['photos'][0]['url'], '600x600');
                            }
                        }
                        try {
                            //Post to Facebook
                            $publish_action = $container->get('common.utils')->getFacebookAcount('publish_action');
                            if (!empty($publish_action)) {
                                $result = $facebook->api('/me/' . $publish_action, 'post', array(
                                    'message' => $facebook_message,
                                    'review' => $share_facebook_url,
                                    'image' => $picture
                                ));
                            } else {
                                $description = '';
                                $title = '';
                                if (isset($reviewSubmitData['review']) && !empty($reviewSubmitData['review'])) {
                                    $description = Common::textTruncate(strip_tags($reviewSubmitData['review']['body']), 140);
                                    $title = $reviewSubmitData['review']['title'];
                                }
                                // RePublish to fb if error
                                $result = $facebook->api('/me/feed', 'post', array(
                                    'message' => $facebook_message,
                                    'link' => $share_facebook_url,
                                    'picture' => $picture,
                                    'name' => $title,
                                    'description' => $description
                                ));
                            }
                                                        
                        } catch (\Exception $e) {
                            $description = '';
                            $title = '';
                            if (isset($reviewSubmitData['review']) && !empty($reviewSubmitData['review'])) {
                                $description = Common::textTruncate(strip_tags($reviewSubmitData['review']['body']), 140);
                                $title = $reviewSubmitData['review']['title'];
                            }
                            // RePublish to fb if error
                            $result = $facebook->api('/me/feed', 'post', array(
                                'message' => $facebook_message,
                                'link' => $share_facebook_url,
                                'picture' => $picture,
                                'name' => $title,
                                'description' => $description
                            ));

                            $caller->get('monolog.logger.review')->err($e->getMessage());
                        }
                    }
                    return array(
                        'status' => 'success',
                    );
                }
            }
        } catch (\Exception $e) {
            $caller->get('monolog.logger.review')->err($e->getMessage());
        }

        /* If has no permission -> Request permission */
        /*
          try {
          $sPermission = 'publish_actions';
          $loginUrl = $facebook->getLoginUrl(array(
          'display' => 'popup',
          'scope' => $sPermission,
          'redirect_uri' => $caller->generateUrl('HgwBundle_review_create_post_to_facebook', array(
          'restaurant_slug'	=> $restaurant_slug,
          'hash_id'			=> Common::idToUrlId($submission_id),
          ), true)
          ));

          return array(
          'status' => 'request_permissions',
          'request_pers_uri' => $loginUrl,
          );
          } catch(\Exception $e) {

          }
         */
        /* Facebook API error */
        return array(
            'status' => 'error',
        );
    }



    /**
     * Get File Extension from MimeType
     *
     * @author  Vu Tran
     * @param string $mime_type
     */
    public static function getFileExtensionFromMimeType($mime_type) {
        //Create Extension Array
        $known_extension = array(
            "image/png" => "png",
            "image/jpg" => "jpg",
            "image/jpeg" => "jpg",
            "image/pjpeg" => "jpg"
        );

        return isset($known_extension[$mime_type]) ? $known_extension[$mime_type] : null;
    }
}
