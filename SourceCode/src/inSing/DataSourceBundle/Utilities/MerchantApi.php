<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\DataSourceBundle\Utilities;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of MerchantApi
 *
 * @author VuTranQ
 */
class MerchantApi
{
    /**
     *
     * @var Container
     */
    protected $container;
    
    /**
     *
     * @var Cache
     */
    protected $cache;
    
    /**
     *
     * @var Logger
     */
    protected $logger;
    
    /**
     * @var Cache
     */
    const MERCHANT_SESSION_TOKEN_CACHE_KEY = 'MERCHANT_SESSION_TOKEN_CACHE_KEY';
    
    /**
     *
     * @var countryCode
     */
    protected $countryCode;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->cache = $this->container->get('hgw.cache');
        $this->logger = new HgwLogger($this->container, 'merchant_api');
        $this->countryCode = $this->container->getParameter('country_code');
    }
    
    protected function merchantLog($exc)
    {
        $this->container->get('monolog.logger.merchantapi')->error($exc->getCode() . ' ' . $exc->getMessage());
    }

    protected function genSignature($url, $params) 
    {
        $consumerSecret = $this->container->getParameter('merchant_consumer_secret');
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);
        $pathinfo = $consumerSecret . $pathinfo;
        $result = $pathinfo;
        //ksort($params, SORT_NATURAL);
        uksort($params, 'strnatcmp');
        $result = $pathinfo . http_build_query($params, '', '');
        $result = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $result);
        $result = str_replace('=', '', $result);
        return md5($result);
    }
    
    /**
     * Get Calling String
     *
     * @param $url
     * @param $fields
     * @param bool $isReturnArray
     * @return string
     */
//    protected function getCallingString($url, $fields, $isReturnArray = true)
//    {
//        $fields['sig'] = $this->genSignature($url, $fields);
//        if (!$isReturnArray) {
//            // form up variables in the correct format for HTTP POST
//            $fieldsString = '';
//            foreach ($fields as $key => $value) {
//                $fieldsString .= $key . '=' . urlencode($value) . '&';
//            }
//            $fieldsString = rtrim($fieldsString, '&');
//            
//            return $fieldsString;
//        }
//        
//        return $fields;
//    }
    
    /**
     * Get session token
     * @return string sessionToken
     */
    public function getSessionToken() {
        try {
            $merchantSessionTokenCacheKey = self::MERCHANT_SESSION_TOKEN_CACHE_KEY;
            $sessionToken = $this->container->get('hgw.cache')->fetch($merchantSessionTokenCacheKey);

            if (empty($sessionToken)) {
                $result = $this->authenticate();
                $sessionToken = $result['session_token'];
                
                //calculate the period time to cache
                $cacheTime = 180;

                //cache data on $cacheTime
                $this->container->get('hgw.cache')->save($merchantSessionTokenCacheKey, $sessionToken, $cacheTime);
            }
            return $sessionToken;
        } catch (\Exception $exc) {
            $this->logger->error($exc->getMessage());
        }

        return null;
    }
    
    public function authenticate()
    {
        try {
            $result_authenticate = $this->doPostWrapper(
                $this->container->getParameter('merchant_url_auth'),
                array(
                    'access_token' => $this->container->getParameter('merchant_access_token'),
                    'access_token_secret' => $this->container->getParameter('merchant_token_secret'),
                    'consumer_key' => $this->container->getParameter('merchant_consumer_key'),
                    'consumer_secret' => $this->container->getParameter('merchant_consumer_secret')
                )
            );
            
            if (Curl::HTTP_OK != $result_authenticate['status']) {
                return null;
            }
            
            return array('session_token' => $result_authenticate['data']['session_token']);
        } catch (\Exception $exc) {
            $this->logger->error($exc->getMessage());
        }
    }
    
    public function doGetWrapper($url, $params)
    {
        $params['sig'] = $this->genSignature($url, $params);

        $result = $this->container->get('curl')
                ->setMethod(Curl::HTTP_GET)
                ->call($url, $params);

        if ($result['status'] == Curl::SESSION_TOKEN_TIMEOUT || $result['status'] == Curl::HTTP_UNAUTHORIZED) {
            $this->container->delete(self::MERCHANT_SESSION_TOKEN_CACHE_KEY);
            $sessionToken = $this->getSessionToken();
            $params['session_token'] = $sessionToken;
            $result = $this->container->get('curl')->setMethod(Curl::HTTP_GET)->call($url, $params);
        } else if ($result['status'] != Curl::HTTP_OK) {
            if (is_array($result)) {
                $this->logger->error($url, $result);
            } else {
                $this->logger->error('Result NULL: ' . $url, $params);
            }
        }

        return $result;
    }

    protected function doPostWrapper($url, $params)
    {
        $params['sig'] = $this->genSignature($url, $params);
        
        $result = $this->container->get('curl')
                ->setMethod(Curl::HTTP_POST)
                ->call($url, $params);
        
        if ($result['status'] == Curl::SESSION_TOKEN_TIMEOUT || $result['status'] == Curl::HTTP_UNAUTHORIZED) {
            $this->container->delete(Curl::MERCHANT_SESSION_TOKEN_CACHE_KEY);
            $sessionToken = $this->getSessionToken();
            $params['session_token'] = $sessionToken;
            $result = $this->container->get('curl')->setMethod(Curl::HTTP_POST)->call($url, $params);
        } else if ($result['status'] != Curl::HTTP_OK) {
            if (is_array($result)) {
                $this->logger->error($url, $result);
            } else {
                $this->logger->error('Result NULL: ' . $url, $params);
            }
        }

        return $result;
    }
    
    /**
     * get List merchant user
     * @param type $filter
     * @param type $page_number
     * @param type $page_size
     * @return array()
     */
    public function getListMerchantUser ($filter = null, $page_number = 1, $page_size = 20) 
    {
        $ret = array();
        
        try {
            $params = array(
                'session_token' => $this->getSessionToken(),
                'page' => $page_number,
                'per_page' => $page_size
            );
            
            if (!empty($filter)) {
                $params = array_merge($params, $filter);
            }
            
            $list_merchant_user = $this->doGetWrapper($this->container->getParameter('merchant_url_list_user'), $params);
            if (Curl::HTTP_OK == $list_merchant_user['status'] && !empty($list_merchant_user['data'])) {
                $ret = $list_merchant_user;
            }
            
            return $ret;
            
        } catch (\Exception $exc) {
            $this->logger->error($exc->getMessage());
        }
    }
    
    /**
     * Check Owner user
     * @param type $user_id
     * @return array()
     */
    public function checkOwneruser ($user_id)
    {
        $ret = array();
        if (!empty($user_id)) {
            $params = array(
                'filter_user_ids' => $user_id,
                'filter_statuses' => '1',
                'use_cache' => 'N'
            );
            $ret = $this->getListMerchantUser($params);
        }
        
        return $ret;
    }
    
    public function getMerchantSession ($params)
    {
        try {
            $ret = array();
            if (!empty($params) && is_array($params)) {                
                $params['session_token'] = $this->getSessionToken();
                $login_token = $this->doPostWrapper($this->container->getParameter('merchant_url_create_login_token'),$params);
                if (Curl::HTTP_OK == $login_token['status'] && !empty($login_token['data'])) {
                    $ret = $login_token;
                }
            }
            
            return $ret;
        } catch (\Exception $exc) {
            $this->logger->error($exc->getMessage());
        }
    }
    
}
