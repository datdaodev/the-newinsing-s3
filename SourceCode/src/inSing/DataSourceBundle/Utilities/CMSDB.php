<?php

namespace inSing\DataSourceBundle\Utilities;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CMSDB {

    /**
     *
     * @var ContainerInterface
     */
    public $container;

    /**
     *
     * @var MySQLConn
     */
    protected $db_conn = null;

    protected $logger = null;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->logger = new HgwLogger($this->container, 'cms_db_info');
    }

    public function __destruct(){
        $this->db_conn->close();
    }

    public function initDB()
    {
        $db_info = $this->container->getParameter('cms_db_info');

        $this->db_conn = new MySQLConn($db_info['host'], $db_info['name'], $db_info['user'], $db_info['password'], $db_info['port']);

        return $this;
    }
    public function getListArticles($channel, $page, $per_page)
    {
        try {

            $limit = ($page - 1) * $per_page;

            $query = "SELECT DISTINCT c.id,
                                    c.title,
                                    (select cs.seo_url FROM content_series cs JOIN content_series_value cs_value ON cs_value.series_id = cs.id WHERE cs_value.content_id=c.id ORDER BY cs_value.id LIMIT 1) as series
                        FROM
                          content c
                          JOIN content_channel cc ON c.channel_id = cc.id
                            AND (
                              LOWER(cc.name) = LOWER('{$channel}')
                            )
                        WHERE c.type = 0
                          AND (
                            c.start_date <= NOW()
                            OR c.start_date IS NULL
                          )
                          AND (
                            c.expire_date >= NOW()
                            OR c.expire_date IS NULL
                          )
                          AND c.obsolete = 0
                          AND c.status=1
                        GROUP BY c.id
                        ORDER BY c.publish_date DESC
                        LIMIT {$limit}, {$per_page}";

            $articles = $this->db_conn->executeResultQuery($query, \PDO::FETCH_ASSOC);
            return $articles;
        } catch (\Exception $exc) {
            $this->logger->error("Query: {$query}");
            $this->logger->exp_err($exc);
        }
        return array();
    }

    public function getTotalArticles($channel) {
        try{
            $query = "SELECT COUNT(DISTINCT c.id)  as total
                        FROM
                          content c
                          JOIN content_channel cc ON c.channel_id = cc.id AND (
                              LOWER(cc.name) = LOWER('{$channel}')
                            )
                        WHERE c.type = 0
                          AND (
                            c.start_date <= NOW()
                            OR c.start_date IS NULL
                          )
                          AND (
                            c.expire_date >= NOW()
                            OR c.expire_date IS NULL
                          )
                          AND c.obsolete = 0
                          AND c.status=1
                        ORDER BY c.publish_date DESC";
            $total = $this->db_conn->fetchQuery($query, \PDO::FETCH_COLUMN);
            return $total;
        } catch (\Exception $exc) {
            $this->logger->error("Query: {$query}");
            $this->logger->exp_err($exc);
        }
        return 0;
    }
    public function getListGalleries($channel, $gallery_type, $page, $per_page)
    {
        try {

            $limit = ($page - 1) * $per_page;

            $query = "SELECT c.id,
                            c.title,
                            (select cs.seo_url FROM content_series cs JOIN content_series_value cs_value ON cs_value.series_id = cs.id WHERE cs_value.content_id=c.id ORDER BY cs_value.id LIMIT 1) as series,
                            COUNT(gi.gallery_id) as total
                      FROM
                        content c
                        JOIN content_channel cc ON c.channel_id = cc.id
                          AND (
                            LOWER(cc.name) = LOWER('{$channel}')
                          )
                        JOIN gallery_image gi ON c.id = gi.gallery_id
                      WHERE c.type = 1 AND c.gallery_type = {$gallery_type}
                        AND (
                          c.start_date <= NOW()
                          OR c.start_date is null
                        )
                        AND (
                          c.expire_date >= NOW()
                          OR c.expire_date is null
                        )
                        AND c.obsolete = 0
                        AND c.status = 1
                        GROUP BY c.id, gi.gallery_id
                        HAVING total >= 3
                      ORDER BY c.publish_date DESC
                        LIMIT {$limit}, {$per_page}";

            $galleries = $this->db_conn->executeResultQuery($query, \PDO::FETCH_ASSOC);
            return $galleries;
        } catch (\Exception $exc) {
            $this->logger->error("Query: {$query}");
            $this->logger->exp_err($exc);
        }
        return array();
    }

    public function getListImagesGalleries(array $gallery_ids)
    {
        try {
            $gallery_ids = implode(',', $gallery_ids);
            $query = "SELECT gim.gallery_id, gim.id from gallery_image gim WHERE gallery_id IN ($gallery_ids)";

            $images = $this->db_conn->executeResultQuery($query, \PDO::FETCH_GROUP);
            return $images;
        } catch (\Exception $exc) {
            $this->logger->error("Query: {$query}");
            $this->logger->exp_err($exc);
        }
        return array();
    }

    public function getTotalGalleries($channel, $gallery_type) {
        try{
            $query = "SELECT COUNT(*) FROM (SELECT
                          c.id, COUNT(gi.gallery_id) as total
                        FROM
                          content c
                          JOIN content_channel cc
                            ON c.channel_id = cc.id
                            AND (
                              LOWER(cc.name) = LOWER('{$channel}')
                            )
                          JOIN gallery_image gi
                            ON c.id = gi.gallery_id
                        WHERE c.type = 1 AND c.gallery_type={$gallery_type}
                          AND (
                            c.start_date <= NOW()
                            OR c.start_date is null
                          )
                          AND (
                            c.expire_date >= NOW()
                            OR c.expire_date is null
                          )
                          AND c.obsolete = 0
                          AND c.status=1
                          GROUP BY gi.gallery_id
                          HAVING total >= 3
                          ) AS total";
            $total = $this->db_conn->fetchQuery($query, \PDO::FETCH_COLUMN);
            return $total;
        } catch (\Exception $exc) {
            $this->logger->error("Query: {$query}");
            $this->logger->exp_err($exc);
        }
        return 0;
    }
}