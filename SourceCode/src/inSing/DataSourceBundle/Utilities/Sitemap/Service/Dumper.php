<?php

/*
 * This file is part of the prestaSitemapPlugin package.
 * (c) David Epely <depely@prestaconcept.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace inSing\DataSourceBundle\Utilities\Sitemap\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use inSing\DataSourceBundle\Utilities\Sitemap\DumpingUrlset;
use Doctrine\Common\Cache\MemcachedCache;

/**
 * Service for dumping sitemaps into static files
 *
 * @author Konstantin Tjuterev <kostik.lv@gmail.com>
 * @author Konstantin Myakshin <koc-dp@yandex.ru>
 */

class Dumper extends AbstractGenerator
{
    protected $cacheTimeLive = 604800; // 1 week

    /**
     * Path to folder where temporary files will be created
     *
     * @var string
     */
    protected $tmpFolder;

    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    protected $filesystem;

    /**
     * @var string
     */
    protected $sitemapFilePrefix;

    protected $cache;

    protected $country_code;

    protected $site_map_index_content_temp = null;

    protected $doctrine_connection = null;

    /**
     * @param Filesystem $filesystem Symfony's Filesystem
     * @param $sitemapFilePrefix
     */

    public function __construct(
        Filesystem $filesystem,
        $logger,
        $host,
        $doctrine_connection,
        $cache = null,
        $country_code = '',
        $cacheTimeLive = 0,
        $limitUrls = 0,
        $limitBytes = 0,
        $sitemapFilePrefix = self::DEFAULT_FILENAME
    ) {
        parent::__construct($logger, $host);
        $this->doctrine_connection = $doctrine_connection;
        $this->filesystem = $filesystem;
        $this->sitemapFilePrefix = $sitemapFilePrefix;
        $this->cache = $cache;
        $this->cacheTimeLive = $cacheTimeLive ? $cacheTimeLive : $this->cacheTimeLive;
        $this->limitUrls = $limitUrls;
        $this->limitBytes = $limitBytes;
        $this->country_code = $country_code;
    }

    protected function getIndexCacheKey()
    {
        return self::SITEMAP_PREFIX_CACHE_KEY . $this->country_code . $this->sitemapFilePrefix . '.xml';
    }
    protected function getSectionCacheKey($section)
    {
        return self::SITEMAP_PREFIX_CACHE_KEY . $this->country_code . $section;
    }

    protected function getIndexTempCacheKey()
    {
        return 'TEMP_' . self::SITEMAP_PREFIX_CACHE_KEY . $this->country_code . $this->sitemapFilePrefix . '.xml';
    }

    /**
     * Dumps sitemaps and sitemap index into provided directory
     *
     * @param string $targetDir Directory where to save sitemap files
     * @param string $host
     * @param null   $section   Optional section name - only sitemaps of this section will be updated
     * @param array  $options   Possible options: gzip
     *
     * @return array|bool
     */
    public function dump($targetDir, $section = null, array $options = array())
    {
      try{
        $filenames = array();
        $options = array_merge(array('gzip' => false), $options);

        // we should prepare temp folder each time, because dump may be called several times (with different sections)
        // and activate command below removes temp folder
        $this->prepareTempFolder();

        //$this->populate($section);

        // if no urlset wasn't created during populating
        // it means no URLs were added to the sitemap
        if (!count($this->urlsets)) {
            return array();
        }

        foreach ($this->urlsets as $urlset) {
            $urlset->save($this->tmpFolder, $options['gzip']);
            $filenames[] = basename($urlset->getLoc());
        }

        if (null !== $section) {
            // Load current SitemapIndex file and add all sitemaps except those,
            // matching section currently being regenerated to root
            foreach ($this->loadCurrentSitemapIndexFile($targetDir . '/' . $this->sitemapFilePrefix . '.xml') as $key => $urlset) {
                // cut possible _X, to compare base section name
                $baseKey = preg_replace('/(.*?)(_\d+)?/', '\1', $key);
                if ($baseKey !== $section) {
                    // we add them to root only, if we add them to $this->urlset
                    // deleteExistingSitemaps() will delete matching files, which we don't want
                    $this->getRoot()->addSitemap($urlset);
                }
            }
        }

        file_put_contents($this->tmpFolder . '/' . $this->sitemapFilePrefix . '.xml', $this->getRoot()->toXml());
        $filenames[] = $this->sitemapFilePrefix . '.xml';

        // if we came to this point - we can activate new files
        // if we fail on exception eariler - old files will stay making Google happy
        $this->activate($targetDir);
      } catch (\Exception $exc) {
        $this->logger->err($exc->getMessage());
      }
      return $filenames;
    }

    /**
     * Prepares temp folder for storing sitemap files
     *
     * @return void
     */
    protected function prepareTempFolder()
    {
      try
      {
        $this->tmpFolder = sys_get_temp_dir() . '/HGWSitemaps-' . uniqid();
        $this->filesystem->mkdir($this->tmpFolder);
      } catch (\Exception $exc) {
        $this->logger->err($exc->getMessage());
      }
    }

    /**
     * Cleans up temporary files
     *
     * @return void
     */
    protected function cleanup()
    {
        $this->filesystem->remove($this->tmpFolder);
        $this->root = null;
        $this->urlsets = array();
    }

    /**
     * Loads sitemap index XML file and returns array of Urlset objects
     *
     * @param $filename
     *
     * @return array
     * @throws \InvalidArgumentException
     */

    protected function loadCurrentSitemapIndexFile($filename)
    {
      try
      {
        $urlsets = array();
        if (!file_exists($filename)) {
            return array();
        }

        $index = simplexml_load_file($filename);
        foreach ($index->children() as $child) {
            if ($child->getName() == 'sitemap') {
                if (!isset($child->loc)) {
                  $this->logger->err("One of referenced sitemaps in $filename doesn't contain 'loc' attribute");
                }
                $basename = preg_replace('/^' . preg_quote($this->sitemapFilePrefix) . '\.(.+)\.xml(?:\.gz)?$/', '\1', basename($child->loc)); // cut .xml|.xml.gz

                if (!isset($child->lastmod)) {
                    $this->logger->err("One of referenced sitemaps in $filename doesn't contain 'lastmod' attribute");
                }
                $lastmod = new \DateTime($child->lastmod);
                $urlsets[$basename] = $this->newUrlsetIndex($child->loc, $lastmod);
            }
        }
      } catch (\Exception $exc) {
        $this->logger->err($exc->getMessage());
      }
      return $urlsets;
    }

    /**
     * Loads sitemap index XML file and returns array of Urlset objects
     * @return array
     * @throws \InvalidArgumentException
     */
    protected function loadCurrentSitemapIndexFromCache()
    {
      try
      {
        $urlsets = array();
        $xml_index_content = $this->cache->fetch($this->getIndexTempCacheKey(), true);

        if (!$xml_index_content) {
            return array();
        }

        $index = simplexml_load_string($xml_index_content);
        foreach ($index->children() as $child) {
            if ($child->getName() == 'sitemap') {
                if (!isset($child->loc)) {
                  $this->logger->err("One of referenced sitemaps in $this->sitemapFilePrefix.xml doesn't contain 'loc' attribute");
                }
                $basename = preg_replace('/^' . preg_quote($this->sitemapFilePrefix) . '\.(.+)\.xml(?:\.gz)?$/', '\1', basename($child->loc)); // cut .xml|.xml.gz

                if (!isset($child->lastmod)) {
                    $this->logger->err("One of referenced sitemaps in $this->sitemapFilePrefix.xml doesn't contain 'lastmod' attribute");
                }
                $lastmod = new \DateTime($child->lastmod);
                $urlsets[$basename] = $this->newUrlsetIndex($child->loc, $lastmod);
            }
        }
      } catch (\Exception $exc) {
        $this->logger->err($exc->getMessage());
      }
      return $urlsets;
    }

    /**
     * Loads sitemap index XML file and returns array of Urlset objects
     * @return array
     * @throws \InvalidArgumentException
     */
    protected function loadCurrentSitemapIndex()
    {
        try
        {
            $urlsets = array();
            $xml_index_content = $this->site_map_index_content_temp;

            if (!$xml_index_content) {
                return array();
            }

            $index = simplexml_load_string($xml_index_content);
            foreach ($index->children() as $child) {
                if ($child->getName() == 'sitemap') {
                    if (!isset($child->loc)) {
                        $this->logger->err("One of referenced sitemaps in $this->sitemapFilePrefix.xml doesn't contain 'loc' attribute");
                    }
                    $basename = preg_replace('/^' . preg_quote($this->sitemapFilePrefix) . '\.(.+)\.xml(?:\.gz)?$/', '\1', basename($child->loc)); // cut .xml|.xml.gz

                    if (!isset($child->lastmod)) {
                        $this->logger->err("One of referenced sitemaps in $this->sitemapFilePrefix.xml doesn't contain 'lastmod' attribute");
                    }
                    $lastmod = new \DateTime($child->lastmod);
                    $urlsets[$basename] = $this->newUrlsetIndex($child->loc, $lastmod);
                }
            }
        } catch (\Exception $exc) {
            $this->logger->err($exc->getMessage());
        }
        return $urlsets;
    }

    /**
     * Moves sitemaps created in a temporary folder to their real location
     *
     * @param string $targetDir Directory to move created sitemaps to
     *
     * @throws \RuntimeException
     */
    protected function activate($targetDir)
    {
      try
      {
        if (!is_dir($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        if (!is_writable($targetDir)) {
            $this->cleanup();
            $this->logger->err(sprintf('Can\'t move sitemaps to "%s" - directory is not writeable', $targetDir));
        }
        $this->deleteExistingSitemaps($targetDir);

        // no need to delete the root file as it always exists, it will be overwritten
        $this->filesystem->mirror($this->tmpFolder, $targetDir, null, array('override' => true));
        $this->cleanup();
      } catch (\Exception $exc) {
        $this->logger->err($exc->getMessage());
      }
    }

    /**
     * Deletes sitemap from caching of newly generated files
     *
     * @param $section string
     */
    protected function deleteExistingSitemapsFromCache($section)
    {
        try
        {
            $cache_key = self::SITEMAP_PREFIX_CACHE_KEY . $this->country_code . $this->sitemapFilePrefix . '.' . $section . '.xml.gz';
            $this->cache->delete($cache_key, true);

        } catch (\Exception $exc) {
            $this->logger->err($exc->getMessage());
        }
    }

    /**
     * Deletes sitemap files matching filename patterns of newly generated files
     *
     * @param $targetDir string
     */
    protected function deleteExistingSitemaps($targetDir)
    {
      try
      {
        foreach ($this->urlsets as $urlset) {
            $basename = basename($urlset->getLoc());
            if (preg_match('/(.*)_[\d]+\.xml(?:\.gz)?$/', $basename)) {
                continue; // skip numbered files
            }
            // pattern is base name of sitemap file (with .xml cut) optionally followed by _X for numbered files
            $basename = preg_replace('/\.xml(?:\.gz)?$/', '', $basename); // cut .xml|.xml.gz
            $pattern = '/' . preg_quote($basename, '/') . '(_\d+)?\.xml(?:\.gz)?$/';
            foreach (Finder::create()->in($targetDir)->name($pattern)->files() as $file) {
                $this->filesystem->remove($file);
            }
        }
      } catch (\Exception $exc) {
        $this->logger->err($exc->getMessage());
      }
    }

    /**
     * Factory method for creating Urlset objects
     *
     * @param string $name
     *
     * @param \DateTime $lastmod
     *
     * @return DumpingUrlset
     */
    protected function newUrlset($name, \DateTime $lastmod = null)
    {
        return new DumpingUrlset($this->limitUrls, $this->limitBytes, $this->host . $this->sitemapFilePrefix . '.' . $name . '.xml', $lastmod);
    }

    /**
     * Factory method for creating Urlset objects
     *
     * @param string $url
     *
     * @param \DateTime $lastmod
     *
     * @return DumpingUrlset
     */
    protected function newUrlsetIndex($url, \DateTime $lastmod = null)
    {
        return new DumpingUrlset($this->limitUrls, $this->limitBytes, $url, $lastmod);
    }

    /**
     * Dumps sitemaps and sitemap index into provided directory
     *
     * @param string $targetDir Directory where to save sitemap files
     * @param string $section
     * @param array  $options   Possible options: gzip
     *
     * @return array|bool
     */
    public function dump_to_cache($targetDir, $section = null, array $options = array())
    {
      try
      {
        $filenames = array();
        $options = array_merge(array('gzip' => false), $options);

        // we should prepare temp folder each time, because dump may be called several times (with different sections)
        // and activate command below removes temp folder
        $this->prepareTempFolder();

        //$this->populate($section);

        // if no urlset wasn't created during populating
        // it means no URLs were added to the sitemap
        if (!count($this->urlsets)) {
            return array();
        }

        foreach ($this->urlsets as $urlset) {
            $urlset->save($this->tmpFolder, $options['gzip']);
            $filenames[] = basename($urlset->getLoc());
        }

        if (null !== $section) {
            // Load current SitemapIndex file and add all sitemaps except those,
            // matching section currently being regenerated to root
            $sitemap_index = $this->loadCurrentSitemapIndexFromCache();
            foreach ($sitemap_index as $key => $urlset) {
                // cut possible _X, to compare base section name
                $baseKey = preg_replace('/(.*?)(_\d+)?/', '\1', $key);
                if ($baseKey !== $section) {
                    // we add them to root only, if we add them to $this->urlset
                    // will delete matching files, which we don't want
                    //$this->deleteExistingSitemapsFromCache($section);
                    $this->getRoot()->addSitemap($urlset);
                }
            }
        }

        file_put_contents($this->tmpFolder . '/' . $this->sitemapFilePrefix . '.xml', $this->getRoot()->toXml());
        $filenames[] = $this->sitemapFilePrefix . '.xml';

        // if we came to this point - we can activate new files
        // if we fail on exception eariler - old files will stay making Google happy
        $this->activate_cache();
      } catch (\Exception $exc) {
        $this->logger->err($exc->getMessage());
      }

      return $filenames;
    }

    /**
     * Dumps sitemaps and sitemap index into provided directory
     *
     * @param string $targetDir Directory where to save sitemap files
     * @param string $section
     * @param array  $options   Possible options: gzip
     *
     * @return array|bool
     */
    public function dump_to_database($targetDir, $section = null, array $options = array())
    {
        try
        {
            $filenames = array();
            $options = array_merge(array('gzip' => false), $options);

            // we should prepare temp folder each time, because dump may be called several times (with different sections)
            // and activate command below removes temp folder
            $this->prepareTempFolder();

            //$this->populate($section);

            // if no urlset wasn't created during populating
            // it means no URLs were added to the sitemap
            if (!count($this->urlsets)) {
                return array();
            }

            foreach ($this->urlsets as $urlset) {
                $urlset->save($this->tmpFolder, $options['gzip']);
                $filenames[] = basename($urlset->getLoc());
            }

            if (null !== $section) {
                // Load current SitemapIndex file and add all sitemaps except those,
                // matching section currently being regenerated to root
                $sitemap_index = $this->loadCurrentSitemapIndex();
                foreach ($sitemap_index as $key => $urlset) {
                    // cut possible _X, to compare base section name
                    $baseKey = preg_replace('/(.*?)(_\d+)?/', '\1', $key);
                    if ($baseKey !== $section) {
                        // we add them to root only, if we add them to $this->urlset
                        // will delete matching files, which we don't want
                        //$this->deleteExistingSitemapsFromCache($section);
                        $this->getRoot()->addSitemap($urlset);
                    }
                }
            }

            file_put_contents($this->tmpFolder . '/' . $this->sitemapFilePrefix . '.xml', $this->getRoot()->toXml());
            $filenames[] = $this->sitemapFilePrefix . '.xml';

            // if we came to this point - we can activate new files
            // if we fail on exception eariler - old files will stay making Google happy
            $this->activate_database();
        } catch (\Exception $exc) {
            $this->logger->err($exc->getMessage());
        }

        return $filenames;
    }

    /**
     * Moves sitemaps created in a temporary folder to cache
     * @throws \RuntimeException
     */
    protected function activate_cache()
    {
      try {
        if($this->cache)
        {
          if ($handler = opendir($this->tmpFolder)) {
            while (($sub = readdir($handler)) !== FALSE) {
              if (is_file($this->tmpFolder . "/" . $sub) && !in_array($sub, array(".", "..", "Thumb.db", "Thumbs.db"))) {
                  $file_sitemap = $this->tmpFolder . "/" . $sub;
                  $cache_key = self::SITEMAP_PREFIX_CACHE_KEY . $this->country_code . $sub;

                  // Check site index to save to temp cache
                  $site_index_cache_key = $this->getIndexCacheKey();
                  if($cache_key == $site_index_cache_key)
                  {
                      $cache_key = $this->getIndexTempCacheKey();
                  }
                  $this->cache->delete($cache_key, true);
                  $this->cache->save($cache_key, file_get_contents($file_sitemap), $this->cacheTimeLive, true);
              }
            }
            closedir($handler);
          }

        }
        $this->cleanup();
        } catch (\Exception $exc) {
          $this->logger->err($exc->getMessage());
      }
    }

    /**
     * Moves sitemaps created in a temporary folder to cache
     * @throws \RuntimeException
     */
    protected function activate_database()
    {
        try {
            if($this->cache)
            {
                if ($handler = opendir($this->tmpFolder)) {
                    while (($sub = readdir($handler)) !== FALSE) {
                        if (is_file($this->tmpFolder . "/" . $sub) && !in_array($sub, array(".", "..", "Thumb.db", "Thumbs.db"))) {
                            $file_sitemap = $this->tmpFolder . "/" . $sub;
                            if($sub == ($this->sitemapFilePrefix . '.xml'))
                            {
                                $this->site_map_index_content_temp = file_get_contents($file_sitemap);
                            }
                            else{
                                $sql = "INSERT INTO sitemap (file_name, site_map_content, country_code, created_date) ";

                                $bound = '(:file_name, :site_map_content, :country_code, :created_date)';

                                $sql .= ' VALUES ' . $bound;

                                $sql .= ' ON DUPLICATE KEY UPDATE site_map_content=:up_site_map_content, created_date=:up_created_date';

                                $stmt = $this->doctrine_connection->prepare($sql);

                                $array = array();
                                $date_now = new \DateTime();
                                $array[":file_name"] = $sub;
                                $array[":site_map_content"] = file_get_contents($file_sitemap);
                                $array[":country_code"] = $this->country_code;
                                $array[":created_date"] = $date_now->format('Y-m-d H:i:s');

                                $array[":up_site_map_content"] = file_get_contents($file_sitemap);
                                $array[":up_created_date"] = $date_now->format('Y-m-d H:i:s');
                                $stmt->execute($array);
                                $this->cache->delete($this->getSectionCacheKey($sub));
                            }
                        }
                    }
                    closedir($handler);
                }

            }
            $this->cleanup();
        } catch (\Exception $exc) {
            $this->logger->err($exc->getMessage());
        }
    }

    public function doneToIndexFile()
    {
        // Code
        return;
    }

    public function doneToIndexFileCached()
    {
        // Get new site map index from temp cache
        $cache_key_temp = $this->getIndexTempCacheKey();
        $cache_refreshed_content = $this->cache->fetch($cache_key_temp, true);

        // Add to new site map
        $cache_key = $this->getIndexCacheKey();
        $this->cache->delete($cache_key, true);
        $this->logger->info("Cache key is {$cache_key}");
        $this->cache->save($cache_key, $cache_refreshed_content, $this->cacheTimeLive, true);

        // Remove temp cache
        $this->cache->delete($cache_key_temp, true);
    }
    public function doneToIndexDatabase()
    {
        try {
            $sql = "INSERT INTO sitemap (file_name, site_map_content, country_code, created_date) ";

            $bound = '(:file_name, :site_map_content, :country_code, :created_date)';

            $sql .= ' VALUES ' . $bound;

            $sql .= ' ON DUPLICATE KEY UPDATE site_map_content=:up_site_map_content, created_date=:up_created_date';

            $stmt = $this->doctrine_connection->prepare($sql);

            $array = array();
            $date_now = new \DateTime();

            $array[":file_name"] = $this->sitemapFilePrefix . '.xml';
            $array[":site_map_content"] = $this->site_map_index_content_temp;
            $array[":country_code"] = $this->country_code;
            $array[":created_date"] = $date_now->format('Y-m-d H:i:s');

            $array[":up_site_map_content"] = $this->site_map_index_content_temp;
            $array[":up_created_date"] = $date_now->format('Y-m-d H:i:s');
            $stmt->execute($array);
            $this->cache->delete($this->getIndexCacheKey());
        } catch (\Exception $exc) {
            $this->logger->err($exc->getMessage());
        }
    }


}
