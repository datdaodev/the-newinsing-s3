<?php

/**
 * This file is part of the PrestaSitemapBundle
 *
 * (c) PrestaConcept <www.prestaconcept.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace inSing\DataSourceBundle\Utilities\Sitemap\Service;

use inSing\DataSourceBundle\Utilities\Sitemap\Url\Url;
use inSing\DataSourceBundle\Utilities\Sitemap\Sitemapindex;
use inSing\DataSourceBundle\Utilities\Sitemap\Url\UrlConcrete;


/**
 * Abstract sitemap generator class
 *
 * @author Konstantin Myakshin <koc-dp@yandex.ru>
 */
abstract class AbstractGenerator
{
    const DEFAULT_FILENAME = 'sitemap';

    const SITEMAP_PREFIX_CACHE_KEY = 'HGW_REGIONAL_SITEMAP';

    protected $limitUrls = 0;
    protected $limitBytes = 0;

    /**
     * @var Sitemapindex
     */
    protected $root;

    /**
     * @var Urlset[]|DumpingUrlset[]
     */
    protected $urlsets = array();

    protected $logger = null;

    protected $host;


    public function __construct($logger, $host)
    {
        $this->logger = $logger;
        $this->host = $host;
    }

    /**
     * add an Url to an Urlset
     *
     * section is helpfull for partial cache invalidation
     *
     * @param Url    $url
     * @param string $section
     *
     * @throws \RuntimeException
     */
    public function addUrl(Url $url, $section)
    {
      try{
        $urlset = $this->getUrlset($section);

        //maximum 50k sitemap in sitemapindex
        $i = 0;

        $xmlConstrain = new Sitemapindex($this->limitUrls, $this->limitBytes);
        while ($urlset->isFull() && $i <= $xmlConstrain->getLimitItems()) {
            $urlset = $this->getUrlset($section . '_' . $i);
            $i++;
        }

        if ($urlset->isFull()) {
            $this->logger->err('The limit of sitemapindex has been exceeded');
            return;
        }
        $urlset->addUrl($url);
      } catch (\Exception $exc) {
        $this->logger->err($exc->getMessage());
      }
    }

    /**
     * add an Url to an Urlset
     *
     * section is helpfull for partial cache invalidation
     *
     * @param string    $url
     * @param string $section
     *
     * @throws \RuntimeException
     */
    public function addUrlString($url, $section, $priority = 1)
    {
      try{
        $url_obj = new UrlConcrete(
                          $url,
                          new \DateTime(),
                          UrlConcrete::CHANGEFREQ_DAILY,
                          $priority
                      );
        $this->addUrl($url_obj, $section);
      } catch (\Exception $exc) {
        $this->logger->err($exc->getMessage());
      }
    }

    /**
     * get or create urlset
     *
     * @param string $name
     *
     * @return Sitemap\Urlset
     */
    public function getUrlset($name)
    {
        if (!isset($this->urlsets[$name])) {
            $this->urlsets[$name] = $this->newUrlset($name);
        }

        return $this->urlsets[$name];
    }

    /**
     * Factory method for create Urlsets
     *
     * @param string $name
     * @param \DateTime $lastmod
     *
     * @return Sitemap\Urlset
     */
    abstract protected function newUrlset($name, \DateTime $lastmod = null);

    /**
     * Dispatches SitemapPopulate Event - the listeners should use it to add their URLs to the sitemap
     *
     * @param string|null $section
     */

    /**
     * @return Sitemap\Sitemapindex
     */
    protected function getRoot()
    {
        if (null === $this->root) {
            $this->root = new Sitemapindex($this->limitUrls, $this->limitBytes);

            foreach ($this->urlsets as $urlset) {
                $this->root->addSitemap($urlset);
            }
        }

        return $this->root;
    }

    public function validationUrl($url) {
        return (filter_var($url, FILTER_VALIDATE_URL) !== FALSE);
    }

    public function pingSiteMapWithCURL($url) {
        $is_url = $this->validationUrl($url);
        if(!$is_url)
        {
            return false;
        }
        $result = FALSE;
        // Initiate curl.
        $ch = curl_init();
        // Set timeout.
        $timeout = 30;
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        // Set url for call.
        curl_setopt($ch, CURLOPT_URL, $url);
        // Execute curl call.
        $success = curl_exec($ch);
        // Only on success, get the status.
        if ($success) {
            // Get info about last curl call.
            $result = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        }
        // Close curl.
        curl_close($ch);
        return (int) $result;
    }

    public function pingSiteMapWithFSockOpen($url) {

        $is_url = $this->validationUrl($url);
        if(!$is_url)
        {
            return false;
        }

        $result = FALSE;
        // Parse url.
        $url = parse_url($url);
        // Append query to path.
        $url['path'] .= '?'.$url['query'];
        // Setup fsockopen.
        $port = 80;
        $timeout = 30;
        $errno = null;
        $errstr = null;
        $match = null;
        $fso = fsockopen($url['host'], $port, $errno, $errstr, $timeout);
        // Proceed if connection was successfully opened.
        if ($fso) {
            // Create headers.
            $headers = 'GET ' . $url['path'] . 'HTTP/1.0' . "\r\n";
            $headers .= 'Host: ' . $url['host'] . "\r\n";
            $headers .= 'Connection: closed' . "\r\n";
            $headers .= "\r\n";
            // Write headers to socket.
            fwrite($fso, $headers);
            // Set timeout for stream read/write.
            stream_set_timeout($fso, $timeout);
            // Use a loop in case something unexpected happens.
            // I do not know what, but that why it is unexpected.
            while (!feof($fso)){
                // 128 bytes is getting use the header with the http response code in it.
                $buffer = fread($fso, 128);
                // Filter only the http status line (first line) and break loop on success.
                if(!empty($buffer) && ($buffer = substr($buffer, 0, strpos($buffer, "\r\n")))){
                    break;
                }
            }
            // Match status.
            preg_match('/^HTTP.+\s(\d{3})/', $buffer, $match);
            // Extract status.
            list(, $status) = $match;
            $result = $status;
        }
        else {
            // @XXX: Throw exception here??
        }
        return (int) $result;
    }
}
