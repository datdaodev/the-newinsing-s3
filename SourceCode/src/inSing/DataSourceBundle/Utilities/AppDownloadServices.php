<?php
namespace inSing\DataSourceBundle\Utilities;
use \Symfony\Component\DependencyInjection\ContainerInterface ;
/**
 * App download 
 * 
 * @author Minh Tong
 */
class AppDownloadServices
{  
    const IOS = 'iOS';
    const ANDROID = 'Android';
    const BLACKBERRY = 'BlackBerry';
    const WINDOWPHONE = 'WindowPhone';
    
    private $container = null;
    private $currentCountry = null;
    private $paramURL = null;
    public function __construct(ContainerInterface $container , $currentCountry){
        $this->container = $container;
        $this->currentCountry = $currentCountry;
        $this->paramURL = $this->container->getParameter("app_download_url");
    }
    
    /**
     * Author Minh Tong
     * render to app download page for Singapore location
     * @return String URL
     */
    public function renderSGApp() {
        try{
            if ($this->is(self::IOS)) {
                return $this->redirectToApp(self::IOS); 
            }
            if ($this->is(self::ANDROID)) {
          
                return $this->redirectToApp(self::ANDROID); 
            }
            if ($this->is(self::BLACKBERRY)) {
                return $this->redirectToApp(self::BLACKBERRY); 
            }
            if ($this->is(self::WINDOWPHONE)) {
                return $this->redirectToApp(self::WINDOWPHONE); 
            }
            else {
                if ($this->otherCase()) {
                    return $this->redirectToApp(null); 
                } else {
                    return $this->redirectToApp(Constant::DEVICE_TABLET); 
                }
            }
        } catch (Exception $ex) {
             return null;
        }
    }
    
    /**
     * Author Minh Tong
     * render to app download page for Malaysia location
     * @return String URL
     */
    public function renderMYApp() {
        try{
            if ($this->is(self::IOS) && ($this->isWindowPhone() === false)) {
                return $this->redirectToApp(self::IOS); 
            }
            if ($this->is(self::ANDROID) && ($this->isWindowPhone() === false)) {
                return $this->redirectToApp(self::ANDROID); 
            }
            else {
                return null;
            }
        } catch (Exception $ex) {
             return null;
        }
    }
        
    private function isWindowPhone(){
        return (strpos($_SERVER['HTTP_USER_AGENT'],"Windows Phone")) ;     
    }    
    public function redirectToApp($device){
        
        switch ($device) {
            case self::IOS: 
                if($this->isMYCountry()){
                    return $this->paramURL[$this->currentCountry][self::IOS];
                } else {
                    return $this->paramURL[$this->currentCountry][self::IOS];
                }
                break;
            case self::ANDROID: 
                if($this->isMYCountry()){
                    return $this->paramURL[$this->currentCountry][self::ANDROID];
                } else {
                    return $this->paramURL[$this->currentCountry][self::ANDROID];
                }
                break; 
            case self::BLACKBERRY: 
                return $this->paramURL[$this->currentCountry][self::BLACKBERRY];
                break;
            case self::WINDOWPHONE: 
                return $this->paramURL[$this->currentCountry][self::WINDOWPHONE];
                break; 
            case Constant::DEVICE_TABLET: 
                return Constant::DEVICE_TABLET;
                break;    
            default: 
                return Constant::DEVICE_COMPUTER;
                break; 
        }
    }
    
    private function is($device){
        return $this->container->get('common.utils')->mobileOperaSystemDetect($device);
    }
    
    private function isSGCountry(){
        return ($this->currentCountry == Constant::COUNTRY_CODE_SINGAPORE);
    }
    
    private function isMYCountry(){
        return ($this->currentCountry == Constant::COUNTRY_CODE_MALAYSIA);
    }
    
     /**
     * Author Minh Tong
     * check device is not mobile platform
     * @return String URL
     */
    private function otherCase(){
        $result = true;
        $mobile = $this->container->get('common.utils')->isMobile();
        $tablet = $this->container->get('common.utils')->isTablet();
        
        $device = ($mobile ? ($tablet ? Constant::DEVICE_TABLET : Constant::DEVICE_PHONE ) : Constant::DEVICE_COMPUTER);
	
        if ($device != Constant::DEVICE_COMPUTER)
        {
           $result = false;
        }
        return $result;    
    }
}
