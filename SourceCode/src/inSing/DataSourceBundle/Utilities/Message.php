<?php

/**
 * Constant for message
 * @author HoangMinh TonThat <minh.tont@s3corp.com.vn>
 * @copyright (c) 2012, SingTel Digital Media Pte Ltd. All Rights Reserved.
 */

namespace inSing\DataSourceBundle\Utilities;

class Message {

   const SAVED_SUCCESSFULLY = "Saved successfully!";
   const PUBLISH_SUCCESSFULLY = "Published Successfully!";
   const ACTIVATED_SUCCESSFULLY = "Activated successfully!";
   const DEACTIVATED_SUCCESSFULLY = "Deactivated successfully!";
   const UPDATED_SUCCESSFULLY = "Updated successfully!";
   const DELETED_SUCCESSFULLY = "Deleted successfully!";
   const SAVED_UNSUCCESSFULLY = "Saved unsuccessfully!";
   const UPDATED_UNSUCCESSFULLY = "Updated unsuccessfully!";
   const DELETED_UNSUCCESSFULLY = "Deleted unsuccessfully!"; 

   const TURN_ON_CRON_REFRESH_PAGE = "You have turn on cron job refresh data for page successfully!";

   const ERROR_ALL_FIELD_IS_REQUIRED = "Error! All fields is required.";

   const WARNING_DATA_OVERFLOW = "Overflow size of APNS params";

   //Messages of getArticleContent() and getBusinessContent()
   const WARNING_RESTAURANT_DO_NOT_HAVE_TABLEDB_ID = "This restaurant doesn't have TableDbId. So, you can't pick this restaurant";
   const WARNING_NOT_FOUND_SERIES_FOR_ARTICLE = "The series of this article are not suitable with Food/Drink";

   //Messages of Get Story action
   const GET_STORY_NULL_INPUT_DATA = "Please input data to get";
   const GET_STORY_WRONG_URL = "Please input url of Hungry Go Where";
   const GET_STORY_NOT_FOUND_DATA = "Data not found";
   const GET_STORY_WRONG_INPUT_DATA = "Please check your input data";

   //Campaign
   const CAMPAIGN_FULL_SAVE = "Only one Promotion you can pick";

   //Messages of HPB Featured Reviews
   const ERROR_DUPPLICATE_REVIEWS = "Saved Unsuccessfully! Review is dupplicated!";

   // Admin - template
   const TEMPLATE_CAN_NOT_EDIT = "This template is being used. Unable to edit";
   const TEMPLATE_CAN_NOT_DELETE = "This template is being used. Unable to delete";

   // Admin - page
   const ERROR_UNIQUE_SLUG_IN_PAGE_DETAIl = "Slug already exists in current database, please type another slug.";
}
