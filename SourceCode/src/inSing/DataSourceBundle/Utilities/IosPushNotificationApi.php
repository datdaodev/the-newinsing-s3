<?php

/**
 * IosPushNotificationApi class
 *
 */
namespace inSing\DataSourceBundle\Utilities;

use inSing\DataSourceBundle\Repository\PushNotificationRepository;

class IosPushNotificationApi
{

    const OS_IOS = 'ios';
    const OS_ANDROID = 'android';
    const PUSH_API_SESSION_TOKEN_CACHE_KEY = 'PUSH_API_SESSION_TOKEN_CACHE_KEY';
    
    static public function getLogger($container) {
        return new HgwLogger($container, 'push_api');
    }


    static public function authenticate($container)
    {
        try {
            $result_authenticate = self::doPostWrapper($container,
	    		$container->getParameter('rnr_auth_url'),
                        $container->getParameter('rnr_auth_secret'),
	    		array(
                        'app_id' => $container->getParameter('rnr_auth_id'),
                        'secret' => $container->getParameter('rnr_auth_secret')
	    		)
            );

            if (Curl::HTTP_OK != $result_authenticate['status'])
            {
                return null;
            }
            return array("session_token" => $result_authenticate['session_token'],
                "expire_at" => $result_authenticate['expire_at']
            );
        } catch (\Exception $exc) {
            self::getLogger($container)->err($exc->getCode() . ':' . $exc->getMessage());
        }
    }

    /**
     * Get session token
     * @author Date Tran
     * @return string sessionToken
     */
    static public function getSessionToken($container)
    {
        try {
            $cache = $container->get('hgw.cache');
            $sessionToken = $cache->fetch(self::PUSH_API_SESSION_TOKEN_CACHE_KEY);
            if (empty($sessionToken)) {
                $result = self::authenticate($container);
                if(!empty($result) && count($result))
                {
                  $sessionToken = $result['session_token'];
                  $cache->save(self::PUSH_API_SESSION_TOKEN_CACHE_KEY, $sessionToken);
                }
            }
            return $sessionToken;
        } catch (\Exception $exc) {
            // Log
        }

        return null;
    }
    
    private static function getCallingString($url, $secret, $fields, $is_return_array = true)
    {
    	//Generate API Signature
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);
        $pathinfo = $secret . $pathinfo;

        $fields['sig'] = self::genSignature($fields, $pathinfo);

        if(!$is_return_array)
        {
            // form up variables in the correct format for HTTP POST
            $fields_string = '';
            foreach ($fields as $key => $value) {
                $fields_string .= $key . '=' . urlencode($value) . '&';
            }
            $fields_string = rtrim($fields_string,'&');

            return $fields_string;
        }
        return $fields;
    }
    
    private static function genSignatureJson($secret, $params, $pathinfo)
    {
    	$params = json_encode($params);
    	$params = str_replace("\n", '', $params);
    	$params = str_replace("\r", '', $params);

    	$pathinfo = $secret . $pathinfo;

        return md5( $pathinfo . $params );
    }
    
    private static function genSignature($params, $pathinfo)
    {
    	$result = $pathinfo;

        ksort($params);
    	foreach ($params as $key => $value)
    	{
            $result .=  urlencode($key . $value);
        }
        return md5($result);
    }

    protected static function doGetWrapper($container, $url, $secret, $fields) {
        
        $curl = new Curl($container);
        $curl->setMethod(Curl::HTTP_GET);
        $fields = self::getCallingString($url, $secret, $fields);
        self::getLogger($container)->info($url);
        $res = $curl->call($url, $fields);
        
        if ($res['status'] == Curl::SESSION_TOKEN_TIMEOUT || $res['status'] == Curl::HTTP_UNAUTHORIZED) {
            $sessionTokenCache = $container->get('hgw.cache');
            $sessionTokenCache->delete(self::PUSH_API_SESSION_TOKEN_CACHE_KEY);
            $fields['session_token'] = self::getSessionToken($container);
            
            $fields = self::getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
        } else if ($res['status'] != Curl::HTTP_OK) {
            self::getLogger($container)->info($url, $res);
        }
        return $res;
    }

    protected static function doPostWrapper($container, $url, $secret, $fields) {
        
        $curl = new Curl($container);
        $curl->setMethod(Curl::HTTP_POST);
        
        $fields = self::getCallingString($url, $secret, $fields);
        self::getLogger($container)->info($url, $fields);
        $res = $curl->call($url, $fields);
        
        if ($res['status'] == Curl::SESSION_TOKEN_TIMEOUT || $res['status'] == Curl::HTTP_UNAUTHORIZED) {
            $sessionTokenCache = $container->get('hgw.cache');
            $sessionTokenCache->delete(self::PUSH_API_SESSION_TOKEN_CACHE_KEY);
            $fields['session_token'] = self::getSessionToken($container);
            
            $fields = self::getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
        } else if ($res['status'] != Curl::HTTP_OK) {
            self::getLogger($container)->info($url, $res);
        }
        return $res;
    }
    
    protected static function doPostJsonWrapper($container, $url, $secret, $fields) {
        
        $curl = new Curl($container);
        $curl->setMethod(Curl::HTTP_POST);
        $curl->setPostJson(true);
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);
        
        $url_call = $url . '?sig=' . self::genSignatureJson($secret, $fields, $pathinfo);
        self::getLogger($container)->info($url_call, $fields);
                
        $res = $curl->call($url_call, $fields);
        
        if ($res['status'] == Curl::SESSION_TOKEN_TIMEOUT || $res['status'] == Curl::HTTP_UNAUTHORIZED) {
            $sessionTokenCache = $container->get('hgw.cache');
            $sessionTokenCache->delete(self::PUSH_API_SESSION_TOKEN_CACHE_KEY);
            $fields['session_token'] = self::getSessionToken($container);
            $url_call = $url . '?sig=' . self::genSignatureJson($secret, $fields, $pathinfo);
            self::getLogger($container)->info($url_call, $fields);
            $res = $curl->call($url_call, $fields);
        } else if ($res['status'] != Curl::HTTP_OK) {
            self::getLogger($container)->info($url, $res);
        }
        return $res;
    }

  /**
     * @param $container
     * @return array|Returns
     * author: Tuan.bui
     */
    static public function updatePushNotification( $container ) {
        try {
            $session_token = self::getSessionToken($container);

            if (is_null($session_token)) {
                return array();
            }

            $em = $container->get('doctrine')->getManager();
            $dataPushFromDB = $em->getRepository('inSingDataSourceBundle:PushNotification')->getNotificationToUpdateByStatus($container);
            $limitTime = $container->getParameter('ios_push_limit_time');
            $dataPush = '';
            $mergeDataPush = array();

            $countLimitDataPush = count($dataPushFromDB);
            if ($countLimitDataPush) {
                $pageSize = $container->getParameter('ios_push_paging');
                $amountLoop = ceil($countLimitDataPush / $pageSize);
                if ($amountLoop > 0) {
                    for ($i = 1; $i <= $amountLoop; $i++) {
                        $dataPush = self::loopUpdatePush($container, $session_token, $dataPushFromDB, $i, $pageSize);
                        //Return if not SUCCESS
                        if (Curl::HTTP_OK != $dataPush['status']) {
                            return "Error: {$dataPush['status']}, {$dataPush['error']}";
                        }

                        if (count($dataPush)) {
                            $mergeDataPush = array_merge($mergeDataPush, $dataPush['messages']);
                        }
                    }

                    return $mergeDataPush;
                }
            } else {
                return "There is no reservation from RnR.";
            }
        } catch (\Exception $exc) {
            return $exc->getMessage();
        }
    }

    static public function loopUpdatePush($container, $session_token, $dataPushFromDB, $i, $pageSize) {
        $j = $pageSize * ($i - 1);
        $pageSize = $pageSize * $i;

        $dataPush = array();
        for ($j; $j < $pageSize; $j++) {
            if (isset($dataPushFromDB[$j])) {
                $dataPush[] = $dataPushFromDB[$j]->getJobId();
            }
        }

        $data = array(
            'app' => $container->getParameter('app_hgw_code'),
            'session_token' => $session_token,
            'message_id' => $dataPush
        );

        //Get Recommendation Item List from API
        $result = self::doPostJsonWrapper(
            $container,
            $container->getParameter('push_api_url') . "statistics",
            $container->getParameter('rnr_auth_secret'),
            $data
        );

        return $result;
    }

    /**
     * Prod: https://api.push.insing.com/notification/send
     * Test: https://api.test.push.insing.com/notification/send
     * Method call: POST
     * Params to call API: app(string), session_token(string), paramsAPNS(array)
     * @author BienTran
     * @param ContainerInterface $container
     * @param array $paramsAPNS
     * @return array
     */
    public static function sendNotificationToAPNS($container, $paramsAPNS = array())
    {

        $sessionToken = self::getSessionToken($container);

        $params = array(
                'session_token' => $sessionToken,
                //'app' => $container->getParameter('app_hgw_code'),
            );
        $params = array_merge($params, $paramsAPNS);


        $itemsList = self::doPostJsonWrapper($container,
            $container->getParameter('push_api_url').'notification/send',
            $container->getParameter('rnr_auth_secret'),
            $params
        );

        //Return if not SUCCESS
//        if ( RestfulAPIHelper::HTTP_OK != $itemsList['status'] ) return array();

        return $itemsList;
    }

    /**
     * @author vu.luu
     * Get message length
     *
     * @param  array $data
     *
     * E.g.
     * array(
     *     'aps' => array(
     *         'alert' => 'Test Message',
     *         'badge' => 2,
     *         'sound' => 'Chill'
     *     ),
     *     'page' => 'Profile',
     *     'url' => 'http://bit.ly/uFak12'
     * )
     *
     * @return int
     */
    public static function getSize($data)
    {
        // generate json data
        $sJSON = json_encode($data, defined('JSON_UNESCAPED_UNICODE') ? JSON_UNESCAPED_UNICODE : 0);

        if (!defined('JSON_UNESCAPED_UNICODE') && function_exists('mb_convert_encoding')) {
            $sJSON = preg_replace_callback(
                '~\\\\u([0-9a-f]{4})~i',
                create_function('$aMatches', 'return mb_convert_encoding(pack("H*", $aMatches[1]), "UTF-8", "UTF-16");'),
                $sJSON
            );
        }

        $sJSONPayload = str_replace('"aps":[]', '"aps":{}', $sJSON);

        return strlen($sJSONPayload);
    }

}
