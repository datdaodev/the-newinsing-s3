<?php

namespace inSing\DataSourceBundle\Utilities\ApiContent;

use inSing\DataSourceBundle\Utilities\ApiContent\ApiContentAbstract;
use inSing\DataSourceBundle\Utilities\BusinessApiUtils;
use inSing\DataSourceBundle\Utilities\RnRApiUtils;
use inSing\DataSourceBundle\Utilities\Common;
use inSing\DataSourceBundle\Utilities\LocaleParser;

class Business extends ApiContentAbstract {

    /**
     * @var Array
     */
    protected $result = array(
        'result_code' => 0,
        'status' => '',
        'item_id' => '',
        'title' => '',
        'description' => '',
        'neighbourhood' => '',
        'address' => '',
        'image' => '',
        'type_of_place' => '',
        'booking_id' => '',
        'url' => '',
        'avg_price' => '',
        'longitude' => '',
        'latitude' => '',
        'slug' => '',
        'state' => '',
        'vote_count' => '',
        'thumbs_up_count' => '',
        'remove_competitor_banner' => '',
    );


    public function __construct() {
        parent::__construct();
    }

    /**
     * Return api result
     *
     * @param Array $params Include itemId, transCodeName
     * @return Array
     */
    public function getContent($params) {
        // get article from cms api
        if (!isset($params['item_id']) || empty($params['item_id']) ||
                !isset($params['module_type']) && !empty($params['module_type']) ) {
            throw new \Exception('Invalid variable');
        }
        $locale = $this->controller->getSelectedCountry();
        $transcodeName = $this->getTransCodeName($params['module_type']);
        $largerTransCode = $this->getLargerTransCode($params['module_type']);
        $itemId = $params['item_id'];
        // get IBL detail
        $apiResult = $this->container->get('bizsearch.api')->getBusinessDetails(array($itemId), $largerTransCode, $locale);
        
        if(empty($apiResult)) {
            $this->result['result_code'] = 404;
            return $this->result;
        }
        $status = isset($apiResult['status'])
            ? $apiResult['status']
            : '';
        $this->result['status'] = $status;
        if($status == 'Closed') {
            $this->result['result_code'] = 404;
            return $this->result;
        }
        $this->result['item_id'] = isset($apiResult['id'])
            ? $apiResult['id']
            : '';
        if(empty($this->result['item_id'])) {
            $this->result['result_code'] = 404;
            return $this->result;
        }
        // Adding title
        $this->result['title'] = html_entity_decode(strip_tags($apiResult['name']), ENT_NOQUOTES);
        // Adding description
        $this->result['description'] = isset($apiResult['description']) ? htmlspecialchars(html_entity_decode(strip_tags($apiResult['description']), ENT_NOQUOTES)) : '';
        // Adding address & neighbourhood
        $fullAddress = '';
        $neighbourhood = '';
        if (isset($apiResult['address'])) {
            array_walk($apiResult['address'], function(&$value) {
                $value = is_array($value)
                    ? $value
                    : html_entity_decode($value, ENT_QUOTES);
            });
            $addressArr = array(
                $apiResult['address']['unit_number'],
                $apiResult['address']['building_name'],
                $apiResult['address']['block_number']);
            $addressArr = array_filter($addressArr, function ($item){
                return $item != '' ? true : false;
            });
            $fullAddress = isset($apiResult['address'])
                ? trim(join(', ', $addressArr) . ' ' . $apiResult['address']['street_name'])
                : '';
            $neighbourhood = $apiResult['address']['neighbourhood'];
        }

        $fullAddress = LocaleParser::getInstance()->parseBusinessAddress($apiResult, $locale, false);
        $this->result['neighbourhood'] = $neighbourhood;
        $this->result['address'] = $fullAddress;
        // Adding cuisine
        $this->result['cuisine'] = isset($apiResult['cuisine']) ? $apiResult['cuisine']: '';
        if (isset($apiResult['default_image_media_path']) && !empty($apiResult['default_image_media_path'])) {
            $this->result['image'] = $this->controller->get('common.utils')->generatePhotoFromPath($apiResult['default_image_media_path'], $largerTransCode, $transcodeName);
        } else {
            if (isset($apiResult['images']) && !empty($apiResult['images'])) {
                foreach ($apiResult['images'] as $media) {
                    if (!empty($media['media_path'])) {
                        $this->result['image'] = $this->controller->get('common.utils')->generatePhotoFromPath($media['media_path'], $largerTransCode, $transcodeName);
                        break;
                    }
                }
            }
        }
        if(empty($this->result['image'])) {
            $arrUserPhotos = $this->container->get('rnr.api')->getUserPhotosByRestaurantId($apiResult['id'], 1, 1, false, $locale);
            if (!empty($arrUserPhotos) && isset($arrUserPhotos['total']) && $arrUserPhotos['total'] > 0) {
                $this->result['image'] = $this->container->get('common.utils')
                    ->generatePhotoFromUrl($arrUserPhotos['photos'][0]['url'], $largerTransCode, $transcodeName);
            }
        }
        // Adding slug name
        $this->result['slug'] = isset($apiResult['slug']) ? $apiResult['slug'] : '';
        // Adding state
        $this->result['state'] = isset($apiResult['state']) ? $apiResult['state'] : '';
        // Adding longitude & latitude
        $this->result['longitude'] = isset($apiResult['longitude']) ? $apiResult['longitude'] : '';
        $this->result['latitude'] = isset($apiResult['latitude']) ? $apiResult['latitude'] : '';
        // Adding booking_id
        $this->result['booking_id'] = isset($apiResult['tabledb'])
            ? $apiResult['tabledb']['restaurant_id']
            : 0;
        // Adding url
        $this->result['url'] = isset($apiResult['url']) ? $apiResult['url'] : '';
        // Adding avg_price
        $this->result['avg_price'] = isset($apiResult['avg_price'])
            ? $apiResult['avg_price']
            : '';
        /* Processing sum of array recursively */
        $averageOfRatings = Common::avgArrayRecursive($apiResult['rnr']['ratings']);
        
        // Adding vote count
        $this->result['vote_count'] = isset($apiResult['vote_count']) ? $apiResult['vote_count'] : '';
        $this->result['thumbs_up_percentage'] = isset($apiResult['thumbs_up_percentage']) ? $apiResult['thumbs_up_percentage'] : '';
        $this->result['remove_competitor_banner'] = isset($apiResult['remove_competitor_banner']) ? $apiResult['remove_competitor_banner'] : '';
        $this->result['result_code'] = 200;
        $this->result['average_of_ratings'] = isset($averageOfRatings) ? $averageOfRatings : 0;
        $this->result['review_count'] = isset($apiResult['rnr']['review_count']) ? $apiResult['rnr']['review_count'] : 0;

        return $this->result;
    }

}

