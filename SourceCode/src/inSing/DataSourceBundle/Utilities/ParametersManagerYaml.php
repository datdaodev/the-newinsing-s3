<?php
namespace inSing\DataSourceBundle\Utilities;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Yaml\Yaml;

/**
 * Works with parameters.yml at a low level, extracts and writes back value of assets_version
 * Because parsing is done using regular expressions insead of YamlParser, formatting is preserved when writing value to file.
 *
 * @author Alexander Kachkaev <alexander@kachkaev.ru>
 *
 */
class ParametersManagerYaml
{
    static protected $versionParameterMask = '[a-zA-Z0-9_-]+';
    static protected $versionValuesMask = '[a-zA-Z0-9_\.-]*';
    protected $yml_parser;

    protected $fileInput;
    protected $fileOutput;
    protected $parametersName;

    protected $fileContents;
    protected $versionValues;
    protected $oldVersionValues;

    public function __construct($fileInput, $fileOutput)
    {
        $this->fileInput = $fileInput;
        $this->fileOutput = $fileOutput;
        $this->yml_parser = new Yaml();

        $this->readFile();
    }

    /**
     * Gets the value of assets version found in parameters file
     *
     * @param boolean $rereadFile - if true, re-reads the file first
     */
    public function getVersion($rereadFile = false)
    {
        if ($rereadFile)
            $this->readFile();

        return $this->versionValues;
    }

    /**
     * Sets a new value for assets version found in parameters file
     *
     * Assets version must consist only of letters, numbers and the following characters: .-_
     *
     * @param boolean $rereadFile - if true, re-reads the file first
     */
    public function setValues($values)
    {
        foreach ($values as $key => $value)
        {
            if (!preg_match('/^' . static::$versionParameterMask . '$/', $key)) {
                throw new \InvalidArgumentException(
                        'Wrong value for parameter name '
                        . var_export($key, true)
                        . ' - it should consist only of characters, numbers and dash or underscore');
            }

            if (array_key_exists($key, $this->fileContents['parameters'])) {
                $this->oldVersionValues[$key] = $this->fileContents['parameters'][$key];
            }
            else
            {
                throw new \Exception(
                        'Could not find definition of "' . $key
                        . '". Make sure it exists in "' . $this->fileInput
                        . '".');
            }

            // Checking value
            if (!is_string($value) && !is_numeric($value)) {
                throw new \InvalidArgumentException(
                        'Wrong value for assets version: '
                                . var_export($value, true)
                                . ' - it must be string or numeric.');
            }

            if (!preg_match('/^' . static::$versionValuesMask . '$/', $value)) {
                throw new \InvalidArgumentException(
                        'Wrong value for assets version: '
                                . var_export($value, true)
                                . '. It must be empty or consist only of letters, numbers and the following characters: .-_');
            }

            // Updating contents
            $this->fileContents['parameters'][$key] = $value;
            $this->versionValues[$key] = $value;
        }

        // Writing to file
        try {
            $this->fileContents = $this->yml_parser->dump($this->fileContents, 2);

            file_put_contents($this->fileOutput, $this->fileContents);
        } catch (\Exception $e) {
            throw new FileException(
                    'Could not write write "' . $this->fileOutput
                            . '". Make sure it exists and you have enough permissions.');
        }
    }

    /**
     * Reads and parses file with parameters
     *
     * @throws InvalidConfigurationException
     * @throws FileException
     * @throws \Exception
     */
    protected function readFile()
    {

        // Checking if file extension is supported
        $fileExtension = pathinfo($this->fileInput, PATHINFO_EXTENSION);
        if ($fileExtension != 'yml' && $fileExtension != 'yaml')
            throw new InvalidConfigurationException(
                    'Could not use "' . $this->fileInput
                            . '" - only yml files are supported by ParametersManagerYaml');

        // Reading file
        try {
            $this->fileContents = $this->yml_parser->parse($this->fileInput);
        } catch (\Exception $e) {
            throw new FileException(
                    'Could not read file "' . $this->fileInput
                            . '". Make sure it exists and you have enough permissions.');
        }
    }
}
