<?php

namespace inSing\DataSourceBundle\Utilities;

class MySQLConn {

    private $con = null;
//    private static $instance;

//    private static $blatCon = null;
//    private static $blatInstance;
//
//    private static $eataCon = null;
//    private static $eataInstance;

    public function __construct($dbHost, $dbName, $dbUsername, $dbUserPassword, $dbport) {
        $this->getConnection($dbHost, $dbName, $dbUsername, $dbUserPassword, $dbport);
        return $this;
    }

    public function getConnection($dbHost, $dbName, $dbUsername, $dbUserPassword, $dbport) {
        try {
            // Set options
            $options = array(
                \PDO::ATTR_PERSISTENT => true,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
            );

            $str_connect = "mysql:host=" . $dbHost . ";" . "dbname=" . $dbName. ";" . "port=" . $dbport;
            $this->con = new \PDO($str_connect, $dbUsername, $dbUserPassword, $options);
            $this->con->exec("SET CHARACTER SET utf8");
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getInstance() {
        // One connection through whole application
        return $this;
    }

    public function insert($table, $data) {
        try {
            $fieldnames = array_keys($data);

            $sql = "INSERT INTO $table (" . implode(', ', $fieldnames) . ") ";

            $bound = '(:' . implode(', :', $fieldnames) . ' )';

            $sql .= ' VALUES ' . $bound;

            $stmt = $this->con->prepare($sql);

            $array = array();

            foreach ($data as $key => $vals) {
                $array[":$key"] = $vals;
            }

            return $stmt->execute($array);
        } catch (PDOException $e) {
        }
        return false;
    }

    public function update($table, $data, $where) {

        $array = array();
        $sql = "UPDATE $table SET ";

        foreach ($data as $field => $value) {
            $sql .= $field . '  = :f_' . $field . ' ';
            $array[':f_' . $field] = $value;
        }

        $sql .= 'WHERE ';

        foreach ($where as $field => $value) {
            $sql .= $field . '  = :w_' . $field . ' ';
            $array[':w_' . $field] = $value;
        }

        $stmt = $this->con->prepare($sql);

        return $stmt->execute($array);
    }

    public function close() {
        $this->con = null;
    }

    public function executeQuery($query, $params = null) {
        $stmt = $this->con->prepare($query);

        if ($params) {
            return $stmt->execute($params);
        } else {
        return $stmt->execute();
    }
    }

    public function prepareQuery($query) {
        $stmt = $this->con->prepare($query);

        return $stmt;
    }

    public function executeResultQuery($query, $fetchType = \PDO::FETCH_ASSOC) {
        $stmt = $this->con->prepare($query);

        $stmt->execute();
        $result = $stmt->fetchAll($fetchType);
        return $result;
    }

    public function fetchQuery($query, $fetchType = \PDO::FETCH_ASSOC) {
        $stmt = $this->con->prepare($query);

        $stmt->execute();
        $result = $stmt->fetch($fetchType);
        return $result;
    }

    /**
     * Get id by name
     * @param type $table
     * @param type $data
     * @return int
     */
    public function getIDbyName2($select = '*', $table, $data) {
        //$state = $data['state'];
        if ($data) {
            $name = '';
            $values = '';
            foreach ($data as $field => $value) {
                $name.=$field;
                $values.=$value;
            }
            $sql = "select $select from $table where $name=:i";

            $stmt = $this->con->prepare($sql);
            $stmt->execute(array("i" => $values));
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (is_array($result) && isset($result)) {
                $result = current($result);
                //echo '<pre>'; print_r($result);
                return $result[$select];
            }
        }
        return 0;
    }

    //
    public function getMarkRemoved($id) {
        if ($id) {
            $sql = "select mark_removed from business_category where category_id=$id";
            $stmt = $this->con->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            //echo '<pre>';print_r($result);
            if (!empty($result) && is_array($result)) {
                $result = current($result);
                if ($result['mark_removed'] == 0) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }
        return 0;
    }

    //get category_id, id of  business_category, properties by name
    public function getIDofCPbyName($name, $type = false) {
        if ($type == true) {
            $sql = "select id from properties where description=$name";
            $stmt = $this->con->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            if ($result) {
                $result = current($result);
                return $result['id'];
            }
        } else {
            $sql = "select category_id, parent_category_id from business_category where path=$name";
            $stmt = $this->con->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            if ($result) {
                $result = current($result);
                return $result['category_id'];
            }
        }
    }

    public function findOneBy($table, $where = array(), $select = '*') {
        $result = null;
        try {
            $sql = "SELECT $select FROM " . $table;

            $fields = $values = array();

            if (is_array($where) && count($where)) {
                $sql .= ' WHERE';
                $fields = array_keys($where);
                $values = array_values($where);
                $n = count($where);
                $i = 0;
                foreach ($fields as $f) {
                    $sql .= " $f = ?";

                    if ($i < $n - 1) {
                        $sql .= ' AND';
                    }
                    $i++;
                }
            }

            $sth = $this->con->prepare($sql);
            if (count($values)) {
                $i = 0;
                while ($i <= $n - 1) {
                    $i++;
                    $sth->bindParam($i, $values[$i - 1]);
                }
            }

            $sth->execute();
            $result = $sth->fetch(\PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        return $result;
    }

    /*
     * find all by for query
     */

    public function findAllBy($table, $where = array(), $select = '*') {
        $result = null;
        try {
            $sql = "SELECT $select FROM " . $table;

            $fields = $values = array();

            if (is_array($where) && count($where)) {
                $sql .= ' WHERE';
                $fields = array_keys($where);
                $values = array_values($where);
                $n = count($where);
                $i = 0;
                foreach ($fields as $f) {
                    $sql .= " $f = ?";

                    if ($i < $n - 1) {
                        $sql .= ' AND';
                    }
                    $i++;
                }
            }

            $sth = $this->con->prepare($sql);
            if (count($values)) {
                $i = 0;
                while ($i <= $n - 1) {
                    $i++;
                    $sth->bindParam($i, $values[$i - 1]);
                }
            }

            $sth->execute();
            $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        return $result;
    }

    public function beginTransaction() {
        return $this->con->beginTransaction();
    }

    public function commit() {
        return $this->con->commit();
    }

    public function rollBack() {
        return $this->con->rollBack();
    }

    public function lastInsertId() {
        return $this->con->lastInsertId();
    }

}
