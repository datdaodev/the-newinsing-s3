<?php

namespace inSing\DataSourceBundle\Utilities;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RnRDB {

    /**
     *
     * @var ContainerInterface
     */
    public $container;

    /**
     *
     * @var MySQLConn
     */
    protected $db_conn = null;

    protected $logger = null;

    protected $country_code = null;

    protected $item_type = null;

    protected $user_type = null;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->logger = new HgwLogger($this->container, 'rnr_db_info');
        $rnr_db_info = $this->container->getParameter('rnr_db_info');
        $rnr_db_info = $rnr_db_info['slave'];
        $this->db_conn = new MySQLConn($rnr_db_info['host'], $rnr_db_info['db_name'], $rnr_db_info['user'], $rnr_db_info['password'], $rnr_db_info['port']);

    }

    public function __destruct(){
        $this->db_conn->close();
    }

    public function init($country_code)
    {
        $this->country_code = $country_code;
        $item_types = $this->container->getParameter('rnr_channel');
        $this->item_type = $item_types[$this->country_code];

        $user_types = $this->container->getParameter('rnr_user_type');
        $this->item_type = $user_types[$this->country_code];

        return $this;
    }

    public function switchDB($is_slave = true)
    {
        $rnr_db_info = $this->container->getParameter('rnr_db_info');
        if($is_slave)
        {
            $rnr_db_info = $rnr_db_info['slave'];
        }
        else
        {
            $rnr_db_info = $rnr_db_info['master'];
        }
        $this->db_conn = new MySQLConn($rnr_db_info['host'], $rnr_db_info['db_name'], $rnr_db_info['user'], $rnr_db_info['password'], $rnr_db_info['port']);
    }

    public function getTotalUserPhotoByItemIds($item_ids)
    {
        try {
            if(!empty($item_ids))
            {
                $item_ids = implode(",", $item_ids);
                $query = "SELECT COUNT(p.id) AS total
                    FROM photo p INNER JOIN submission sm ON p.submission_id = sm.id AND sm.item_id IN ({$item_ids})
                    WHERE p.status = 1";
                $total = $this->db_conn->fetchQuery($query, \PDO::FETCH_COLUMN);
                return $total;
            }
        } catch (\Exception $exc) {
            $this->logger->error("Query: {$query}");
            $this->logger->exp_err($exc);
        }
        return 0;
    }

    public function getUserPhotosByItemIds(array $item_ids, $page, $per_page)
    {
        try {
            if(!empty($item_ids))
            {
                $limit = ($page - 1) * $per_page;
                $item_ids = implode(",", $item_ids);

                $query = "SELECT sm.item_id AS item_id, p.id AS photo_id
                    FROM photo p INNER JOIN submission sm ON p.submission_id = sm.id AND sm.item_id IN ({$item_ids})
                    WHERE p.status = 1 ORDER BY sm.created_at DESC LIMIT {$limit}, {$per_page}";

                $photos = $this->db_conn->executeResultQuery($query, \PDO::FETCH_ASSOC);
                return $photos;
            }
        } catch (\Exception $exc) {
            $this->logger->error("Query: {$query}");
            $this->logger->exp_err($exc);
        }
        return array();
    }
}