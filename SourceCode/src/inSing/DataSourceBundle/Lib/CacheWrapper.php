<?php

namespace inSing\DataSourceBundle\Lib;

/**
 * Wrapper from cache util bundle
 * @author Khuong Phan
 */
use Symfony\Component\DependencyInjection\ContainerInterface;

class CacheWrapper
{
    public static $cache = null;
    
    public function __construct(ContainerInterface $container) 
    {
        self::$cache = $container->get('new.insing.cache');
        self::$cache->setCacheLifetime($container->getParameter('insing_datasource_cache_timeout'));
    }

    public static function getCache() 
    {
        return self::$cache;
    }
}