<?php
namespace inSing\DataSourceBundle\Lib;

class GoogleAnalytics
{
    // Header
    private $inSingLogoCat		= 'Main inSing Navigation';
    private $inSingLogoAction	= 'Click';
    private $inSingLogoLabel	= 'inSing Logo';
    
    private $HGWCat				= 'Main inSing Navigation';
    private $HGWAction			= 'Click';
    private $HGWLabel			= 'HGW';
    
    private $ShoppingCat		= 'Main inSing Navigation';
    private $ShoppingAction		= 'Click';
    private $ShoppingLabel		= 'Shopping';
    
    private $MoviesCat			= 'Main inSing Navigation';
    private $MoviesAction		= 'Click';
    private $MoviesLabel		= 'Movies';
    
    private $EventsCat			= 'Main inSing Navigation';
    private $EventsAction		= 'Click';
    private $EventsLabel		= 'Events';
    
    private $BestDealsCat		= 'Main inSing Navigation';
    private $BestDealsAction	= 'Click';
    private $BestDealsLabel		= 'Best Deals';
    
    private $TravelCat			= 'Main inSing Navigation';
    private $TravelAction		= 'Click';
    private $TravelLabel		= 'Travel';
    
    private $FeaturesCat		= 'Main inSing Navigation';
    private $FeaturesAction		= 'Click';
    private $FeaturesLabel		= 'Features';
    
    private $SearchCat			= 'Main inSing Navigation';
    private $SearchAction		= 'Click';
    private $SearchLabel		= 'Business';
    
    private $LoginCat			= 'Main inSing Navigation';
    private $LoginAction		= 'Click';
    private $LoginLabel			= 'Login';
    

    // Footer
    private $SiteMapCat = 'Footer';
    private $SiteMapAction = 'Click';
    private $SiteMapLabel = 'Site Map';
    private $AboutUsCat = 'Footer';
    private $AboutUsAction = 'Click';
    private $AboutUsLabel = 'About Us';
    private $ContactUsCat = 'Footer';
    private $ContactUsAction = 'Click';
    private $ContactUsLabel = 'Contact Us';
    private $CareersCat = 'Footer';
    private $CareersAction = 'Click';
    private $CareersLabel = 'Careers';
    private $FAQCat = 'Footer';
    private $FAQAction = 'Click';
    private $FAQLabel = 'FAQ';
    private $DisplayAdvertisingCat = 'Footer';
    private $DisplayAdvertisingAction = 'Click';
    private $DisplayAdvertisingLabel = 'Display Advertising';
    private $BusinessListingsAdsCat = 'Footer';
    private $BusinessListingsAdsAction = 'Click';
    private $BusinessListingsAdsLabel = 'Business Listing & Search';
    private $HungryGoWhereCat = 'Footer';
    private $HungryGoWhereAction = 'Click';
    private $HungryGoWhereLabel = 'HungryGoWhere';
    private $LoginFooterCat = 'Footer';
    private $LoginFooterAction = 'Click';
    private $LoginFooterLabel = 'Login';
    private $RegisterFooterCat = 'Footer';
    private $RegisterFooterAction = 'Click';
    private $RegisterFooterLabel = 'Register';

    // Homepage
    private $mainCat = 'Trending Stories';
    private $mainAction = 'Trending Stories navigation click';
    private $mainLabel = 'mainpage';
    private $wtfCat = 'Trending Stories';
    private $wtfAction = 'Trending Stories navigation click';
    private $wtfLabel = 'wtf';
    private $lolCat = 'Trending Stories';
    private $lolAction = 'Trending Stories navigation click';
    private $lolLabel = 'lol';
    private $booCat = 'Trending Stories';
    private $booAction = 'Trending Stories navigation click';
    private $booLabel = 'boo';
    private $winCat = 'Trending Stories';
    private $winAction = 'Trending Stories navigation click';
    private $winLabel = 'win';
    private $itemFacebookCat = 'Trending Stories';
    private $itemFacebookAction = 'Share on Facebook';
    private $itemFacebookLabel = '';
    private $itemTwitterCat = 'Trending Stories';
    private $itemTwitterAction = 'Tweet on Twitter';
    private $itemTwitterLabel = '';
    private $itemGooglePlusCat = 'Trending Stories';
    private $itemGooglePlusAction = '+1 on Google Plus';
    private $itemGooglePlusLabel = '';
    private $itemBookmarkCat = 'Trending Stories';
    private $itemBookmarkAction = 'Bookmark on inSing';
    private $itemBookmarkLabel = '';
    private $itemCat = 'Trending Stories';
    private $itemAction = 'Item click';
    private $itemLabel = '';
    private $itemUserCat = 'Trending Stories';
    private $itemUserAction = 'User thumbnail click';
    private $itemUserLabel = '';

    /**
     * Ex :
     * @author	Vu Tran
     * @method	generate event ga
     * @param	gaCategory, gaAction, gaLabel
     * @return 	ga push
     * @since	Jul 9, 2013 5:03:36 PM 2013
     */
    private function generateEvent($gaCategory, $gaAction, $gaLabel)
    {
        return "_gaq.push(['_trackEvent', '$gaCategory', '$gaAction', '$gaLabel'])";
    }

    /**
     * Ex :
     * @author	Vu Tran
     * @method	geneate ga event for header
     * @param
     * @return	array ga header needed.
     * @since	Jul 9, 2013 5:04:29 PM 2013
     */
    //generate event ga main header
    public function getGAHeader()
    {
        $result = array();

        $result['inSingLogo']	= $this->generateEvent($this->inSingLogoCat, $this->inSingLogoAction, $this->inSingLogoLabel);
        $result['HGW']			= $this->generateEvent($this->HGWCat, $this->HGWAction, $this->HGWLabel);
        $result['Shopping']		= $this->generateEvent($this->ShoppingCat, $this->ShoppingAction, $this->ShoppingLabel);
        $result['Movies']		= $this->generateEvent($this->MoviesCat, $this->MoviesAction, $this->MoviesLabel);
        $result['Events']		= $this->generateEvent($this->EventsCat, $this->EventsAction, $this->EventsLabel);
        $result['BestDeals']	= $this->generateEvent($this->BestDealsCat, $this->BestDealsAction, $this->BestDealsLabel);
        $result['Travel']		= $this->generateEvent($this->TravelCat, $this->TravelAction, $this->TravelLabel);
        $result['Features']		= $this->generateEvent($this->FeaturesCat, $this->FeaturesAction, $this->FeaturesLabel);
        $result['Search']		= $this->generateEvent($this->SearchCat, $this->SearchAction, $this->SearchLabel);
        $result['Login']		= $this->generateEvent($this->LoginCat, $this->LoginAction, $this->LoginLabel);

        return $result;
    }

    /**
     * Ex :
     * @author	Vu Tran
     * @method	generate GA tracking footer
     * @param
     * @return	array ga footer needed.
     * @since	Jul 9, 2013 5:05:43 PM 2013
     */
    public function getGAFooter()
    {
        $result = array();

        $result['SiteMap'] = $this->generateEvent($this->SiteMapCat, $this->SiteMapAction, $this->SiteMapLabel);
        $result['AboutUs'] = $this->generateEvent($this->AboutUsCat, $this->AboutUsAction, $this->AboutUsLabel);
        $result['ContactUs'] = $this->generateEvent($this->ContactUsCat, $this->ContactUsAction, $this->ContactUsLabel);
        $result['Careers'] = $this->generateEvent($this->CareersCat, $this->CareersAction, $this->CareersLabel);
        $result['FAQ'] = $this->generateEvent($this->FAQCat, $this->FAQAction, $this->FAQLabel);
        $result['DisplayAdvertising'] = $this->generateEvent($this->DisplayAdvertisingCat, $this->DisplayAdvertisingAction, $this->DisplayAdvertisingLabel);
        $result['BusinessListingsAds'] = $this->generateEvent($this->BusinessListingsAdsCat, $this->BusinessListingsAdsAction, $this->BusinessListingsAdsLabel);
        $result['HungryGoWhere'] = $this->generateEvent($this->HungryGoWhereCat, $this->HungryGoWhereAction, $this->HungryGoWhereLabel);
        $result['Login'] = $this->generateEvent($this->LoginFooterCat, $this->LoginFooterAction, $this->LoginFooterLabel);
        $result['Register'] = $this->generateEvent($this->RegisterFooterCat, $this->RegisterFooterAction, $this->RegisterFooterLabel);

        return $result;
    }

    /**
     * Ex :
     * @author	Vu Tran
     * @method	generate ga for homepage
     * @param	value of label function needed : ex Slot [1-5], [HREF value of link]
     * @return	array of ga homepage
     * @since	Jul 9, 2013 5:06:16 PM 2013
     */
    public function getGAHomePage($value = '')
    {
        $value = trim($value);
        $result = array();

        $result['mainNavigation'] = $this->generateEvent($this->mainCat, $this->mainAction, $this->mainLabel);
        $result['wtfNavigation'] = $this->generateEvent($this->wtfCat, $this->wtfAction, $this->wtfLabel);
        $result['lolNavigation'] = $this->generateEvent($this->lolCat, $this->lolAction, $this->lolLabel);
        $result['booNavigation'] = $this->generateEvent($this->booCat, $this->booAction, $this->booLabel);
        $result['winNavigation'] = $this->generateEvent($this->winCat, $this->winAction, $this->winLabel);
        $result['itemFacebook'] = $this->generateEvent($this->itemFacebookCat, $this->itemFacebookAction, $this->itemFacebookLabel . $value);
        $result['itemTwitter'] = $this->generateEvent($this->itemTwitterCat, $this->itemTwitterAction, $this->itemTwitterLabel . $value);
        $result['itemGooglePlus'] = $this->generateEvent($this->itemGooglePlusCat, $this->itemGooglePlusAction, $this->itemGooglePlusLabel . $value);
        $result['itemBookmark'] = $this->generateEvent($this->itemBookmarkCat, $this->itemBookmarkAction, $this->itemBookmarkLabel . $value);
        $result['itemUser'] = $this->generateEvent($this->itemUserCat, $this->itemUserAction, $this->itemUserLabel . $value);
        $result['item'] = $this->generateEvent($this->itemCat, $this->itemAction, $this->itemLabel . $value);

        return $result;
    }
}
?>