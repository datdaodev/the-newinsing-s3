<?php

namespace inSing\DataSourceBundle\Lib\Exception;

use inSing\DataSourceBundle\Lib\Exception\DataSourceException;

class RuntimeErrorException extends DataSourceException
{
    
}