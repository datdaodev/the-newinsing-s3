<?php 

namespace inSing\DataSourceBundle\Lib\Monitor;

/**
 * An observer class for monitoring speed of execution
 *
 * @author Trung Nguyen
 */
class SpeedMonitor implements \SplObserver 
{
    const MAX_EXECUTE_TIME = 5;
    
    const LOG_FILE = 'data_source.log';
    
    const SLOWORDIE_LOG_FILE = 'slow_or_die.log';
    
    /**
     * @var Logger
     */
    protected $logger;
    
    public function __construct($logger)
    {
        $this->logger = $logger;
    }
    
    /**
     *  Calculate the duration of execution and save to log file
     *
     * @param  \SplSubject $subject
     * @return void
     */
    public function update(\SplSubject $subject)
    {
        $context = array(
            get_class($subject->ds),
            $subject->currentMethod,
            $subject->stopTime - $subject->startTime
        );
        
        // log to file
        $this->logger->addInfoToFile($subject->pid, $context, self::LOG_FILE);
        
        // log to slow_or_die.log in case the duration of execution reach to limit
        if ($subject->stopTime - $subject->startTime >= self::MAX_EXECUTE_TIME) {
            $this->logger->addInfoToFile($subject->pid, $context, self::SLOWORDIE_LOG_FILE);
        }
    }
}