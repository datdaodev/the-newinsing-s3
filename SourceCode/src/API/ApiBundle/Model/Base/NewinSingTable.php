<?php
/**
 * This class contains some functions which use for querying db
 * 
 * @author Vu Tran
 */

namespace API\ApiBundle\Model\Base;

use Doctrine\ORM\EntityManager;

class NewinSingTable {
	/**
	 * Doctrine Entity Manager
	 * 
	 * @var \Doctrine\ORM\EntityManager
	 */
	protected $em;
	
	/** 
	 * Doctrine Connection
	 * 
	 * @var \Doctrine\DBAL\Connection
	 */
	protected $conn;
	
	protected $parameters;
	
	/**
	 * Constructor of HgwrTable
	 * 
	 * @author Vu Tran
	 * @param  EntityManager $em
	 */
	public function __construct(EntityManager $em, $parameters) 
	{
		$this->em 	= $em;
		$this->conn	= $this->em->getConnection();
		$this->parameters = $parameters;
	}
}