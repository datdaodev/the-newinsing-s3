<?php

namespace API\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller; 
use Symfony\Component\HttpFoundation\Request;
use	FOS\RestBundle\View\View; 
use FOS\Rest\Util\Codes;
use	API\CoreBundle\Util\Helper as APIHelper;

/**
 * The base controller is used by all API Systems
 * 
 * @author Tung Co (improved by Trung Nguyen)
 */
class APIBaseController extends Controller
{
    	
    /**
     * @var Route
     */
    protected $currentRoute = null; // store route object of this request
    
    /**
     * @var Cache
     */
    protected $cache = null; // store cache object
    
	/**
	 * Do common tasks (e.g valid signature, session token, ...)
	 * 
	 * @author Tung.Co
	 * @param  Request $request
	 */
	public function preExecute(Request $request)
	{	    
	  	// get current route, fetch route's options then 
	    $this->currentRoute = $this->get('router')->getRouteCollection()->get($request->get('_route'));
	    $routeOptions = $this->currentRoute->getOptions();
	    
	    // check to see if this service is available 
	    if ($routeOptions['disable']) {
	    	$this->get('insing.util.logger')->addInfoToFile('Call disabled api : ' . $request->get('_route'));
	    	throw new \Exception("Service unavailable", Codes::HTTP_SERVICE_UNAVAILABLE);  
	    }
	    
	    // --------------------------------------------------------
	    // extract parameters from request
	    $contentType = '';
	    
	    if(strpos($request->headers->get('Content-Type'), 'application/json') === 0)
	    {
	    	$contentType = 'json';
	    	$arr = json_decode($request->getContent(), true);
	    	
	    	$sessionToken 	= isset( $arr['session_token'] ) ? $arr['session_token'] : '';
	    	$userToken 		= isset( $arr['user_token'] ) ? $arr['user_token'] : '';
	    	$appId 			= isset( $arr['app_id'] ) ? $arr['app_id'] : '';
	    	$password 		= isset( $arr['secret'] ) ? $arr['secret'] : '';
	    	$sig 			= $request->query->get('sig');
	    	
	    	// log
	    	$arrLogParams = array(
    	    	'content_type' => $request->headers->get('Content-Type'),
    	    	'conten_param' => $request->getContent()
	    	);
	    	$this->get('insing.util.logger')->addInfoToFile('Call api : ' . $request->get('_route'), $arrLogParams);
	    }
	    else
	    {
	    	//Get GET params & Router Params
	    	$get_param = $request->query->all();
	    	
	    	foreach ($this->currentRoute->compile()->getVariables() as $variable) {
	    		$get_param[$variable] = $request->attributes->get($variable);
	    	}
	    	
		    $sessionToken 	= $request->getMethod() == 'GET' ? $request->query->get('session_token') : $request->request->get('session_token');
		    $userToken 		= $request->getMethod() == 'GET' ? $request->query->get('user_token') : $request->request->get('user_token');
		    $appId 			= $request->getMethod() == 'GET' ? $request->query->get('app_id') : $request->request->get('app_id');
		    $password 		= $request->getMethod() == 'GET' ? $request->query->get('secret') : $request->request->get('secret');
		    $sig 			= $request->getMethod() == 'GET' ? $request->query->get('sig') : $request->request->get('sig');
		    
		    // log
	    	$arrLogParams = array(
    			'content_type' 	=> $request->headers->get('Content-Type'),
    			'get_param' 	=> $get_param,
    			'post_param' 	=> $request->request->all()
	    	);
	    	$this->get('insing.util.logger')->addInfoToFile('Call api : ' . $request->get('_route'), $arrLogParams);
	    }
	    
	    // --------------------------------------------------------
            // check session token if required		
            if ($routeOptions['check_session_token']) {		    
                $this->checkSessionToken( $sessionToken );
            } else { // check app id and password are valid
                $this->checkAppId($appId, $password);        
            }

            //var_dump($this->genSignature($sessionToken, $password, $contentType));
	    // check signature (required)	     
            if ($sig != $this->genSignature($sessionToken, $password, $contentType)) {
            	throw new \Exception("Signature is incorrect", Codes::HTTP_BAD_REQUEST);
            }
			
		// check user token if required
            if ($routeOptions['check_user_token']) {
                $this->checkUserToken( $userToken );
	    }
	    
	    // inject cache object
	    if ($routeOptions['cache']) {
	        $this->cache = $this->get('insing.util.cache')->getInstanceByName($routeOptions['cache_type']);
	    }
	    
	    /*
	    // get time to update expired time for token
	    $epochTime = time() + $this->container->getParameter('time_to_live_session_token');
	    $expiredTime 	= date('Y-m-d H:i:s', $epochTime);
	    
	    // update expired time for session token
	    if ($routeOptions['check_session_token'])
	    {
		    $this->get('SqlService')->updateExpiredDateBySessionToken($sessionToken, $expiredTime);
	    }
	    
	    // update expired time for user token
	    if ($routeOptions['check_user_token'])
	    {
	    	$this->get('SqlService')->updateExpiredDateByUserToken($userToken, $expiredTime);
	    } */
    
	}
	
	/**
	 * Check if this app id and password are valid
	 *
	 * @author Co Vu Thanh Tung
	 * @param  string $appId
	 * @param  string $password
	 * @return void
	 * @throws 401 if token is blank or doesn't exist
	 */
	protected function checkAppId($appId, $password)
	{
		if (!isset($appId) || $appId == '' || is_null($appId)
		    || !isset($password) || $password == '' || is_null($password)) {
		    
			throw new \Exception("Authentication failed", Codes::HTTP_UNAUTHORIZED);
		}
	
		$result = $this->get('SqlService')->chechAppId($appId, $password);
	
		if (!isset($result[0])) {
			throw new \Exception("Authentication failed", Codes::HTTP_UNAUTHORIZED);
		}
	}
	
	/**
	 * Check if this session token is valid
	 *
	 * @author Co Vu Thanh Tung
	 * @param  string $session_token
	 * @return void
	 * @throws 401 if token is blank or doesn't exist
	 * @throws 419 if token is expired
	 */
	protected function checkSessionToken($session_token = null)
	{
		if (!isset($session_token) || $session_token == '' || is_null($session_token)) {
			throw new \Exception("Session token is invalid", Codes::HTTP_UNAUTHORIZED);
		}
	
		$result = $this->get('SqlService')->checkSessionToken($session_token);				
		
		if (!isset($result[0])) {
			throw new \Exception("Session token is invalid", Codes::HTTP_UNAUTHORIZED);
		} elseif(isset($result[0]['is_expired']) && $result[0]['is_expired']) {
			throw new \Exception("Session token timeout", Codes::SESSION_TOKEN_TIMEOUT);
		}
		
		// cache session token for later use
		$cacheType = $this->container->getParameter('new_insing_api_session_token_cache');
		$cache = $this->get('insing.util.cache')->getInstanceByName($cacheType);
		
		if (!is_null($cache)){
			$cache->setCache($session_token, $result);
		}
	}
	
    /**
	 * Check if this user token is valid
	 *
	 * @author Co Vu Thanh Tung
	 * @param  string $user_token
	 * @return void
	 * @throws 401 if token is blank or doesn't exist
	 * @throws 419 if token is expired
	 */
    protected function checkUserToken($user_token = null)
    {
        if(!isset($user_token) || $user_token == '' || is_null($user_token)) {
        	throw new \Exception("User token is invalid", Codes::HTTP_UNAUTHORIZED);
        }
    
        $result = $this->get('SqlService')->checkUserToken( $user_token );
    
        if(!isset($result[0])) {
        	throw new \Exception("User token is invalid", Codes::HTTP_UNAUTHORIZED);
        } elseif(isset($result[0]['is_expired']) && $result[0]['is_expired']) {
        	throw new \Exception("User token timeout", Codes::USER_TOKEN_TIMEOUT);
        }
    }
  
	/**
	 * Generate signature base on current request
	 * 
	 * @author Co Vu Thanh Tung
	 * @param  string $session token
	 * @param  string $appId
	 * @param  string $contentType
	 * @return string $signature
	 */
	protected function genSignature($sessionToken, $password, $contentType)
	{
		if (!isset($password) || $password == '' || is_null($password)) {
			$data = $this->get('SqlService')->getInfoBySessionToken($sessionToken);
			
			if (!isset($data) || $data == '' || count($data) == 0) {
				throw new \Exception("Session token is invalid", Codes::HTTP_UNAUTHORIZED);
			}
			
 			$appId    = $data[0]['app_id'];
 			$appInfo  = $this->get('SqlService')->getSecretFromAppId($appId);
 			$password = $appInfo[0]['secret'];
		}	
		
		$pathInfo = $this->getRequest()->getPathInfo();	// e.g /1.0/authenticate
		$pathInfo = str_replace('/', '', $pathInfo);
		$pathInfo = $password . $pathInfo;
		
		switch ($contentType) {
			case 'json' :
				$params = $this->getRequest()->getContent();
				return APIHelper::genSignatureJson($params, $pathInfo);
				
			default:
				$params = ( $this->getRequest()->getMethod() == 'GET' ? $this->getRequest()->query->all() : $this->getRequest()->request->all() );
				return APIHelper::genSignature($params, $pathInfo);
		}
	}
	
	/**
	 * Output result to clients
	 * 
	 * @author Tung.Co
	 * @param  int   $code
	 * @param  array $data
	 * @return string
	 */
	protected function output($code, $data)
	{
		if (!is_array($data)) {
			$data = array();
		}
		
		return View::create()->setStatusCode($code)->setData($data);
	}
	
}
