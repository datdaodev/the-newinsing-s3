<?php

/**
 * This file is a part of APICoreBundle package.
 */

namespace API\CoreBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;

class ExceptionListener 
{
	
	/**
	 * Customize response
	 *
	 * @author Trung.Nguyen
	 * @param  GetResponseForExceptionEvent $event
	 */
	public function onKernelException(GetResponseForExceptionEvent $event)
	{		
		$exception = $event->getException();
		
		$jsonData = array(
			'status' => strval($exception->getCode()), 
			'error' => $exception->getMessage()			
		);
		
		try {
			$response = new Response(json_encode($jsonData) , strval($exception->getCode()));
		} 
		catch (\Exception $e) {
			$response = new Response(json_encode($jsonData) , '200');
		}
		
		// setup the Response object based on the caught exception
		$event->setResponse($response);
	
		// you can alternatively set a new Exception
		// $exception = new \Exception('Some special exception');
		// $event->setException($exception);
	}
	
}