<?php

/**
 * This file is a part of APICoreBundle package.
 */

namespace API\CoreBundle\Listener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class ControllerListener 
{
	
	/**
	 * Inject [preExecute] method on all Actions
	 * 
	 * @author Trung.Nguyen
	 * @param  FilterControllerEvent $event
	 */
	public function onKernelController(FilterControllerEvent $event) 
	{
		if (HttpKernelInterface::MASTER_REQUEST === $event->getRequestType()) 
		{
			$controllers = $event->getController();
			
			if(is_array($controllers)) 
			{
				$controller = $controllers[0];
	
				if (is_object($controller) && method_exists($controller, 'preExecute')) 
				{
					$controller->preExecute($event->getRequest());
				}
			}
		}
	}
	
}